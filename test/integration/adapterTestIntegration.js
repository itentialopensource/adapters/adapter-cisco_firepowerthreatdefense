/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_firepowerthreatdefense',
      type: 'FirepowerThreatDefense',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const FirepowerThreatDefense = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Firepower_threat_defense Adapter Test', () => {
  describe('FirepowerThreatDefense Class Tests', () => {
    const a = new FirepowerThreatDefense(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const interfaceAddInterfaceMigrationImmediateBodyParam = {
      sourceInterfaceId: 'string',
      sourceInterfaceType: 'string',
      destinationInterfaceId: 'string',
      destinationInterfaceType: 'string',
      type: 'interfacemigrationimmediate'
    };
    describe('#addInterfaceMigrationImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addInterfaceMigrationImmediate(interfaceAddInterfaceMigrationImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.sourceInterfaceId);
                assert.equal('string', data.response.sourceInterfaceType);
                assert.equal('string', data.response.destinationInterfaceId);
                assert.equal('string', data.response.destinationInterfaceType);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('interfacemigrationimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'addInterfaceMigrationImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let interfaceObjId = 'fakedata';
    const interfaceAddBridgeGroupInterfaceBodyParam = {
      monitorInterface: false,
      type: 'bridgegroupinterface'
    };
    describe('#addBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBridgeGroupInterface(interfaceAddBridgeGroupInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(true, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(true, Array.isArray(data.response.selectedInterfaces));
                assert.equal(10, data.response.bridgeGroupId);
                assert.equal('string', data.response.id);
                assert.equal('bridgegroupinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              interfaceObjId = data.response.id;
              saveMockData('Interface', 'addBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let interfaceParentId = 'fakedata';
    const interfaceAddEtherChannelInterfaceBodyParam = {
      monitorInterface: true,
      mode: 'BRIDGEGROUPMEMBER',
      mtu: 7,
      enabled: false,
      etherChannelID: 7,
      memberInterfaces: [
        {
          type: 'string'
        }
      ],
      lacpMode: 'ON',
      speedType: 'IGNORE',
      duplexType: 'AUTO',
      type: 'etherchannelinterface'
    };
    describe('#addEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addEtherChannelInterface(interfaceAddEtherChannelInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(false, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(false, data.response.managementOnly);
                assert.equal(false, data.response.managementInterface);
                assert.equal('PASSIVE', data.response.mode);
                assert.equal('UP', data.response.linkState);
                assert.equal(3, data.response.mtu);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(1, data.response.etherChannelID);
                assert.equal(true, Array.isArray(data.response.memberInterfaces));
                assert.equal('PASSIVE', data.response.lacpMode);
                assert.equal('TEN_THOUSAND', data.response.speedType);
                assert.equal('FULL', data.response.duplexType);
                assert.equal(true, data.response.tenGigabitInterface);
                assert.equal(true, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal('etherchannelinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              interfaceParentId = data.response.id;
              saveMockData('Interface', 'addEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceAddEtherChannelSubInterfaceBodyParam = {
      monitorInterface: false,
      mode: 'BRIDGEGROUPMEMBER',
      mtu: 7,
      enabled: true,
      subIntfId: 1,
      type: 'subinterface'
    };
    describe('#addEtherChannelSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addEtherChannelSubInterface(null, interfaceParentId, interfaceAddEtherChannelSubInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(true, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(true, data.response.managementOnly);
                assert.equal(true, data.response.managementInterface);
                assert.equal('SWITCHPORT', data.response.mode);
                assert.equal('DOWN', data.response.linkState);
                assert.equal(7, data.response.mtu);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(false, data.response.present);
                assert.equal(1, data.response.rulePosition);
                assert.equal(true, data.response.tenGigabitInterface);
                assert.equal(true, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal(6, data.response.subIntfId);
                assert.equal(10, data.response.vlanId);
                assert.equal('subinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'addEtherChannelSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceAddSubInterfaceBodyParam = {
      monitorInterface: false,
      mode: 'PASSIVE',
      mtu: 3,
      enabled: true,
      subIntfId: 3,
      type: 'subinterface'
    };
    describe('#addSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSubInterface(null, interfaceParentId, interfaceAddSubInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(true, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(false, data.response.managementOnly);
                assert.equal(true, data.response.managementInterface);
                assert.equal('BRIDGEGROUPMEMBER', data.response.mode);
                assert.equal('UP', data.response.linkState);
                assert.equal(3, data.response.mtu);
                assert.equal(false, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(true, data.response.present);
                assert.equal(1, data.response.rulePosition);
                assert.equal(true, data.response.tenGigabitInterface);
                assert.equal(false, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal(1, data.response.subIntfId);
                assert.equal(4, data.response.vlanId);
                assert.equal('subinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'addSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceAddVlanInterfaceBodyParam = {
      monitorInterface: false,
      mode: 'BRIDGEGROUPMEMBER',
      mtu: 5,
      enabled: false,
      vlanId: 6,
      type: 'vlaninterface'
    };
    describe('#addVlanInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addVlanInterface(interfaceAddVlanInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(false, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(true, data.response.managementOnly);
                assert.equal(true, data.response.managementInterface);
                assert.equal('SWITCHPORT', data.response.mode);
                assert.equal('DOWN', data.response.linkState);
                assert.equal(4, data.response.mtu);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(10, data.response.vlanId);
                assert.equal(false, data.response.tenGigabitInterface);
                assert.equal(true, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal('vlaninterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'addVlanInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceMigrationImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceMigrationImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getInterfaceMigrationImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeGroupInterfaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBridgeGroupInterfaceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getBridgeGroupInterfaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceEditBridgeGroupInterfaceBodyParam = {
      monitorInterface: true,
      type: 'bridgegroupinterface'
    };
    describe('#editBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editBridgeGroupInterface(interfaceObjId, interfaceEditBridgeGroupInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'editBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBridgeGroupInterface(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(false, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(true, Array.isArray(data.response.selectedInterfaces));
                assert.equal(3, data.response.bridgeGroupId);
                assert.equal('string', data.response.id);
                assert.equal('bridgegroupinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelInterfaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEtherChannelInterfaceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getEtherChannelInterfaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceEditEtherChannelInterfaceBodyParam = {
      monitorInterface: true,
      mode: 'PASSIVE',
      mtu: 9,
      enabled: true,
      etherChannelID: 1,
      memberInterfaces: [
        {
          type: 'string'
        }
      ],
      lacpMode: 'ON',
      speedType: 'TEN',
      duplexType: 'IGNORE',
      type: 'etherchannelinterface'
    };
    describe('#editEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editEtherChannelInterface(interfaceObjId, interfaceEditEtherChannelInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'editEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEtherChannelInterface(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(false, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(false, data.response.managementOnly);
                assert.equal(false, data.response.managementInterface);
                assert.equal('SWITCHPORT', data.response.mode);
                assert.equal('DOWN', data.response.linkState);
                assert.equal(6, data.response.mtu);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(5, data.response.etherChannelID);
                assert.equal(true, Array.isArray(data.response.memberInterfaces));
                assert.equal('PASSIVE', data.response.lacpMode);
                assert.equal('TEN_THOUSAND', data.response.speedType);
                assert.equal('IGNORE', data.response.duplexType);
                assert.equal(true, data.response.tenGigabitInterface);
                assert.equal(true, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal('etherchannelinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelSubInterfaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEtherChannelSubInterfaceList(null, null, null, null, interfaceParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getEtherChannelSubInterfaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceEditEtherChannelSubInterfaceBodyParam = {
      monitorInterface: false,
      mode: 'SWITCHPORT',
      mtu: 4,
      enabled: false,
      subIntfId: 4,
      type: 'subinterface'
    };
    describe('#editEtherChannelSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editEtherChannelSubInterface(null, interfaceParentId, interfaceObjId, interfaceEditEtherChannelSubInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'editEtherChannelSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEtherChannelSubInterface(interfaceParentId, interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(true, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(false, data.response.managementOnly);
                assert.equal(false, data.response.managementInterface);
                assert.equal('SWITCHPORT', data.response.mode);
                assert.equal('UP', data.response.linkState);
                assert.equal(5, data.response.mtu);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(false, data.response.present);
                assert.equal(7, data.response.rulePosition);
                assert.equal(false, data.response.tenGigabitInterface);
                assert.equal(true, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal(4, data.response.subIntfId);
                assert.equal(2, data.response.vlanId);
                assert.equal('subinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getEtherChannelSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalInterfaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPhysicalInterfaceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getPhysicalInterfaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceEditPhysicalInterfaceBodyParam = {
      monitorInterface: false,
      mode: 'PASSIVE',
      mtu: 4,
      enabled: false,
      type: 'physicalinterface'
    };
    describe('#editPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editPhysicalInterface(interfaceObjId, interfaceEditPhysicalInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'editPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPhysicalInterface(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(false, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(true, data.response.managementOnly);
                assert.equal(true, data.response.managementInterface);
                assert.equal('PASSIVE', data.response.mode);
                assert.equal('DOWN', data.response.linkState);
                assert.equal(6, data.response.mtu);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal('TEN', data.response.speedType);
                assert.equal('IGNORE', data.response.duplexType);
                assert.equal('object', typeof data.response.switchPortConfig);
                assert.equal('object', typeof data.response.powerOverEthernet);
                assert.equal(true, data.response.present);
                assert.equal(true, data.response.tenGigabitInterface);
                assert.equal(false, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal('physicalinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubInterfaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubInterfaceList(null, null, null, null, interfaceParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getSubInterfaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceEditSubInterfaceBodyParam = {
      monitorInterface: true,
      mode: 'BRIDGEGROUPMEMBER',
      mtu: 9,
      enabled: true,
      subIntfId: 6,
      type: 'subinterface'
    };
    describe('#editSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSubInterface(null, interfaceParentId, interfaceObjId, interfaceEditSubInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'editSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubInterface(interfaceParentId, interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(false, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(false, data.response.managementOnly);
                assert.equal(true, data.response.managementInterface);
                assert.equal('BRIDGEGROUPMEMBER', data.response.mode);
                assert.equal('DOWN', data.response.linkState);
                assert.equal(8, data.response.mtu);
                assert.equal(false, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(false, data.response.present);
                assert.equal(1, data.response.rulePosition);
                assert.equal(false, data.response.tenGigabitInterface);
                assert.equal(true, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal(6, data.response.subIntfId);
                assert.equal(8, data.response.vlanId);
                assert.equal('subinterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceDataList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceDataList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getInterfaceDataList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceData(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.hardwareName);
                assert.equal('object', typeof data.response.ipv4Address);
                assert.equal('object', typeof data.response.ipv6Address);
                assert.equal('string', data.response.macAddress);
                assert.equal('TEN', data.response.speedType);
                assert.equal(true, data.response.enabled);
                assert.equal('DOWN', data.response.linkState);
                assert.equal('string', data.response.voltage);
                assert.equal('string', data.response.current);
                assert.equal('string', data.response.id);
                assert.equal('InterfaceData', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getInterfaceData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanInterfaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanInterfaceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getVlanInterfaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceEditVlanInterfaceBodyParam = {
      monitorInterface: false,
      mode: 'PASSIVE',
      mtu: 6,
      enabled: false,
      vlanId: 5,
      type: 'vlaninterface'
    };
    describe('#editVlanInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editVlanInterface(interfaceObjId, interfaceEditVlanInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'editVlanInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanInterface(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.hardwareName);
                assert.equal(false, data.response.monitorInterface);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(true, data.response.managementOnly);
                assert.equal(false, data.response.managementInterface);
                assert.equal('ROUTED', data.response.mode);
                assert.equal('DOWN', data.response.linkState);
                assert.equal(6, data.response.mtu);
                assert.equal(false, data.response.enabled);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.standbyMacAddress);
                assert.equal('object', typeof data.response.pppoe);
                assert.equal(7, data.response.vlanId);
                assert.equal(false, data.response.tenGigabitInterface);
                assert.equal(true, data.response.gigabitInterface);
                assert.equal('string', data.response.id);
                assert.equal('vlaninterface', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getVlanInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryInterfaceMigrationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryInterfaceMigrationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getJobHistoryInterfaceMigrationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryInterfaceMigration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryInterfaceMigration(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal(true, Array.isArray(data.response.violations));
                assert.equal('string', data.response.id);
                assert.equal('jobhistoryinterfacemigration', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getJobHistoryInterfaceMigration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemInformationObjId = 'fakedata';
    describe('#getSystemInformation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemInformation(systemInformationObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ipv4);
                assert.equal('string', data.response.ipv6);
                assert.equal('string', data.response.softwareVersion);
                assert.equal('object', typeof data.response.vdbVersion);
                assert.equal('object', typeof data.response.sruVersion);
                assert.equal('object', typeof data.response.snortVersion);
                assert.equal('object', typeof data.response.databaseInfo);
                assert.equal('string', data.response.platformModel);
                assert.equal(7, data.response.currentTime);
                assert.equal('string', data.response.serialNumber);
                assert.equal('object', typeof data.response.geolocationVersion);
                assert.equal('object', typeof data.response.securityIntelligenceFeedsInfo);
                assert.equal('string', data.response.modelId);
                assert.equal('string', data.response.modelNumber);
                assert.equal('string', data.response.applianceUuid);
                assert.equal('string', data.response.managementInterfaceName);
                assert.equal(4, data.response.systemUptime);
                assert.equal('string', data.response.chassisSerialNumber);
                assert.equal('string', data.response.fxosVersion);
                assert.equal('string', data.response.id);
                assert.equal('SystemInformation', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemInformation', 'getSystemInformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const urlCategoryInfoObjId = 'fakedata';
    describe('#getUrlCategoryInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUrlCategoryInfo(urlCategoryInfoObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.lastUpdatedOn);
                assert.equal('string', data.response.id);
                assert.equal('UrlCategoryInfo', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UrlCategoryInfo', 'getUrlCategoryInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commandAddCommandBodyParam = {
      commandInput: 'string',
      type: 'Command'
    };
    describe('#addCommand - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCommand(commandAddCommandBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.commandInput);
                assert.equal('string', data.response.commandOutput);
                assert.equal(1, data.response.timeOut);
                assert.equal('string', data.response.id);
                assert.equal('Command', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Command', 'addCommand', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commandAutoCompleteObjId = 'fakedata';
    describe('#getCommandAutoComplete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCommandAutoComplete(commandAutoCompleteObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.autoCompleteCommands);
                assert.equal('string', data.response.id);
                assert.equal('CommandAutoComplete', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CommandAutoComplete', 'getCommandAutoComplete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacePresenceChangeAddInterfacePresenceChangeBodyParam = {
      type: 'InterfacePresenceChange'
    };
    describe('#addInterfacePresenceChange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addInterfacePresenceChange(interfacePresenceChangeAddInterfacePresenceChangeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.removedInterfaces));
                assert.equal(true, Array.isArray(data.response.newInterfaces));
                assert.equal(true, Array.isArray(data.response.returnedInterfaces));
                assert.equal('string', data.response.id);
                assert.equal('InterfacePresenceChange', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InterfacePresenceChange', 'addInterfacePresenceChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const featureInformationObjId = 'fakedata';
    describe('#getFeatureInformation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFeatureInformation(featureInformationObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.featureList));
                assert.equal('string', data.response.id);
                assert.equal('FeatureInformation', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FeatureInformation', 'getFeatureInformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const telemetryObjId = 'fakedata';
    describe('#getTelemetry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelemetry(telemetryObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.telemetryData);
                assert.equal('string', data.response.id);
                assert.equal('Telemetry', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Telemetry', 'getTelemetry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPAccessListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHTTPAccessListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HTTPAccessList', 'getHTTPAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hTTPAccessListObjId = 'fakedata';
    const hTTPAccessListEditHTTPAccessListBodyParam = {
      type: 'httpaccesslist'
    };
    describe('#editHTTPAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editHTTPAccessList(hTTPAccessListObjId, hTTPAccessListEditHTTPAccessListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HTTPAccessList', 'editHTTPAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHTTPAccessList(hTTPAccessListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.networkObjects));
                assert.equal('string', data.response.id);
                assert.equal('httpaccesslist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HTTPAccessList', 'getHTTPAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSHAccessListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSHAccessListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHAccessList', 'getSSHAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSHAccessListObjId = 'fakedata';
    const sSHAccessListEditSSHAccessListBodyParam = {
      type: 'sshaccesslist'
    };
    describe('#editSSHAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSSHAccessList(sSHAccessListObjId, sSHAccessListEditSSHAccessListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHAccessList', 'editSSHAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSHAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSHAccessList(sSHAccessListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.networkObjects));
                assert.equal('string', data.response.id);
                assert.equal('sshaccesslist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHAccessList', 'getSSHAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dataInterfaceManagementAccessObjId = 'fakedata';
    const dataInterfaceManagementAccessAddDataInterfaceManagementAccessBodyParam = {
      networkObjects: [
        {
          type: 'string'
        }
      ],
      networkInterface: {
        type: 'string'
      },
      protocols: [
        'HTTPS'
      ],
      type: 'datainterfacemanagementaccess'
    };
    describe('#addDataInterfaceManagementAccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDataInterfaceManagementAccess(dataInterfaceManagementAccessAddDataInterfaceManagementAccessBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.networkObjects));
                assert.equal('object', typeof data.response.networkInterface);
                assert.equal(true, Array.isArray(data.response.protocols));
                assert.equal('string', data.response.id);
                assert.equal('datainterfacemanagementaccess', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              dataInterfaceManagementAccessObjId = data.response.id;
              saveMockData('DataInterfaceManagementAccess', 'addDataInterfaceManagementAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataInterfaceManagementAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataInterfaceManagementAccessList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataInterfaceManagementAccess', 'getDataInterfaceManagementAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataInterfaceManagementAccessEditDataInterfaceManagementAccessBodyParam = {
      networkObjects: [
        {
          type: 'string'
        }
      ],
      networkInterface: {
        type: 'string'
      },
      protocols: [
        'HTTPS'
      ],
      type: 'datainterfacemanagementaccess'
    };
    describe('#editDataInterfaceManagementAccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDataInterfaceManagementAccess(dataInterfaceManagementAccessObjId, dataInterfaceManagementAccessEditDataInterfaceManagementAccessBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataInterfaceManagementAccess', 'editDataInterfaceManagementAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataInterfaceManagementAccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataInterfaceManagementAccess(dataInterfaceManagementAccessObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.networkObjects));
                assert.equal('object', typeof data.response.networkInterface);
                assert.equal(true, Array.isArray(data.response.protocols));
                assert.equal('string', data.response.id);
                assert.equal('datainterfacemanagementaccess', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataInterfaceManagementAccess', 'getDataInterfaceManagementAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceHostnameList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceHostnameList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHostname', 'getDeviceHostnameList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceHostnameObjId = 'fakedata';
    const deviceHostnameEditDeviceHostnameBodyParam = {
      type: 'devicehostname'
    };
    describe('#editDeviceHostname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDeviceHostname(deviceHostnameObjId, deviceHostnameEditDeviceHostnameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHostname', 'editDeviceHostname', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceHostname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceHostname(deviceHostnameObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.hostname);
                assert.equal('string', data.response.id);
                assert.equal('devicehostname', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHostname', 'getDeviceHostname', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebUICertificateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebUICertificateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebUICertificate', 'getWebUICertificateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webUICertificateObjId = 'fakedata';
    const webUICertificateEditWebUICertificateBodyParam = {
      certificate: {
        type: 'string'
      },
      type: 'webuicertificate'
    };
    describe('#editWebUICertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editWebUICertificate(webUICertificateObjId, webUICertificateEditWebUICertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebUICertificate', 'editWebUICertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebUICertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebUICertificate(webUICertificateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.certificate);
                assert.equal(true, Array.isArray(data.response.trustChain));
                assert.equal('string', data.response.id);
                assert.equal('webuicertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebUICertificate', 'getWebUICertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManagementIPList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManagementIPList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagementIP', 'getManagementIPList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementIPObjId = 'fakedata';
    const managementIPEditManagementIPBodyParam = {
      routeInternally: true,
      type: 'managementip'
    };
    describe('#editManagementIP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editManagementIP(managementIPObjId, managementIPEditManagementIPBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagementIP', 'editManagementIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManagementIP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManagementIP(managementIPObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('DHCP', data.response.ipv4Mode);
                assert.equal('string', data.response.ipv4Address);
                assert.equal('string', data.response.ipv4NetMask);
                assert.equal('string', data.response.ipv4Gateway);
                assert.equal('STATIC', data.response.ipv6Mode);
                assert.equal('string', data.response.ipv6Address);
                assert.equal(10, data.response.ipv6Prefix);
                assert.equal('string', data.response.ipv6Gateway);
                assert.equal(true, data.response.dhcpServerEnabled);
                assert.equal('string', data.response.dhcpServerAddressPool);
                assert.equal('UP', data.response.linkState);
                assert.equal(true, data.response.routeInternally);
                assert.equal('string', data.response.id);
                assert.equal('managementip', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagementIP', 'getManagementIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let securityZoneObjId = 'fakedata';
    const securityZoneAddSecurityZoneBodyParam = {
      name: 'string',
      mode: 'PASSIVE',
      type: 'securityzone'
    };
    describe('#addSecurityZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSecurityZone(securityZoneAddSecurityZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('PASSIVE', data.response.mode);
                assert.equal('string', data.response.id);
                assert.equal('securityzone', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              securityZoneObjId = data.response.id;
              saveMockData('SecurityZone', 'addSecurityZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZoneList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityZoneList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityZone', 'getSecurityZoneList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityZoneEditSecurityZoneBodyParam = {
      name: 'string',
      mode: 'PASSIVE',
      type: 'securityzone'
    };
    describe('#editSecurityZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecurityZone(securityZoneObjId, securityZoneEditSecurityZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityZone', 'editSecurityZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityZone(securityZoneObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('PASSIVE', data.response.mode);
                assert.equal('string', data.response.id);
                assert.equal('securityzone', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityZone', 'getSecurityZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let identityServicesEngineObjId = 'fakedata';
    const identityServicesEngineAddIdentityServicesEngineBodyParam = {
      name: 'string',
      ftdCertificate: {
        type: 'string',
        name: 'string'
      },
      pxGridCertificate: {
        type: 'string',
        name: 'string'
      },
      mntCertificate: {
        type: 'string',
        name: 'string'
      },
      enabled: false,
      subscribeToSessionDirectoryTopic: true,
      subscribeToSxpTopic: true,
      type: 'identityservicesengine'
    };
    describe('#addIdentityServicesEngine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIdentityServicesEngine(identityServicesEngineAddIdentityServicesEngineBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.ftdCertificate);
                assert.equal('object', typeof data.response.pxGridCertificate);
                assert.equal('object', typeof data.response.mntCertificate);
                assert.equal(true, Array.isArray(data.response.iseNetworkFilters));
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.subscribeToSessionDirectoryTopic);
                assert.equal(false, data.response.subscribeToSxpTopic);
                assert.equal('string', data.response.primaryIseServer);
                assert.equal('string', data.response.secondaryIseServer);
                assert.equal('string', data.response.id);
                assert.equal('identityservicesengine', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              identityServicesEngineObjId = data.response.id;
              saveMockData('IdentityServicesEngine', 'addIdentityServicesEngine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityServicesEngineList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityServicesEngineList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityServicesEngine', 'getIdentityServicesEngineList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityServicesEngineEditIdentityServicesEngineBodyParam = {
      name: 'string',
      ftdCertificate: {
        type: 'string',
        name: 'string'
      },
      pxGridCertificate: {
        type: 'string',
        name: 'string'
      },
      mntCertificate: {
        type: 'string',
        name: 'string'
      },
      enabled: false,
      subscribeToSessionDirectoryTopic: false,
      subscribeToSxpTopic: false,
      type: 'identityservicesengine'
    };
    describe('#editIdentityServicesEngine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIdentityServicesEngine(identityServicesEngineObjId, identityServicesEngineEditIdentityServicesEngineBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityServicesEngine', 'editIdentityServicesEngine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityServicesEngine - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityServicesEngine(identityServicesEngineObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.ftdCertificate);
                assert.equal('object', typeof data.response.pxGridCertificate);
                assert.equal('object', typeof data.response.mntCertificate);
                assert.equal(true, Array.isArray(data.response.iseNetworkFilters));
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.subscribeToSessionDirectoryTopic);
                assert.equal(false, data.response.subscribeToSxpTopic);
                assert.equal('string', data.response.primaryIseServer);
                assert.equal('string', data.response.secondaryIseServer);
                assert.equal('string', data.response.id);
                assert.equal('identityservicesengine', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityServicesEngine', 'getIdentityServicesEngine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let standardAccessListObjId = 'fakedata';
    const standardAccessListAddStandardAccessListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'standardaccessentry'
        }
      ],
      type: 'standardaccesslist'
    };
    describe('#addStandardAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addStandardAccessList(standardAccessListAddStandardAccessListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('standardaccesslist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              standardAccessListObjId = data.response.id;
              saveMockData('StandardAccessList', 'addStandardAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardAccessListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStandardAccessListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardAccessList', 'getStandardAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const standardAccessListEditStandardAccessListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'standardaccessentry'
        }
      ],
      type: 'standardaccesslist'
    };
    describe('#editStandardAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editStandardAccessList(standardAccessListObjId, standardAccessListEditStandardAccessListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardAccessList', 'editStandardAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStandardAccessList(standardAccessListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('standardaccesslist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardAccessList', 'getStandardAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let extendedAccessListObjId = 'fakedata';
    const extendedAccessListAddExtendedAccessListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'extendedaccessentry'
        }
      ],
      type: 'extendedaccesslist'
    };
    describe('#addExtendedAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addExtendedAccessList(extendedAccessListAddExtendedAccessListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('extendedaccesslist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              extendedAccessListObjId = data.response.id;
              saveMockData('ExtendedAccessList', 'addExtendedAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedAccessListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtendedAccessListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedAccessList', 'getExtendedAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extendedAccessListEditExtendedAccessListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'extendedaccessentry'
        }
      ],
      type: 'extendedaccesslist'
    };
    describe('#editExtendedAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editExtendedAccessList(extendedAccessListObjId, extendedAccessListEditExtendedAccessListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedAccessList', 'editExtendedAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedAccessList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtendedAccessList(extendedAccessListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('extendedaccesslist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedAccessList', 'getExtendedAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let aSPathListObjId = 'fakedata';
    const aSPathListAddASPathListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'aspathentry'
        }
      ],
      type: 'aspathlist'
    };
    describe('#addASPathList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addASPathList(aSPathListAddASPathListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('aspathlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              aSPathListObjId = data.response.id;
              saveMockData('ASPathList', 'addASPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASPathListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getASPathListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathList', 'getASPathListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aSPathListEditASPathListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'aspathentry'
        }
      ],
      type: 'aspathlist'
    };
    describe('#editASPathList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editASPathList(aSPathListObjId, aSPathListEditASPathListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathList', 'editASPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASPathList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getASPathList(aSPathListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('aspathlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathList', 'getASPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let routeMapObjId = 'fakedata';
    const routeMapAddRouteMapBodyParam = {
      entries: [
        {
          type: 'routemapentry'
        }
      ],
      type: 'routemap'
    };
    describe('#addRouteMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRouteMap(routeMapAddRouteMapBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('routemap', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              routeMapObjId = data.response.id;
              saveMockData('RouteMap', 'addRouteMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteMapList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRouteMapList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMap', 'getRouteMapList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeMapEditRouteMapBodyParam = {
      entries: [
        {
          type: 'routemapentry'
        }
      ],
      type: 'routemap'
    };
    describe('#editRouteMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editRouteMap(routeMapObjId, routeMapEditRouteMapBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMap', 'editRouteMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRouteMap(routeMapObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('routemap', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMap', 'getRouteMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let standardCommunityListObjId = 'fakedata';
    const standardCommunityListAddStandardCommunityListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'standardcommunityentry'
        }
      ],
      type: 'standardcommunitylist'
    };
    describe('#addStandardCommunityList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addStandardCommunityList(standardCommunityListAddStandardCommunityListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('standardcommunitylist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              standardCommunityListObjId = data.response.id;
              saveMockData('StandardCommunityList', 'addStandardCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardCommunityListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStandardCommunityListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardCommunityList', 'getStandardCommunityListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const standardCommunityListEditStandardCommunityListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'standardcommunityentry'
        }
      ],
      type: 'standardcommunitylist'
    };
    describe('#editStandardCommunityList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editStandardCommunityList(standardCommunityListObjId, standardCommunityListEditStandardCommunityListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardCommunityList', 'editStandardCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardCommunityList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStandardCommunityList(standardCommunityListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('standardcommunitylist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardCommunityList', 'getStandardCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let expandedCommunityListObjId = 'fakedata';
    const expandedCommunityListAddExpandedCommunityListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'expandedcommunityentry'
        }
      ],
      type: 'expandedcommunitylist'
    };
    describe('#addExpandedCommunityList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addExpandedCommunityList(expandedCommunityListAddExpandedCommunityListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('expandedcommunitylist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              expandedCommunityListObjId = data.response.id;
              saveMockData('ExpandedCommunityList', 'addExpandedCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExpandedCommunityListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExpandedCommunityListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpandedCommunityList', 'getExpandedCommunityListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const expandedCommunityListEditExpandedCommunityListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'expandedcommunityentry'
        }
      ],
      type: 'expandedcommunitylist'
    };
    describe('#editExpandedCommunityList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editExpandedCommunityList(expandedCommunityListObjId, expandedCommunityListEditExpandedCommunityListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpandedCommunityList', 'editExpandedCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExpandedCommunityList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExpandedCommunityList(expandedCommunityListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('expandedcommunitylist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpandedCommunityList', 'getExpandedCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let iPV4PrefixListObjId = 'fakedata';
    const iPV4PrefixListAddIPV4PrefixListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'ipprefixentry'
        }
      ],
      type: 'ipv4prefixlist'
    };
    describe('#addIPV4PrefixList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIPV4PrefixList(iPV4PrefixListAddIPV4PrefixListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('ipv4prefixlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              iPV4PrefixListObjId = data.response.id;
              saveMockData('IPV4PrefixList', 'addIPV4PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV4PrefixListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPV4PrefixListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV4PrefixList', 'getIPV4PrefixListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPV4PrefixListEditIPV4PrefixListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'ipprefixentry'
        }
      ],
      type: 'ipv4prefixlist'
    };
    describe('#editIPV4PrefixList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIPV4PrefixList(iPV4PrefixListObjId, iPV4PrefixListEditIPV4PrefixListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV4PrefixList', 'editIPV4PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV4PrefixList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPV4PrefixList(iPV4PrefixListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('ipv4prefixlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV4PrefixList', 'getIPV4PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let iPV6PrefixListObjId = 'fakedata';
    const iPV6PrefixListAddIPV6PrefixListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'ipprefixentry'
        }
      ],
      type: 'ipv6prefixlist'
    };
    describe('#addIPV6PrefixList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIPV6PrefixList(iPV6PrefixListAddIPV6PrefixListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('ipv6prefixlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              iPV6PrefixListObjId = data.response.id;
              saveMockData('IPV6PrefixList', 'addIPV6PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV6PrefixListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPV6PrefixListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV6PrefixList', 'getIPV6PrefixListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPV6PrefixListEditIPV6PrefixListBodyParam = {
      name: 'string',
      entries: [
        {
          type: 'ipprefixentry'
        }
      ],
      type: 'ipv6prefixlist'
    };
    describe('#editIPV6PrefixList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIPV6PrefixList(iPV6PrefixListObjId, iPV6PrefixListEditIPV6PrefixListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV6PrefixList', 'editIPV6PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV6PrefixList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPV6PrefixList(iPV6PrefixListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.id);
                assert.equal('ipv6prefixlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV6PrefixList', 'getIPV6PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let policyListObjId = 'fakedata';
    const policyListAddPolicyListBodyParam = {
      name: 'string',
      action: 'DENY',
      matchCommunityExactly: true,
      type: 'policylist'
    };
    describe('#addPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPolicyList(policyListAddPolicyListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('PERMIT', data.response.action);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal(true, Array.isArray(data.response.standardAccessListAddresses));
                assert.equal(true, Array.isArray(data.response.ipv4PrefixListAddresses));
                assert.equal(true, Array.isArray(data.response.standardAccessListNextHops));
                assert.equal(true, Array.isArray(data.response.ipv4PrefixListNextHops));
                assert.equal(true, Array.isArray(data.response.standardAccessListRouteSources));
                assert.equal(true, Array.isArray(data.response.ipv4PrefixListRouteSources));
                assert.equal(true, Array.isArray(data.response.asPathLists));
                assert.equal(true, Array.isArray(data.response.communityLists));
                assert.equal(true, data.response.matchCommunityExactly);
                assert.equal(4, data.response.metric);
                assert.equal(7, data.response.tag);
                assert.equal('string', data.response.id);
                assert.equal('policylist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              policyListObjId = data.response.id;
              saveMockData('PolicyList', 'addPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyList', 'getPolicyListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyListEditPolicyListBodyParam = {
      name: 'string',
      action: 'PERMIT',
      matchCommunityExactly: false,
      type: 'policylist'
    };
    describe('#editPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editPolicyList(policyListObjId, policyListEditPolicyListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyList', 'editPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyList(policyListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('PERMIT', data.response.action);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal(true, Array.isArray(data.response.standardAccessListAddresses));
                assert.equal(true, Array.isArray(data.response.ipv4PrefixListAddresses));
                assert.equal(true, Array.isArray(data.response.standardAccessListNextHops));
                assert.equal(true, Array.isArray(data.response.ipv4PrefixListNextHops));
                assert.equal(true, Array.isArray(data.response.standardAccessListRouteSources));
                assert.equal(true, Array.isArray(data.response.ipv4PrefixListRouteSources));
                assert.equal(true, Array.isArray(data.response.asPathLists));
                assert.equal(true, Array.isArray(data.response.communityLists));
                assert.equal(false, data.response.matchCommunityExactly);
                assert.equal(4, data.response.metric);
                assert.equal(5, data.response.tag);
                assert.equal('string', data.response.id);
                assert.equal('policylist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyList', 'getPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHardwareBypassList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHardwareBypassList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HardwareBypass', 'getHardwareBypassList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hardwareBypassObjId = 'fakedata';
    const hardwareBypassEditHardwareBypassBodyParam = {
      type: 'hardwarebypass'
    };
    describe('#editHardwareBypass - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editHardwareBypass(hardwareBypassObjId, hardwareBypassEditHardwareBypassBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HardwareBypass', 'editHardwareBypass', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHardwareBypass - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHardwareBypass(hardwareBypassObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('INTERFACEPAIR3_4', data.response.bypassPair);
                assert.equal(true, data.response.bypassPowerDown);
                assert.equal(true, data.response.bypassImmediately);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('string', data.response.id);
                assert.equal('hardwarebypass', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HardwareBypass', 'getHardwareBypass', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let applicationObjId = 'fakedata';
    const applicationAddApplicationFilterBodyParam = {
      name: 'string',
      type: 'applicationfilter'
    };
    describe('#addApplicationFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addApplicationFilter(applicationAddApplicationFilterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.applications));
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.conditions));
                assert.equal('applicationfilter', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              applicationObjId = data.response.id;
              saveMockData('Application', 'addApplicationFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationCategoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationCategoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationCategoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationFilterList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationFilterList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationFilterList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationEditApplicationFilterBodyParam = {
      name: 'string',
      type: 'applicationfilter'
    };
    describe('#editApplicationFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editApplicationFilter(applicationObjId, applicationEditApplicationFilterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'editApplicationFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationFilter(applicationObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.applications));
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.conditions));
                assert.equal('applicationfilter', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationTagList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationTagList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationTagList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCountryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Country', 'getCountryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const countryObjId = 'fakedata';
    describe('#getCountry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCountry(countryObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.code);
                assert.equal(true, data.response.deprecated);
                assert.equal('string', data.response.iso2);
                assert.equal('string', data.response.iso3);
                assert.equal('string', data.response.id);
                assert.equal('country', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Country', 'getCountry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContinentList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContinentList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Continent', 'getContinentList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const continentObjId = 'fakedata';
    describe('#getContinent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContinent(continentObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.code);
                assert.equal(true, data.response.deprecated);
                assert.equal(true, Array.isArray(data.response.countries));
                assert.equal(true, Array.isArray(data.response.countryCodes));
                assert.equal('string', data.response.id);
                assert.equal('continent', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Continent', 'getContinent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLCategoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLCategoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLCategory', 'getURLCategoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLReputationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLReputationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLReputation', 'getURLReputationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let geoLocationObjId = 'fakedata';
    const geoLocationAddGeoLocationBodyParam = {
      name: 'string',
      type: 'geolocation'
    };
    describe('#addGeoLocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addGeoLocation(geoLocationAddGeoLocationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.locations));
                assert.equal('string', data.response.id);
                assert.equal('geolocation', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              geoLocationObjId = data.response.id;
              saveMockData('GeoLocation', 'addGeoLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeoLocationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGeoLocationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeoLocation', 'getGeoLocationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const geoLocationEditGeoLocationBodyParam = {
      name: 'string',
      type: 'geolocation'
    };
    describe('#editGeoLocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editGeoLocation(geoLocationObjId, geoLocationEditGeoLocationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeoLocation', 'editGeoLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeoLocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGeoLocation(geoLocationObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.locations));
                assert.equal('string', data.response.id);
                assert.equal('geolocation', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeoLocation', 'getGeoLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let uRLObjectObjId = 'fakedata';
    const uRLObjectAddURLObjectGroupBodyParam = {
      name: 'string',
      type: 'urlobjectgroup'
    };
    describe('#addURLObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addURLObjectGroup(uRLObjectAddURLObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal('urlobjectgroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              uRLObjectObjId = data.response.id;
              saveMockData('URLObject', 'addURLObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uRLObjectAddURLObjectBodyParam = {
      name: 'string',
      url: 'string',
      type: 'urlobject'
    };
    describe('#addURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addURLObject(uRLObjectAddURLObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.id);
                assert.equal('urlobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'addURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObjectGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLObjectGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'getURLObjectGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uRLObjectEditURLObjectGroupBodyParam = {
      name: 'string',
      type: 'urlobjectgroup'
    };
    describe('#editURLObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editURLObjectGroup(uRLObjectObjId, uRLObjectEditURLObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'editURLObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLObjectGroup(uRLObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal('urlobjectgroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'getURLObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'getURLObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uRLObjectEditURLObjectBodyParam = {
      name: 'string',
      url: 'string',
      type: 'urlobject'
    };
    describe('#editURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editURLObject(uRLObjectObjId, uRLObjectEditURLObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'editURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLObject(uRLObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.id);
                assert.equal('urlobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'getURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let deploymentObjId = 'fakedata';
    describe('#addDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDeployment((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.cliErrorMessage);
                assert.equal('DEPLOY_FAILED', data.response.state);
                assert.equal(8, data.response.queuedTime);
                assert.equal(3, data.response.startTime);
                assert.equal(3, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.statusMessages));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.modifiedObjects);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              deploymentObjId = data.response.id;
              saveMockData('Deployment', 'addDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeploymentList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'getDeploymentList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeployment(deploymentObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.cliErrorMessage);
                assert.equal('DEPLOYING', data.response.state);
                assert.equal(7, data.response.queuedTime);
                assert.equal(1, data.response.startTime);
                assert.equal(9, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.statusMessages));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.modifiedObjects);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'getDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configIssueObjId = 'fakedata';
    describe('#getConfigIssue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigIssue(configIssueObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.configIssueMessages));
                assert.equal('string', data.response.id);
                assert.equal('ConfigIssue', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigIssue', 'getConfigIssue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let joinHAStatusObjId = 'fakedata';
    describe('#addJoinHAStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addJoinHAStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.cliErrorMessage);
                assert.equal('DEPLOYED', data.response.state);
                assert.equal(1, data.response.queuedTime);
                assert.equal(1, data.response.startTime);
                assert.equal(2, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.statusMessages));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.modifiedObjects);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              joinHAStatusObjId = data.response.id;
              saveMockData('JoinHAStatus', 'addJoinHAStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJoinHAStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJoinHAStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JoinHAStatus', 'getJoinHAStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJoinHAStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJoinHAStatus(joinHAStatusObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.cliErrorMessage);
                assert.equal('DEPLOYED', data.response.state);
                assert.equal(8, data.response.queuedTime);
                assert.equal(8, data.response.startTime);
                assert.equal(7, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.statusMessages));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.modifiedObjects);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JoinHAStatus', 'getJoinHAStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let breakHAStatusObjId = 'fakedata';
    const breakHAStatusAddBreakHAStatusBodyParam = {
      type: 'breakhastatus'
    };
    describe('#addBreakHAStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBreakHAStatus(null, breakHAStatusAddBreakHAStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.interfaceOption);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.cliErrorMessage);
                assert.equal('DEPLOYED', data.response.state);
                assert.equal(8, data.response.queuedTime);
                assert.equal(8, data.response.startTime);
                assert.equal(1, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.statusMessages));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.modifiedObjects);
                assert.equal('breakhastatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              breakHAStatusObjId = data.response.id;
              saveMockData('BreakHAStatus', 'addBreakHAStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBreakHAStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBreakHAStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BreakHAStatus', 'getBreakHAStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBreakHAStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBreakHAStatus(breakHAStatusObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.interfaceOption);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.cliErrorMessage);
                assert.equal('DEPLOYED', data.response.state);
                assert.equal(4, data.response.queuedTime);
                assert.equal(5, data.response.startTime);
                assert.equal(10, data.response.endTime);
                assert.equal(true, Array.isArray(data.response.statusMessages));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.modifiedObjects);
                assert.equal('breakhastatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BreakHAStatus', 'getBreakHAStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startHAFailover - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startHAFailover((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('HA_PRIMARY', data.response.nodeRole);
                assert.equal('HA_FAILED_NODE', data.response.nodeState);
                assert.equal('HA_SUSPENDED_NODE', data.response.peerNodeState);
                assert.equal('PRIMARY_IMPORTING_CONFIG', data.response.configStatus);
                assert.equal('PROCESSING', data.response.haHealthStatus);
                assert.equal('string', data.response.disabledReason);
                assert.equal('string', data.response.disabledTimestamp);
                assert.equal('string', data.response.id);
                assert.equal('HAStatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAAction', 'startHAFailover', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addHAStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addHAStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('HA_PRIMARY', data.response.nodeRole);
                assert.equal('HA_SUSPENDED_NODE', data.response.nodeState);
                assert.equal('HA_ACTIVE_NODE', data.response.peerNodeState);
                assert.equal('IN_SYNC', data.response.configStatus);
                assert.equal('HEALTHY', data.response.haHealthStatus);
                assert.equal('string', data.response.disabledReason);
                assert.equal('string', data.response.disabledTimestamp);
                assert.equal('string', data.response.id);
                assert.equal('HAStatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAAction', 'addHAStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startHAResume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startHAResume((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('HA_PRIMARY', data.response.nodeRole);
                assert.equal('HA_UNKNOWN_NODE', data.response.nodeState);
                assert.equal('HA_UNKNOWN_NODE', data.response.peerNodeState);
                assert.equal('IN_SYNC', data.response.configStatus);
                assert.equal('PROCESSING', data.response.haHealthStatus);
                assert.equal('string', data.response.disabledReason);
                assert.equal('string', data.response.disabledTimestamp);
                assert.equal('string', data.response.id);
                assert.equal('HAStatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAAction', 'startHAResume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startHASuspend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startHASuspend((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('HA_PRIMARY', data.response.nodeRole);
                assert.equal('HA_CONFIGURATION_SYNC', data.response.nodeState);
                assert.equal('HA_STANDBY_NODE', data.response.peerNodeState);
                assert.equal('PRIMARY_IMPORTING_CONFIG', data.response.configStatus);
                assert.equal('PROCESSING', data.response.haHealthStatus);
                assert.equal('string', data.response.disabledReason);
                assert.equal('string', data.response.disabledTimestamp);
                assert.equal('string', data.response.id);
                assert.equal('HAStatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAAction', 'startHASuspend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentDataObjId = 'fakedata';
    describe('#getDeploymentData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeploymentData(deploymentDataObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.numberOfObjectsToDeploy);
                assert.equal('object', typeof data.response.deployedConfigChecksum);
                assert.equal(true, data.response.forceDeployment);
                assert.equal(false, data.response.configError);
                assert.equal('string', data.response.id);
                assert.equal('DeploymentData', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeploymentData', 'getDeploymentData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trafficInterruptionReasonsObjId = 'fakedata';
    describe('#getTrafficInterruptionReasons - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrafficInterruptionReasons(trafficInterruptionReasonsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.reasons));
                assert.equal('string', data.response.id);
                assert.equal('TrafficInterruptionReasons', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrafficInterruptionReasons', 'getTrafficInterruptionReasons', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pendingChangesObjId = 'fakedata';
    describe('#getdownload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownload(pendingChangesObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PendingChanges', 'getdownload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBaseEntityDiffList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBaseEntityDiffList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PendingChanges', 'getBaseEntityDiffList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClipboard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getClipboard(pendingChangesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.id);
                assert.equal('Clipboard', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PendingChanges', 'getClipboard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const downloadObjId = 'fakedata';
    describe('#getdownloadbackup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadbackup(downloadObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Download', 'getdownloadbackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadconfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadconfig(downloadObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Download', 'getdownloadconfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloaddiskfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloaddiskfile(downloadObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Download', 'getdownloaddiskfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadinternalcacertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadinternalcacertificate(downloadObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Download', 'getdownloadinternalcacertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadtroubleshoot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadtroubleshoot(downloadObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Download', 'getdownloadtroubleshoot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scheduleExportConfigAddScheduleExportConfigBodyParam = {
      type: 'scheduleexportconfig'
    };
    describe('#addScheduleExportConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addScheduleExportConfig(scheduleExportConfigAddScheduleExportConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal(false, data.response.masked);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('scheduleexportconfig', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScheduleExportConfig', 'addScheduleExportConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleExportConfigList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleExportConfigList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScheduleExportConfig', 'getScheduleExportConfigList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExportConfigJobHistoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExportConfigJobHistoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExportConfigJobHistory', 'getExportConfigJobHistoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exportConfigJobHistoryObjId = 'fakedata';
    describe('#getExportConfigJobHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExportConfigJobHistory(exportConfigJobHistoryObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('IN_PROGRESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('exportconfigjobhistory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExportConfigJobHistory', 'getExportConfigJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditEventList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditEventList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditEvent', 'getAuditEventList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditEventObjId = 'fakedata';
    describe('#getAuditEvent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditEvent(auditEventObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.sourceIp);
                assert.equal('string', data.response.userName);
                assert.equal(1, data.response.timeStamp);
                assert.equal('object', typeof data.response.eventData);
                assert.equal('string', data.response.id);
                assert.equal('auditevent', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditEvent', 'getAuditEvent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditEntityChangeParentId = 'fakedata';
    describe('#getAuditEntityChangeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditEntityChangeList(null, null, null, null, auditEntityChangeParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditEntityChange', 'getAuditEntityChangeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cliDeploymentErrorObjId = 'fakedata';
    describe('#getCliDeploymentError - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCliDeploymentError(cliDeploymentErrorObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStatusUuid);
                assert.equal('string', data.response.cliErrorMessage);
                assert.equal('FLEX_CLI', data.response.blockType);
                assert.equal('string', data.response.blockUuid);
                assert.equal(5, data.response.errorLineId);
                assert.equal(false, data.response.negateLine);
                assert.equal(4, data.response.blockStartLineId);
                assert.equal(2, data.response.blockEndLineId);
                assert.equal(true, Array.isArray(data.response.cliLines));
                assert.equal('string', data.response.id);
                assert.equal('CliDeploymentError', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CliDeploymentError', 'getCliDeploymentError', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let backupImmediateObjId = 'fakedata';
    const backupImmediateAddBackupImmediateBodyParam = {
      type: 'backupimmediate'
    };
    describe('#addBackupImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBackupImmediate(backupImmediateAddBackupImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('IMMEDIATE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('SDCARD', data.response.backupLocation);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('backupimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              backupImmediateObjId = data.response.id;
              saveMockData('BackupImmediate', 'addBackupImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBackupImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupImmediate', 'getBackupImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const backupImmediateEditBackupImmediateBodyParam = {
      type: 'backupimmediate'
    };
    describe('#editBackupImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editBackupImmediate(backupImmediateObjId, backupImmediateEditBackupImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupImmediate', 'editBackupImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBackupImmediate(backupImmediateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('IMMEDIATE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('DEFAULT', data.response.backupLocation);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('backupimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupImmediate', 'getBackupImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let backupScheduledObjId = 'fakedata';
    const backupScheduledAddBackupScheduledBodyParam = {
      type: 'backupscheduled'
    };
    describe('#addBackupScheduled - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBackupScheduled(backupScheduledAddBackupScheduledBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('DAILY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('DEFAULT', data.response.backupLocation);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('backupscheduled', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              backupScheduledObjId = data.response.id;
              saveMockData('BackupScheduled', 'addBackupScheduled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupScheduledList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBackupScheduledList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupScheduled', 'getBackupScheduledList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const backupScheduledEditBackupScheduledBodyParam = {
      type: 'backupscheduled'
    };
    describe('#editBackupScheduled - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editBackupScheduled(backupScheduledObjId, backupScheduledEditBackupScheduledBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupScheduled', 'editBackupScheduled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupScheduled - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBackupScheduled(backupScheduledObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('DEFAULT', data.response.backupLocation);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('backupscheduled', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupScheduled', 'getBackupScheduled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let restoreImmediateObjId = 'fakedata';
    const restoreImmediateAddRestoreImmediateBodyParam = {
      archiveName: 'string',
      type: 'restoreimmediate'
    };
    describe('#addRestoreImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRestoreImmediate(restoreImmediateAddRestoreImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('HOURLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.archiveName);
                assert.equal(false, data.response.preserveArchive);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('restoreimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              restoreImmediateObjId = data.response.id;
              saveMockData('RestoreImmediate', 'addRestoreImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestoreImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestoreImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestoreImmediate', 'getRestoreImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restoreImmediateEditRestoreImmediateBodyParam = {
      archiveName: 'string',
      type: 'restoreimmediate'
    };
    describe('#editRestoreImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editRestoreImmediate(restoreImmediateObjId, restoreImmediateEditRestoreImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestoreImmediate', 'editRestoreImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestoreImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestoreImmediate(restoreImmediateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('CRON', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.archiveName);
                assert.equal(true, data.response.preserveArchive);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('restoreimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestoreImmediate', 'getRestoreImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArchivedBackupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getArchivedBackupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArchivedBackup', 'getArchivedBackupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const archivedBackupObjId = 'fakedata';
    describe('#getArchivedBackup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getArchivedBackup(archivedBackupObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDate);
                assert.equal(9, data.response.fileSize);
                assert.equal('string', data.response.archiveName);
                assert.equal('CRON', data.response.originalScheduleType);
                assert.equal('DEFAULT', data.response.backupLocation);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.softwareVersion);
                assert.equal('string', data.response.vdbVersion);
                assert.equal('string', data.response.serialNumber);
                assert.equal('string', data.response.modelNumber);
                assert.equal('string', data.response.modelId);
                assert.equal('string', data.response.buildVersion);
                assert.equal('string', data.response.schemaVersion);
                assert.equal('string', data.response.id);
                assert.equal('archivedbackup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArchivedBackup', 'getArchivedBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let geolocationUpdateImmediateObjId = 'fakedata';
    const geolocationUpdateImmediateAddGeolocationUpdateImmediateBodyParam = {
      type: 'geolocationupdateimmediate'
    };
    describe('#addGeolocationUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addGeolocationUpdateImmediate(geolocationUpdateImmediateAddGeolocationUpdateImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('NONE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('geolocationupdateimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              geolocationUpdateImmediateObjId = data.response.id;
              saveMockData('GeolocationUpdateImmediate', 'addGeolocationUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGeolocationUpdateImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateImmediate', 'getGeolocationUpdateImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const geolocationUpdateImmediateEditGeolocationUpdateImmediateBodyParam = {
      type: 'geolocationupdateimmediate'
    };
    describe('#editGeolocationUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editGeolocationUpdateImmediate(geolocationUpdateImmediateObjId, geolocationUpdateImmediateEditGeolocationUpdateImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateImmediate', 'editGeolocationUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGeolocationUpdateImmediate(geolocationUpdateImmediateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('NONE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('geolocationupdateimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateImmediate', 'getGeolocationUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let geolocationUpdateScheduleObjId = 'fakedata';
    const geolocationUpdateScheduleAddGeolocationUpdateScheduleBodyParam = {
      type: 'geolocationupdateschedule'
    };
    describe('#addGeolocationUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addGeolocationUpdateSchedule(geolocationUpdateScheduleAddGeolocationUpdateScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('WEEKLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('geolocationupdateschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              geolocationUpdateScheduleObjId = data.response.id;
              saveMockData('GeolocationUpdateSchedule', 'addGeolocationUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateScheduleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGeolocationUpdateScheduleList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateSchedule', 'getGeolocationUpdateScheduleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const geolocationUpdateScheduleEditGeolocationUpdateScheduleBodyParam = {
      type: 'geolocationupdateschedule'
    };
    describe('#editGeolocationUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editGeolocationUpdateSchedule(geolocationUpdateScheduleObjId, geolocationUpdateScheduleEditGeolocationUpdateScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateSchedule', 'editGeolocationUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGeolocationUpdateSchedule(geolocationUpdateScheduleObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('CRON', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('geolocationupdateschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateSchedule', 'getGeolocationUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postupdategeolocationfromfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postupdategeolocationfromfile(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.checkSum);
                assert.equal('string', data.response.id);
                assert.equal('geolocationfileupload', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeoLocationFileUpload', 'postupdategeolocationfromfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sRUUpdateImmediateObjId = 'fakedata';
    const sRUUpdateImmediateAddSRUUpdateImmediateBodyParam = {
      type: 'sruupdateimmediate'
    };
    describe('#addSRUUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSRUUpdateImmediate(sRUUpdateImmediateAddSRUUpdateImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('CRON', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('SRU_OOB_UPDATE', data.response.sruImmediateJobType);
                assert.equal(false, data.response.forceUpdate);
                assert.equal(false, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('sruupdateimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              sRUUpdateImmediateObjId = data.response.id;
              saveMockData('SRUUpdateImmediate', 'addSRUUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSRUUpdateImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateImmediate', 'getSRUUpdateImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sRUUpdateImmediateEditSRUUpdateImmediateBodyParam = {
      type: 'sruupdateimmediate'
    };
    describe('#editSRUUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSRUUpdateImmediate(sRUUpdateImmediateObjId, sRUUpdateImmediateEditSRUUpdateImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateImmediate', 'editSRUUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSRUUpdateImmediate(sRUUpdateImmediateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('DAILY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('SRU_OOB_UPDATE', data.response.sruImmediateJobType);
                assert.equal(true, data.response.forceUpdate);
                assert.equal(false, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('sruupdateimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateImmediate', 'getSRUUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sRUUpdateScheduleObjId = 'fakedata';
    const sRUUpdateScheduleAddSRUUpdateScheduleBodyParam = {
      type: 'sruupdateschedule'
    };
    describe('#addSRUUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSRUUpdateSchedule(sRUUpdateScheduleAddSRUUpdateScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('DAILY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('sruupdateschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              sRUUpdateScheduleObjId = data.response.id;
              saveMockData('SRUUpdateSchedule', 'addSRUUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateScheduleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSRUUpdateScheduleList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateSchedule', 'getSRUUpdateScheduleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sRUUpdateScheduleEditSRUUpdateScheduleBodyParam = {
      type: 'sruupdateschedule'
    };
    describe('#editSRUUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSRUUpdateSchedule(sRUUpdateScheduleObjId, sRUUpdateScheduleEditSRUUpdateScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateSchedule', 'editSRUUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSRUUpdateSchedule(sRUUpdateScheduleObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('SINGLE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('sruupdateschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateSchedule', 'getSRUUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSRUFileUpload - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSRUFileUpload(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.checkSum);
                assert.equal(true, data.response.uploadOnly);
                assert.equal('string', data.response.id);
                assert.equal('srufileupload', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUFileUpload', 'addSRUFileUpload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let vDBUpdateImmediateObjId = 'fakedata';
    const vDBUpdateImmediateAddVDBUpdateImmediateBodyParam = {
      type: 'vdbupdateimmediate'
    };
    describe('#addVDBUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addVDBUpdateImmediate(vDBUpdateImmediateAddVDBUpdateImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('DAILY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('vdbupdateimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              vDBUpdateImmediateObjId = data.response.id;
              saveMockData('VDBUpdateImmediate', 'addVDBUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVDBUpdateImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateImmediate', 'getVDBUpdateImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vDBUpdateImmediateEditVDBUpdateImmediateBodyParam = {
      type: 'vdbupdateimmediate'
    };
    describe('#editVDBUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editVDBUpdateImmediate(vDBUpdateImmediateObjId, vDBUpdateImmediateEditVDBUpdateImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateImmediate', 'editVDBUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVDBUpdateImmediate(vDBUpdateImmediateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('IMMEDIATE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('vdbupdateimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateImmediate', 'getVDBUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let vDBUpdateScheduleObjId = 'fakedata';
    const vDBUpdateScheduleAddVDBUpdateScheduleBodyParam = {
      type: 'vdbupdateschedule'
    };
    describe('#addVDBUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addVDBUpdateSchedule(vDBUpdateScheduleAddVDBUpdateScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('IMMEDIATE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('vdbupdateschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              vDBUpdateScheduleObjId = data.response.id;
              saveMockData('VDBUpdateSchedule', 'addVDBUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateScheduleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVDBUpdateScheduleList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateSchedule', 'getVDBUpdateScheduleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vDBUpdateScheduleEditVDBUpdateScheduleBodyParam = {
      type: 'vdbupdateschedule'
    };
    describe('#editVDBUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editVDBUpdateSchedule(vDBUpdateScheduleObjId, vDBUpdateScheduleEditVDBUpdateScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateSchedule', 'editVDBUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVDBUpdateSchedule(vDBUpdateScheduleObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('SINGLE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.deployAfterUpdate);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('vdbupdateschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateSchedule', 'getVDBUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postupdatevdbfromfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postupdatevdbfromfile(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.checkSum);
                assert.equal('string', data.response.id);
                assert.equal('vdbfileupload', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBFileUpload', 'postupdatevdbfromfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullUpgradeJobList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPullUpgradeJobList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullUpgradeJob', 'getPullUpgradeJobList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullUpgradeJobObjId = 'fakedata';
    describe('#getPullUpgradeJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPullUpgradeJob(pullUpgradeJobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('FAILED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('pullupgradejob', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullUpgradeJob', 'getPullUpgradeJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullFileJobList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPullFileJobList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullFileJob', 'getPullFileJobList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullFileJobObjId = 'fakedata';
    describe('#getPullFileJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPullFileJob(pullFileJobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('IN_PROGRESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.id);
                assert.equal('pullfilejob', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullFileJob', 'getPullFileJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryEntityList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryEntityList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryEntityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryBackupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryBackupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryBackupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobObjId = 'fakedata';
    describe('#getJobHistoryBackup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryBackup(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.archiveName);
                assert.equal('string', data.response.id);
                assert.equal('jobhistorybackup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryCloudManagementList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryCloudManagementList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryCloudManagementList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryCloudManagement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryCloudManagement(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('jobhistorycloudmanagement', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryCloudManagement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryDeploymentList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryDeploymentList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryDeploymentList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryDeployment(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('QUEUED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.deploymentStatusUuid);
                assert.equal('string', data.response.id);
                assert.equal('jobhistorydeployment', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryGeolocationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryGeolocationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryGeolocationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryGeolocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryGeolocation(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('FAILED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('jobhistorygeolocation', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryGeolocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryHaConfigSyncList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryHaConfigSyncList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryHaConfigSyncList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryHaConfigSync - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryHaConfigSync(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('QUEUED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.dataBaseFileName);
                assert.equal('string', data.response.sruWorkingTgz);
                assert.equal(9, data.response.transactionId);
                assert.equal(false, data.response.restore);
                assert.equal('string', data.response.id);
                assert.equal('jobhistoryhaconfigsync', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryHaConfigSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryEntity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryEntity(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('SUCCESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('jobhistoryentity', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryEntity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseJobHistoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicenseJobHistoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getLicenseJobHistoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseJobHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicenseJobHistory(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('FAILED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('licensejobhistory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getLicenseJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistorySruUpdateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistorySruUpdateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistorySruUpdateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistorySruUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistorySruUpdate(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('QUEUED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('jobhistorysruupdate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistorySruUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryVDBUpdateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryVDBUpdateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryVDBUpdateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryVDBUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobHistoryVDBUpdate(jobObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('IN_PROGRESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('jobhistoryvdbupdate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'getJobHistoryVDBUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let raVpnGroupPolicyObjId = 'fakedata';
    const raVpnGroupPolicyAddRaVpnGroupPolicyBodyParam = {
      type: 'ravpngrouppolicy'
    };
    describe('#addRaVpnGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRaVpnGroupPolicy(raVpnGroupPolicyAddRaVpnGroupPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.banner);
                assert.equal('object', typeof data.response.dnsServerGroup);
                assert.equal('string', data.response.defaultDomainName);
                assert.equal(5, data.response.simultaneousLoginPerUser);
                assert.equal(7, data.response.maxConnectionTimeout);
                assert.equal(8, data.response.maxConnectionTimeAlertInterval);
                assert.equal(2, data.response.vpnIdleTimeout);
                assert.equal(5, data.response.vpnIdleTimeoutAlertInterval);
                assert.equal(true, Array.isArray(data.response.ipv4LocalAddressPool));
                assert.equal(true, Array.isArray(data.response.ipv6LocalAddressPool));
                assert.equal('object', typeof data.response.dhcpScope);
                assert.equal('TUNNEL_SPECIFIED', data.response.ipv4SplitTunnelSetting);
                assert.equal('EXCLUDE_SPECIFIED_OVER_TUNNEL', data.response.ipv6SplitTunnelSetting);
                assert.equal(true, Array.isArray(data.response.ipv4SplitTunnelNetworks));
                assert.equal(true, Array.isArray(data.response.ipv6SplitTunnelNetworks));
                assert.equal('USE_SPLIT_TUNNEL_SETTING', data.response.splitDNSRequestPolicy);
                assert.equal('string', data.response.splitDNSDomainList);
                assert.equal('string', data.response.scepForwardingUrl);
                assert.equal(9, data.response.periodicClientCertAuthenticationInterval);
                assert.equal(false, data.response.enableDTLS);
                assert.equal(false, data.response.enableDTLSCompression);
                assert.equal('LZS', data.response.sslCompression);
                assert.equal(false, data.response.enableSSLrekey);
                assert.equal('EXISTING_TUNNEL', data.response.rekeyMethod);
                assert.equal(1, data.response.rekeyInterval);
                assert.equal(true, data.response.ignoreDFBit);
                assert.equal(false, data.response.bypassUnsupportProtocol);
                assert.equal(4, data.response.mtuSize);
                assert.equal(false, data.response.useAlwaysOnVPNSettingInProfile);
                assert.equal(true, data.response.enableKeepAliveMessages);
                assert.equal(2, data.response.keepAliveMessageInterval);
                assert.equal(false, data.response.enableGatewayDPD);
                assert.equal(4, data.response.gatewayDPDInterval);
                assert.equal(true, data.response.enableClientDPD);
                assert.equal(10, data.response.clientDPDInterval);
                assert.equal(true, Array.isArray(data.response.clientProfiles));
                assert.equal(true, data.response.keepInstallerOnClient);
                assert.equal('object', typeof data.response.vpnTrafficFilterACL);
                assert.equal(false, data.response.enableRestrictVPNToVLAN);
                assert.equal(3, data.response.restrictVPNToVLANId);
                assert.equal('object', typeof data.response.clientFirewallPrivateNetworkRules);
                assert.equal('object', typeof data.response.clientFirewallPublicNetworkRules);
                assert.equal('NO_MODIFY', data.response.browserProxyType);
                assert.equal('object', typeof data.response.proxy);
                assert.equal(true, Array.isArray(data.response.proxyExceptions));
                assert.equal(false, data.response.isEnablePeriodicClientCertAuthentication);
                assert.equal('string', data.response.id);
                assert.equal('ravpngrouppolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              raVpnGroupPolicyObjId = data.response.id;
              saveMockData('RaVpnGroupPolicy', 'addRaVpnGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnGroupPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaVpnGroupPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnGroupPolicy', 'getRaVpnGroupPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const raVpnGroupPolicyEditRaVpnGroupPolicyBodyParam = {
      type: 'ravpngrouppolicy'
    };
    describe('#editRaVpnGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editRaVpnGroupPolicy(raVpnGroupPolicyObjId, raVpnGroupPolicyEditRaVpnGroupPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnGroupPolicy', 'editRaVpnGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaVpnGroupPolicy(raVpnGroupPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.banner);
                assert.equal('object', typeof data.response.dnsServerGroup);
                assert.equal('string', data.response.defaultDomainName);
                assert.equal(3, data.response.simultaneousLoginPerUser);
                assert.equal(3, data.response.maxConnectionTimeout);
                assert.equal(9, data.response.maxConnectionTimeAlertInterval);
                assert.equal(7, data.response.vpnIdleTimeout);
                assert.equal(2, data.response.vpnIdleTimeoutAlertInterval);
                assert.equal(true, Array.isArray(data.response.ipv4LocalAddressPool));
                assert.equal(true, Array.isArray(data.response.ipv6LocalAddressPool));
                assert.equal('object', typeof data.response.dhcpScope);
                assert.equal('TUNNEL_ALL', data.response.ipv4SplitTunnelSetting);
                assert.equal('EXCLUDE_SPECIFIED_OVER_TUNNEL', data.response.ipv6SplitTunnelSetting);
                assert.equal(true, Array.isArray(data.response.ipv4SplitTunnelNetworks));
                assert.equal(true, Array.isArray(data.response.ipv6SplitTunnelNetworks));
                assert.equal('TUNNEL_ALL', data.response.splitDNSRequestPolicy);
                assert.equal('string', data.response.splitDNSDomainList);
                assert.equal('string', data.response.scepForwardingUrl);
                assert.equal(4, data.response.periodicClientCertAuthenticationInterval);
                assert.equal(true, data.response.enableDTLS);
                assert.equal(true, data.response.enableDTLSCompression);
                assert.equal('DEFLATE', data.response.sslCompression);
                assert.equal(false, data.response.enableSSLrekey);
                assert.equal('NEW_TUNNEL', data.response.rekeyMethod);
                assert.equal(7, data.response.rekeyInterval);
                assert.equal(false, data.response.ignoreDFBit);
                assert.equal(true, data.response.bypassUnsupportProtocol);
                assert.equal(5, data.response.mtuSize);
                assert.equal(true, data.response.useAlwaysOnVPNSettingInProfile);
                assert.equal(true, data.response.enableKeepAliveMessages);
                assert.equal(9, data.response.keepAliveMessageInterval);
                assert.equal(false, data.response.enableGatewayDPD);
                assert.equal(7, data.response.gatewayDPDInterval);
                assert.equal(true, data.response.enableClientDPD);
                assert.equal(1, data.response.clientDPDInterval);
                assert.equal(true, Array.isArray(data.response.clientProfiles));
                assert.equal(false, data.response.keepInstallerOnClient);
                assert.equal('object', typeof data.response.vpnTrafficFilterACL);
                assert.equal(true, data.response.enableRestrictVPNToVLAN);
                assert.equal(1, data.response.restrictVPNToVLANId);
                assert.equal('object', typeof data.response.clientFirewallPrivateNetworkRules);
                assert.equal('object', typeof data.response.clientFirewallPublicNetworkRules);
                assert.equal('AUTO_DETECT', data.response.browserProxyType);
                assert.equal('object', typeof data.response.proxy);
                assert.equal(true, Array.isArray(data.response.proxyExceptions));
                assert.equal(true, data.response.isEnablePeriodicClientCertAuthentication);
                assert.equal('string', data.response.id);
                assert.equal('ravpngrouppolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnGroupPolicy', 'getRaVpnGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let ldapAttributeMapObjId = 'fakedata';
    const ldapAttributeMapAddLdapAttributeMapBodyParam = {
      name: 'string',
      type: 'ldapattributemap'
    };
    describe('#addLdapAttributeMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addLdapAttributeMap(ldapAttributeMapAddLdapAttributeMapBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.ldapAttributeMaps));
                assert.equal(true, Array.isArray(data.response.ldapAttributeToGroupPolicyMappings));
                assert.equal('string', data.response.id);
                assert.equal('ldapattributemap', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              ldapAttributeMapObjId = data.response.id;
              saveMockData('LdapAttributeMap', 'addLdapAttributeMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapAttributeMapList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLdapAttributeMapList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LdapAttributeMap', 'getLdapAttributeMapList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ldapAttributeMapEditLdapAttributeMapBodyParam = {
      name: 'string',
      type: 'ldapattributemap'
    };
    describe('#editLdapAttributeMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editLdapAttributeMap(ldapAttributeMapObjId, ldapAttributeMapEditLdapAttributeMapBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LdapAttributeMap', 'editLdapAttributeMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapAttributeMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLdapAttributeMap(ldapAttributeMapObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.ldapAttributeMaps));
                assert.equal(true, Array.isArray(data.response.ldapAttributeToGroupPolicyMappings));
                assert.equal('string', data.response.id);
                assert.equal('ldapattributemap', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LdapAttributeMap', 'getLdapAttributeMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let anyConnectPackageFileObjId = 'fakedata';
    const anyConnectPackageFileAddAnyConnectPackageFileBodyParam = {
      name: 'string',
      platformType: 'MACOS',
      type: 'anyconnectpackagefile'
    };
    describe('#addAnyConnectPackageFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAnyConnectPackageFile(anyConnectPackageFileAddAnyConnectPackageFileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.md5Checksum);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.diskFileName);
                assert.equal('WINDOWS', data.response.platformType);
                assert.equal('string', data.response.id);
                assert.equal('anyconnectpackagefile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              anyConnectPackageFileObjId = data.response.id;
              saveMockData('AnyConnectPackageFile', 'addAnyConnectPackageFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectPackageFileList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAnyConnectPackageFileList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectPackageFile', 'getAnyConnectPackageFileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const anyConnectPackageFileEditAnyConnectPackageFileBodyParam = {
      name: 'string',
      platformType: 'LINUX',
      type: 'anyconnectpackagefile'
    };
    describe('#editAnyConnectPackageFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAnyConnectPackageFile(anyConnectPackageFileObjId, anyConnectPackageFileEditAnyConnectPackageFileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectPackageFile', 'editAnyConnectPackageFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectPackageFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAnyConnectPackageFile(anyConnectPackageFileObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.md5Checksum);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.diskFileName);
                assert.equal('LINUX', data.response.platformType);
                assert.equal('string', data.response.id);
                assert.equal('anyconnectpackagefile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectPackageFile', 'getAnyConnectPackageFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let raVpnConnectionProfileParentId = 'fakedata';
    let raVpnConnectionProfileObjId = 'fakedata';
    const raVpnConnectionProfileAddRaVpnConnectionProfileBodyParam = {
      name: 'string',
      defaultGroupPolicy: {
        type: 'string',
        name: 'string'
      },
      authMethod: 'CLIENT_CERTIFICATE',
      stripGroupFromUsername: false,
      stripRealmFromUsername: false,
      type: 'ravpnconnectionprofile'
    };
    describe('#addRaVpnConnectionProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRaVpnConnectionProfile(raVpnConnectionProfileParentId, raVpnConnectionProfileAddRaVpnConnectionProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.defaultGroupPolicy);
                assert.equal('object', typeof data.response.authenticationIdentitySource);
                assert.equal('object', typeof data.response.authorizationIdentitySource);
                assert.equal('object', typeof data.response.accountingIdentitySource);
                assert.equal('object', typeof data.response.fallbackLocalIdentitySource);
                assert.equal('AAA', data.response.authMethod);
                assert.equal('object', typeof data.response.certificateUsernameSettings);
                assert.equal('object', typeof data.response.secondaryAuthenticationSettings);
                assert.equal(false, data.response.stripGroupFromUsername);
                assert.equal(false, data.response.stripRealmFromUsername);
                assert.equal(true, Array.isArray(data.response.ipv4LocalAddressPool));
                assert.equal(true, Array.isArray(data.response.ipv6LocalAddressPool));
                assert.equal(true, Array.isArray(data.response.dhcpServersForAddressAssignment));
                assert.equal(true, Array.isArray(data.response.groupAlias));
                assert.equal(true, Array.isArray(data.response.groupUrl));
                assert.equal('string', data.response.id);
                assert.equal('ravpnconnectionprofile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              raVpnConnectionProfileParentId = data.response.id;
              raVpnConnectionProfileObjId = data.response.id;
              saveMockData('RaVpnConnectionProfile', 'addRaVpnConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnConnectionProfileList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaVpnConnectionProfileList(null, null, null, null, raVpnConnectionProfileParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnConnectionProfile', 'getRaVpnConnectionProfileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const raVpnConnectionProfileEditRaVpnConnectionProfileBodyParam = {
      name: 'string',
      defaultGroupPolicy: {
        type: 'string',
        name: 'string'
      },
      authMethod: 'CLIENT_CERTIFICATE',
      stripGroupFromUsername: false,
      stripRealmFromUsername: false,
      type: 'ravpnconnectionprofile'
    };
    describe('#editRaVpnConnectionProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editRaVpnConnectionProfile(raVpnConnectionProfileParentId, raVpnConnectionProfileObjId, raVpnConnectionProfileEditRaVpnConnectionProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnConnectionProfile', 'editRaVpnConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnConnectionProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaVpnConnectionProfile(raVpnConnectionProfileParentId, raVpnConnectionProfileObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.defaultGroupPolicy);
                assert.equal('object', typeof data.response.authenticationIdentitySource);
                assert.equal('object', typeof data.response.authorizationIdentitySource);
                assert.equal('object', typeof data.response.accountingIdentitySource);
                assert.equal('object', typeof data.response.fallbackLocalIdentitySource);
                assert.equal('AAA', data.response.authMethod);
                assert.equal('object', typeof data.response.certificateUsernameSettings);
                assert.equal('object', typeof data.response.secondaryAuthenticationSettings);
                assert.equal(false, data.response.stripGroupFromUsername);
                assert.equal(false, data.response.stripRealmFromUsername);
                assert.equal(true, Array.isArray(data.response.ipv4LocalAddressPool));
                assert.equal(true, Array.isArray(data.response.ipv6LocalAddressPool));
                assert.equal(true, Array.isArray(data.response.dhcpServersForAddressAssignment));
                assert.equal(true, Array.isArray(data.response.groupAlias));
                assert.equal(true, Array.isArray(data.response.groupUrl));
                assert.equal('string', data.response.id);
                assert.equal('ravpnconnectionprofile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnConnectionProfile', 'getRaVpnConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let raVpnObjId = 'fakedata';
    const raVpnAddRaVpnBodyParam = {
      name: 'string',
      vpnGatewaySettings: {
        type: 'vpngatewaysettings'
      },
      type: 'ravpn'
    };
    describe('#addRaVpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRaVpn(raVpnAddRaVpnBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.vpnGatewaySettings);
                assert.equal(true, Array.isArray(data.response.groupPolicies));
                assert.equal(true, Array.isArray(data.response.anyconnectPackageFiles));
                assert.equal('string', data.response.id);
                assert.equal('ravpn', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              raVpnObjId = data.response.id;
              saveMockData('RaVpn', 'addRaVpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaVpnList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpn', 'getRaVpnList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const raVpnEditRaVpnBodyParam = {
      name: 'string',
      vpnGatewaySettings: {
        type: 'vpngatewaysettings'
      },
      type: 'ravpn'
    };
    describe('#editRaVpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editRaVpn(raVpnObjId, raVpnEditRaVpnBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpn', 'editRaVpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRaVpn(raVpnObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.vpnGatewaySettings);
                assert.equal(true, Array.isArray(data.response.groupPolicies));
                assert.equal(true, Array.isArray(data.response.anyconnectPackageFiles));
                assert.equal('string', data.response.id);
                assert.equal('ravpn', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpn', 'getRaVpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let accessPolicyParentId = 'fakedata';
    const accessPolicyAddAccessRuleBodyParam = {
      name: 'string',
      type: 'accessrule'
    };
    describe('#addAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAccessRule(null, accessPolicyParentId, accessPolicyAddAccessRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.ruleId);
                assert.equal(true, Array.isArray(data.response.sourceZones));
                assert.equal(true, Array.isArray(data.response.destinationZones));
                assert.equal(true, Array.isArray(data.response.sourceNetworks));
                assert.equal(true, Array.isArray(data.response.destinationNetworks));
                assert.equal(true, Array.isArray(data.response.sourcePorts));
                assert.equal(true, Array.isArray(data.response.destinationPorts));
                assert.equal(10, data.response.rulePosition);
                assert.equal('TRUST', data.response.ruleAction);
                assert.equal('LOG_BOTH', data.response.eventLogAction);
                assert.equal(true, Array.isArray(data.response.identitySources));
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('object', typeof data.response.embeddedAppFilter);
                assert.equal('object', typeof data.response.urlFilter);
                assert.equal('object', typeof data.response.intrusionPolicy);
                assert.equal('object', typeof data.response.filePolicy);
                assert.equal(true, data.response.logFiles);
                assert.equal('object', typeof data.response.syslogServer);
                assert.equal('object', typeof data.response.hitCount);
                assert.equal(true, Array.isArray(data.response.destinationDynamicObjects));
                assert.equal(true, Array.isArray(data.response.sourceDynamicObjects));
                assert.equal(true, Array.isArray(data.response.timeRangeObjects));
                assert.equal('string', data.response.id);
                assert.equal('accessrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              accessPolicyParentId = data.response.id;
              saveMockData('AccessPolicy', 'addAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicy', 'getAccessPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accessPolicyObjId = 'fakedata';
    const accessPolicyEditAccessPolicyBodyParam = {
      name: 'string',
      type: 'accesspolicy'
    };
    describe('#editAccessPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAccessPolicy(accessPolicyObjId, accessPolicyEditAccessPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicy', 'editAccessPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessPolicy(accessPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.defaultAction);
                assert.equal('object', typeof data.response.sslPolicy);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.identityPolicySetting);
                assert.equal('object', typeof data.response.securityIntelligence);
                assert.equal('accesspolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicy', 'getAccessPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessRuleList(null, null, null, null, null, accessPolicyParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicy', 'getAccessRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accessPolicyEditAccessRuleBodyParam = {
      name: 'string',
      type: 'accessrule'
    };
    describe('#editAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAccessRule(null, accessPolicyParentId, accessPolicyObjId, accessPolicyEditAccessRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicy', 'editAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessRule(null, accessPolicyParentId, accessPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.ruleId);
                assert.equal(true, Array.isArray(data.response.sourceZones));
                assert.equal(true, Array.isArray(data.response.destinationZones));
                assert.equal(true, Array.isArray(data.response.sourceNetworks));
                assert.equal(true, Array.isArray(data.response.destinationNetworks));
                assert.equal(true, Array.isArray(data.response.sourcePorts));
                assert.equal(true, Array.isArray(data.response.destinationPorts));
                assert.equal(10, data.response.rulePosition);
                assert.equal('PERMIT', data.response.ruleAction);
                assert.equal('LOG_FLOW_END', data.response.eventLogAction);
                assert.equal(true, Array.isArray(data.response.identitySources));
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('object', typeof data.response.embeddedAppFilter);
                assert.equal('object', typeof data.response.urlFilter);
                assert.equal('object', typeof data.response.intrusionPolicy);
                assert.equal('object', typeof data.response.filePolicy);
                assert.equal(true, data.response.logFiles);
                assert.equal('object', typeof data.response.syslogServer);
                assert.equal('object', typeof data.response.hitCount);
                assert.equal(true, Array.isArray(data.response.destinationDynamicObjects));
                assert.equal(true, Array.isArray(data.response.sourceDynamicObjects));
                assert.equal(true, Array.isArray(data.response.timeRangeObjects));
                assert.equal('string', data.response.id);
                assert.equal('accessrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicy', 'getAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let nATParentId = 'fakedata';
    const nATAddManualNatRuleBodyParam = {
      name: 'string',
      natType: 'STATIC',
      type: 'manualnatrule'
    };
    describe('#addManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addManualNatRule(null, nATParentId, nATAddManualNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal('DYNAMIC', data.response.natType);
                assert.equal('object', typeof data.response.patOptions);
                assert.equal(true, data.response.netToNet);
                assert.equal(false, data.response.noProxyArp);
                assert.equal(true, data.response.dns);
                assert.equal(true, data.response.interfaceIPv6);
                assert.equal(true, data.response.routeLookup);
                assert.equal(false, data.response.enabled);
                assert.equal(true, data.response.interfaceInOriginalDestination);
                assert.equal(true, data.response.interfaceInTranslatedSource);
                assert.equal('object', typeof data.response.originalSource);
                assert.equal('object', typeof data.response.originalDestination);
                assert.equal('object', typeof data.response.originalSourcePort);
                assert.equal('object', typeof data.response.originalDestinationPort);
                assert.equal('object', typeof data.response.translatedSource);
                assert.equal('object', typeof data.response.translatedDestination);
                assert.equal('object', typeof data.response.translatedSourcePort);
                assert.equal('object', typeof data.response.translatedDestinationPort);
                assert.equal(true, data.response.unidirectional);
                assert.equal(10, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('manualnatrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              nATParentId = data.response.id;
              saveMockData('NAT', 'addManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATAddObjectNatRuleBodyParam = {
      name: 'string',
      natType: 'STATIC',
      type: 'objectnatrule'
    };
    describe('#addObjectNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addObjectNatRule(null, nATParentId, nATAddObjectNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal('STATIC', data.response.natType);
                assert.equal('object', typeof data.response.patOptions);
                assert.equal(true, data.response.netToNet);
                assert.equal(false, data.response.noProxyArp);
                assert.equal(false, data.response.dns);
                assert.equal(false, data.response.interfaceIPv6);
                assert.equal(true, data.response.routeLookup);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.originalNetwork);
                assert.equal('object', typeof data.response.translatedNetwork);
                assert.equal('object', typeof data.response.originalPort);
                assert.equal('object', typeof data.response.translatedPort);
                assert.equal(true, data.response.interfaceInTranslatedNetwork);
                assert.equal(4, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('objectnatrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'addObjectNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManualNatRuleContainerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManualNatRuleContainerList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getManualNatRuleContainerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATObjId = 'fakedata';
    describe('#getManualNatRuleContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManualNatRuleContainer(nATObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.afterAuto);
                assert.equal('string', data.response.id);
                assert.equal('manualnatrulecontainer', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getManualNatRuleContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManualNatRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManualNatRuleList(null, null, null, null, nATParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getManualNatRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATEditManualNatRuleBodyParam = {
      name: 'string',
      natType: 'DYNAMIC',
      type: 'manualnatrule'
    };
    describe('#editManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editManualNatRule(null, nATParentId, nATObjId, nATEditManualNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'editManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManualNatRule(nATParentId, nATObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal('DYNAMIC', data.response.natType);
                assert.equal('object', typeof data.response.patOptions);
                assert.equal(false, data.response.netToNet);
                assert.equal(true, data.response.noProxyArp);
                assert.equal(true, data.response.dns);
                assert.equal(false, data.response.interfaceIPv6);
                assert.equal(false, data.response.routeLookup);
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.interfaceInOriginalDestination);
                assert.equal(false, data.response.interfaceInTranslatedSource);
                assert.equal('object', typeof data.response.originalSource);
                assert.equal('object', typeof data.response.originalDestination);
                assert.equal('object', typeof data.response.originalSourcePort);
                assert.equal('object', typeof data.response.originalDestinationPort);
                assert.equal('object', typeof data.response.translatedSource);
                assert.equal('object', typeof data.response.translatedDestination);
                assert.equal('object', typeof data.response.translatedSourcePort);
                assert.equal('object', typeof data.response.translatedDestinationPort);
                assert.equal(false, data.response.unidirectional);
                assert.equal(6, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('manualnatrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRuleContainerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectNatRuleContainerList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getObjectNatRuleContainerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRuleContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectNatRuleContainer(nATObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('objectnatrulecontainer', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getObjectNatRuleContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectNatRuleList(null, null, null, null, nATParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getObjectNatRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATEditObjectNatRuleBodyParam = {
      name: 'string',
      natType: 'DYNAMIC',
      type: 'objectnatrule'
    };
    describe('#editObjectNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editObjectNatRule(null, nATParentId, nATObjId, nATEditObjectNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'editObjectNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectNatRule(nATParentId, nATObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal('STATIC', data.response.natType);
                assert.equal('object', typeof data.response.patOptions);
                assert.equal(false, data.response.netToNet);
                assert.equal(false, data.response.noProxyArp);
                assert.equal(true, data.response.dns);
                assert.equal(false, data.response.interfaceIPv6);
                assert.equal(false, data.response.routeLookup);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.originalNetwork);
                assert.equal('object', typeof data.response.translatedNetwork);
                assert.equal('object', typeof data.response.originalPort);
                assert.equal('object', typeof data.response.translatedPort);
                assert.equal(false, data.response.interfaceInTranslatedNetwork);
                assert.equal(2, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('objectnatrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'getObjectNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadbackup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postuploadbackup(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.id);
                assert.equal('fileuploadstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Upload', 'postuploadbackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploaddiskfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postuploaddiskfile(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.id);
                assert.equal('fileuploadstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Upload', 'postuploaddiskfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadupgrade - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postuploadupgrade(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.id);
                assert.equal('fileuploadstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Upload', 'postuploadupgrade', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let anyConnectClientProfileObjId = 'fakedata';
    const anyConnectClientProfileAddAnyConnectClientProfileBodyParam = {
      name: 'string',
      type: 'anyconnectclientprofile'
    };
    describe('#addAnyConnectClientProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAnyConnectClientProfile(anyConnectClientProfileAddAnyConnectClientProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.md5Checksum);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.diskFileName);
                assert.equal('string', data.response.id);
                assert.equal('anyconnectclientprofile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              anyConnectClientProfileObjId = data.response.id;
              saveMockData('AnyConnectClientProfile', 'addAnyConnectClientProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectClientProfileList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAnyConnectClientProfileList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectClientProfile', 'getAnyConnectClientProfileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const anyConnectClientProfileEditAnyConnectClientProfileBodyParam = {
      name: 'string',
      type: 'anyconnectclientprofile'
    };
    describe('#editAnyConnectClientProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAnyConnectClientProfile(anyConnectClientProfileObjId, anyConnectClientProfileEditAnyConnectClientProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectClientProfile', 'editAnyConnectClientProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectClientProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAnyConnectClientProfile(anyConnectClientProfileObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.md5Checksum);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.diskFileName);
                assert.equal('string', data.response.id);
                assert.equal('anyconnectclientprofile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectClientProfile', 'getAnyConnectClientProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let ikevOnePolicyObjId = 'fakedata';
    const ikevOnePolicyAddIkevOnePolicyBodyParam = {
      name: 'string',
      enabled: true,
      priority: 4,
      authenticationType: 'PRE_SHARED_KEY',
      encryptionType: 'THREE_DES',
      hashType: 'MD5',
      groupType: 'GROUP1',
      type: 'ikevonepolicy'
    };
    describe('#addIkevOnePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIkevOnePolicy(ikevOnePolicyAddIkevOnePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.enabled);
                assert.equal(1, data.response.lifeTime);
                assert.equal(2, data.response.priority);
                assert.equal('PRE_SHARED_KEY', data.response.authenticationType);
                assert.equal('AES', data.response.encryptionType);
                assert.equal('SHA', data.response.hashType);
                assert.equal('GROUP2', data.response.groupType);
                assert.equal(false, data.response.cryptoRestricted);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('ikevonepolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              ikevOnePolicyObjId = data.response.id;
              saveMockData('IkevOnePolicy', 'addIkevOnePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOnePolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevOnePolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOnePolicy', 'getIkevOnePolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ikevOnePolicyEditIkevOnePolicyBodyParam = {
      name: 'string',
      enabled: true,
      priority: 5,
      authenticationType: 'PRE_SHARED_KEY',
      encryptionType: 'AES256',
      hashType: 'SHA',
      groupType: 'GROUP14',
      type: 'ikevonepolicy'
    };
    describe('#editIkevOnePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIkevOnePolicy(ikevOnePolicyObjId, ikevOnePolicyEditIkevOnePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOnePolicy', 'editIkevOnePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOnePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevOnePolicy(ikevOnePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.enabled);
                assert.equal(7, data.response.lifeTime);
                assert.equal(10, data.response.priority);
                assert.equal('PRE_SHARED_KEY', data.response.authenticationType);
                assert.equal('AES', data.response.encryptionType);
                assert.equal('MD5', data.response.hashType);
                assert.equal('GROUP1', data.response.groupType);
                assert.equal(false, data.response.cryptoRestricted);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('ikevonepolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOnePolicy', 'getIkevOnePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let ikevOneProposalObjId = 'fakedata';
    const ikevOneProposalAddIkevOneProposalBodyParam = {
      name: 'string',
      encryptionMethod: 'ESP_AES',
      authenticationMethod: 'ESP_NONE',
      mode: 'TRANSPORT',
      type: 'ikevoneproposal'
    };
    describe('#addIkevOneProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIkevOneProposal(ikevOneProposalAddIkevOneProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('ESP_DES', data.response.encryptionMethod);
                assert.equal('ESP_NONE', data.response.authenticationMethod);
                assert.equal('TUNNEL', data.response.mode);
                assert.equal(false, data.response.cryptoRestricted);
                assert.equal(false, data.response.defaultAssignable);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('ikevoneproposal', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              ikevOneProposalObjId = data.response.id;
              saveMockData('IkevOneProposal', 'addIkevOneProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOneProposalList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevOneProposalList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOneProposal', 'getIkevOneProposalList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ikevOneProposalEditIkevOneProposalBodyParam = {
      name: 'string',
      encryptionMethod: 'ESP_NULL',
      authenticationMethod: 'ESP_MD5_HMAC',
      mode: 'TRANSPORT',
      type: 'ikevoneproposal'
    };
    describe('#editIkevOneProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIkevOneProposal(ikevOneProposalObjId, ikevOneProposalEditIkevOneProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOneProposal', 'editIkevOneProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOneProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevOneProposal(ikevOneProposalObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('ESP_AES', data.response.encryptionMethod);
                assert.equal('ESP_NONE', data.response.authenticationMethod);
                assert.equal('TRANSPORT', data.response.mode);
                assert.equal(true, data.response.cryptoRestricted);
                assert.equal(false, data.response.defaultAssignable);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('ikevoneproposal', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOneProposal', 'getIkevOneProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let ikevTwoPolicyObjId = 'fakedata';
    const ikevTwoPolicyAddIkevTwoPolicyBodyParam = {
      name: 'string',
      enabled: true,
      priority: 8,
      type: 'ikevtwopolicy'
    };
    describe('#addIkevTwoPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIkevTwoPolicy(ikevTwoPolicyAddIkevTwoPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.enabled);
                assert.equal(1, data.response.lifeTime);
                assert.equal(5, data.response.priority);
                assert.equal(true, Array.isArray(data.response.encryptionTypes));
                assert.equal(true, Array.isArray(data.response.groupTypes));
                assert.equal(true, Array.isArray(data.response.integrityTypes));
                assert.equal(true, Array.isArray(data.response.prfTypes));
                assert.equal(false, data.response.cryptoRestricted);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('ikevtwopolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              ikevTwoPolicyObjId = data.response.id;
              saveMockData('IkevTwoPolicy', 'addIkevTwoPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevTwoPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoPolicy', 'getIkevTwoPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ikevTwoPolicyEditIkevTwoPolicyBodyParam = {
      name: 'string',
      enabled: false,
      priority: 8,
      type: 'ikevtwopolicy'
    };
    describe('#editIkevTwoPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIkevTwoPolicy(ikevTwoPolicyObjId, ikevTwoPolicyEditIkevTwoPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoPolicy', 'editIkevTwoPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevTwoPolicy(ikevTwoPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.enabled);
                assert.equal(6, data.response.lifeTime);
                assert.equal(2, data.response.priority);
                assert.equal(true, Array.isArray(data.response.encryptionTypes));
                assert.equal(true, Array.isArray(data.response.groupTypes));
                assert.equal(true, Array.isArray(data.response.integrityTypes));
                assert.equal(true, Array.isArray(data.response.prfTypes));
                assert.equal(true, data.response.cryptoRestricted);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('ikevtwopolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoPolicy', 'getIkevTwoPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let ikevTwoProposalObjId = 'fakedata';
    const ikevTwoProposalAddIkevTwoProposalBodyParam = {
      name: 'string',
      type: 'ikevtwoproposal'
    };
    describe('#addIkevTwoProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIkevTwoProposal(ikevTwoProposalAddIkevTwoProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.encryptionTypes));
                assert.equal(true, Array.isArray(data.response.integrityTypes));
                assert.equal(true, data.response.cryptoRestricted);
                assert.equal(true, data.response.defaultAssignable);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('ikevtwoproposal', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              ikevTwoProposalObjId = data.response.id;
              saveMockData('IkevTwoProposal', 'addIkevTwoProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoProposalList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevTwoProposalList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoProposal', 'getIkevTwoProposalList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ikevTwoProposalEditIkevTwoProposalBodyParam = {
      name: 'string',
      type: 'ikevtwoproposal'
    };
    describe('#editIkevTwoProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIkevTwoProposal(ikevTwoProposalObjId, ikevTwoProposalEditIkevTwoProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoProposal', 'editIkevTwoProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkevTwoProposal(ikevTwoProposalObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.encryptionTypes));
                assert.equal(true, Array.isArray(data.response.integrityTypes));
                assert.equal(false, data.response.cryptoRestricted);
                assert.equal(false, data.response.defaultAssignable);
                assert.equal('string', data.response.summaryLabel);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('ikevtwoproposal', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoProposal', 'getIkevTwoProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sToSConnectionProfileObjId = 'fakedata';
    const sToSConnectionProfileAddSToSConnectionProfileBodyParam = {
      name: 'string',
      outsideInterfaces: [
        {
          type: 'string',
          name: 'string'
        }
      ],
      ikev1AuthMethod: 'CERTIFICATE',
      ikev2AuthMethod: 'CERTIFICATE',
      ikev1Enabled: false,
      ikev2Enabled: false,
      rriEnabled: false,
      dynamicRRIEnabled: true,
      ipsecLifetimeInSeconds: 8,
      ipsecLifetimeInKiloBytes: 9,
      ipsecLifetimeUnlimited: true,
      type: 'stosconnectionprofile'
    };
    describe('#addSToSConnectionProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSToSConnectionProfile(sToSConnectionProfileAddSToSConnectionProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.outsideInterfaces));
                assert.equal(true, Array.isArray(data.response.localNetworks));
                assert.equal(true, data.response.isRemotePeerIpDynamic);
                assert.equal('PRE_SHARED_KEY', data.response.ikev1AuthMethod);
                assert.equal('CERTIFICATE', data.response.ikev2AuthMethod);
                assert.equal('object', typeof data.response.ikev1IDCertificate);
                assert.equal('object', typeof data.response.ikev2IDCertificate);
                assert.equal('string', data.response.remotePeerIpAddress);
                assert.equal(true, Array.isArray(data.response.remoteBackupPeers));
                assert.equal(true, Array.isArray(data.response.remoteNetworks));
                assert.equal(true, data.response.ikev1Enabled);
                assert.equal(true, data.response.ikev2Enabled);
                assert.equal(true, data.response.rriEnabled);
                assert.equal(true, data.response.dynamicRRIEnabled);
                assert.equal('string', data.response.ikev1PreSharedKey);
                assert.equal('GROUP14', data.response.diffieHellmanGroup);
                assert.equal('string', data.response.ikev2LocalPreSharedKey);
                assert.equal('string', data.response.ikev2RemotePeerPreSharedKey);
                assert.equal(true, Array.isArray(data.response.ikev1Proposals));
                assert.equal(true, Array.isArray(data.response.ikev2Proposals));
                assert.equal(6, data.response.ipsecLifetimeInSeconds);
                assert.equal(1, data.response.ipsecLifetimeInKiloBytes);
                assert.equal(false, data.response.ipsecLifetimeUnlimited);
                assert.equal('object', typeof data.response.interfaceForNatExempt);
                assert.equal('string', data.response.id);
                assert.equal('stosconnectionprofile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              sToSConnectionProfileObjId = data.response.id;
              saveMockData('SToSConnectionProfile', 'addSToSConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSToSConnectionProfileList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSToSConnectionProfileList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SToSConnectionProfile', 'getSToSConnectionProfileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sToSConnectionProfileEditSToSConnectionProfileBodyParam = {
      name: 'string',
      outsideInterfaces: [
        {
          type: 'string',
          name: 'string'
        }
      ],
      ikev1AuthMethod: 'CERTIFICATE',
      ikev2AuthMethod: 'PRE_SHARED_KEY',
      ikev1Enabled: true,
      ikev2Enabled: true,
      rriEnabled: true,
      dynamicRRIEnabled: false,
      ipsecLifetimeInSeconds: 1,
      ipsecLifetimeInKiloBytes: 10,
      ipsecLifetimeUnlimited: true,
      type: 'stosconnectionprofile'
    };
    describe('#editSToSConnectionProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSToSConnectionProfile(sToSConnectionProfileObjId, sToSConnectionProfileEditSToSConnectionProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SToSConnectionProfile', 'editSToSConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSToSConnectionProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSToSConnectionProfile(sToSConnectionProfileObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.outsideInterfaces));
                assert.equal(true, Array.isArray(data.response.localNetworks));
                assert.equal(true, data.response.isRemotePeerIpDynamic);
                assert.equal('CERTIFICATE', data.response.ikev1AuthMethod);
                assert.equal('PRE_SHARED_KEY', data.response.ikev2AuthMethod);
                assert.equal('object', typeof data.response.ikev1IDCertificate);
                assert.equal('object', typeof data.response.ikev2IDCertificate);
                assert.equal('string', data.response.remotePeerIpAddress);
                assert.equal(true, Array.isArray(data.response.remoteBackupPeers));
                assert.equal(true, Array.isArray(data.response.remoteNetworks));
                assert.equal(true, data.response.ikev1Enabled);
                assert.equal(true, data.response.ikev2Enabled);
                assert.equal(false, data.response.rriEnabled);
                assert.equal(false, data.response.dynamicRRIEnabled);
                assert.equal('string', data.response.ikev1PreSharedKey);
                assert.equal('GROUP16', data.response.diffieHellmanGroup);
                assert.equal('string', data.response.ikev2LocalPreSharedKey);
                assert.equal('string', data.response.ikev2RemotePeerPreSharedKey);
                assert.equal(true, Array.isArray(data.response.ikev1Proposals));
                assert.equal(true, Array.isArray(data.response.ikev2Proposals));
                assert.equal(8, data.response.ipsecLifetimeInSeconds);
                assert.equal(7, data.response.ipsecLifetimeInKiloBytes);
                assert.equal(false, data.response.ipsecLifetimeUnlimited);
                assert.equal('object', typeof data.response.interfaceForNatExempt);
                assert.equal('string', data.response.id);
                assert.equal('stosconnectionprofile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SToSConnectionProfile', 'getSToSConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let userObjId = 'fakedata';
    const userAddUserBodyParam = {
      name: 'string',
      identitySourceId: 'string',
      userServiceTypes: [
        'MGMT'
      ],
      type: 'user'
    };
    describe('#addUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addUser(userAddUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.password);
                assert.equal('*********', data.response.newPassword);
                assert.equal('object', typeof data.response.userPreferences);
                assert.equal('string', data.response.userRole);
                assert.equal('string', data.response.identitySourceId);
                assert.equal(true, Array.isArray(data.response.userServiceTypes));
                assert.equal('string', data.response.id);
                assert.equal('user', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              userObjId = data.response.id;
              saveMockData('User', 'addUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUserList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userEditUserBodyParam = {
      name: 'string',
      identitySourceId: 'string',
      userServiceTypes: [
        'MGMT'
      ],
      type: 'user'
    };
    describe('#editUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editUser(userObjId, userEditUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'editUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUser(userObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.password);
                assert.equal('*********', data.response.newPassword);
                assert.equal('object', typeof data.response.userPreferences);
                assert.equal('string', data.response.userRole);
                assert.equal('string', data.response.identitySourceId);
                assert.equal(true, Array.isArray(data.response.userServiceTypes));
                assert.equal('string', data.response.id);
                assert.equal('user', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNTPList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNTPList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NTP', 'getNTPList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nTPObjId = 'fakedata';
    const nTPEditNTPBodyParam = {
      ntpServers: [
        'string'
      ],
      type: 'ntp'
    };
    describe('#editNTP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editNTP(nTPObjId, nTPEditNTPBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NTP', 'editNTP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNTP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNTP(nTPObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.ntpServers));
                assert.equal('string', data.response.id);
                assert.equal('ntp', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NTP', 'getNTP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLogSettingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceLogSettingsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceLogSettings', 'getDeviceLogSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceLogSettingsObjId = 'fakedata';
    const deviceLogSettingsEditDeviceLogSettingsBodyParam = {
      type: 'devicelogsettings'
    };
    describe('#editDeviceLogSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDeviceLogSettings(deviceLogSettingsObjId, deviceLogSettingsEditDeviceLogSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceLogSettings', 'editDeviceLogSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLogSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceLogSettings(deviceLogSettingsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.consoleLogFilter);
                assert.equal('object', typeof data.response.syslogServerLogFilter);
                assert.equal('object', typeof data.response.bufferedLogging);
                assert.equal(false, data.response.deviceLoggingEnabled);
                assert.equal('string', data.response.id);
                assert.equal('devicelogsettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceLogSettings', 'getDeviceLogSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let syslogServerObjId = 'fakedata';
    const syslogServerAddSyslogServerBodyParam = {
      protocol: 'TCP',
      host: 'string',
      type: 'syslogserver'
    };
    describe('#addSyslogServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSyslogServer(syslogServerAddSyslogServerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.deviceInterface);
                assert.equal(true, data.response.useManagementInterface);
                assert.equal('TCP', data.response.protocol);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.host);
                assert.equal('string', data.response.port);
                assert.equal('syslogserver', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              syslogServerObjId = data.response.id;
              saveMockData('SyslogServer', 'addSyslogServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogServerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSyslogServerList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyslogServer', 'getSyslogServerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syslogServerEditSyslogServerBodyParam = {
      protocol: 'UDP',
      host: 'string',
      type: 'syslogserver'
    };
    describe('#editSyslogServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSyslogServer(syslogServerObjId, syslogServerEditSyslogServerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyslogServer', 'editSyslogServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSyslogServer(syslogServerObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.deviceInterface);
                assert.equal(true, data.response.useManagementInterface);
                assert.equal('UDP', data.response.protocol);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.host);
                assert.equal('string', data.response.port);
                assert.equal('syslogserver', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyslogServer', 'getSyslogServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSAddDNSServerGroupBodyParam = {
      name: 'string',
      dnsServers: [
        {
          type: 'dnsserver'
        }
      ],
      type: 'dnsservergroup'
    };
    describe('#addDNSServerGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDNSServerGroup(dNSAddDNSServerGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.dnsServers));
                assert.equal(9, data.response.timeout);
                assert.equal(4, data.response.retries);
                assert.equal('string', data.response.searchDomain);
                assert.equal(false, data.response.systemDefined);
                assert.equal('string', data.response.id);
                assert.equal('dnsservergroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'addDNSServerGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataDNSSettingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataDNSSettingsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDataDNSSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSObjId = 'fakedata';
    const dNSEditDataDNSSettingsBodyParam = {
      name: 'string',
      type: 'datadnssettings'
    };
    describe('#editDataDNSSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDataDNSSettings(dNSObjId, dNSEditDataDNSSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'editDataDNSSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataDNSSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataDNSSettings(dNSObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.pollTimer);
                assert.equal(10, data.response.expiryEntryTimer);
                assert.equal('object', typeof data.response.dnsServerGroup);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('datadnssettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDataDNSSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDNSSettingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDNSSettingsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDeviceDNSSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSEditDeviceDNSSettingsBodyParam = {
      name: 'string',
      type: 'devicednssettings'
    };
    describe('#editDeviceDNSSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDeviceDNSSettings(dNSObjId, dNSEditDeviceDNSSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'editDeviceDNSSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDNSSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDNSSettings(dNSObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.dnsServerGroup);
                assert.equal('string', data.response.id);
                assert.equal('devicednssettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDeviceDNSSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServerGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDNSServerGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDNSServerGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSEditDNSServerGroupBodyParam = {
      name: 'string',
      dnsServers: [
        {
          type: 'dnsserver'
        }
      ],
      type: 'dnsservergroup'
    };
    describe('#editDNSServerGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDNSServerGroup(dNSObjId, dNSEditDNSServerGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'editDNSServerGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServerGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDNSServerGroup(dNSObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.dnsServers));
                assert.equal(4, data.response.timeout);
                assert.equal(6, data.response.retries);
                assert.equal('string', data.response.searchDomain);
                assert.equal(true, data.response.systemDefined);
                assert.equal('string', data.response.id);
                assert.equal('dnsservergroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDNSServerGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSLCipherAddSSLCipherBodyParam = {
      name: 'string',
      protocolVersions: [
        'TLSV1_2'
      ],
      securityLevel: 'LOW',
      type: 'sslcipher'
    };
    describe('#addSSLCipher - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSSLCipher(sSLCipherAddSSLCipherBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.protocolVersions));
                assert.equal('CUSTOM', data.response.securityLevel);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('MEDIUM', data.response.inferredSecurityLevel);
                assert.equal(true, Array.isArray(data.response.cipherSuites));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('sslcipher', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'addSSLCipher', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataSSLCipherSettingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataSSLCipherSettingList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'getDataSSLCipherSettingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSLCipherObjId = 'fakedata';
    const sSLCipherEditDataSSLCipherSettingBodyParam = {
      name: 'string',
      sslCiphers: [
        {
          type: 'string',
          name: 'string'
        }
      ],
      type: 'datasslciphersetting'
    };
    describe('#editDataSSLCipherSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDataSSLCipherSetting(sSLCipherObjId, sSLCipherEditDataSSLCipherSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'editDataSSLCipherSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataSSLCipherSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDataSSLCipherSetting(sSLCipherObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('GROUP_14', data.response.diffieHellmanGroup);
                assert.equal('GROUP_20', data.response.ellipticalCurveDHGroup);
                assert.equal(true, Array.isArray(data.response.sslCiphers));
                assert.equal('string', data.response.id);
                assert.equal('datasslciphersetting', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'getDataSSLCipherSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLCipherList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSLCipherList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'getSSLCipherList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSLCipherEditSSLCipherBodyParam = {
      name: 'string',
      protocolVersions: [
        'TLSV1_1'
      ],
      securityLevel: 'CUSTOM',
      type: 'sslcipher'
    };
    describe('#editSSLCipher - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSSLCipher(sSLCipherObjId, sSLCipherEditSSLCipherBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'editSSLCipher', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLCipher - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSLCipher(sSLCipherObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.protocolVersions));
                assert.equal('ALL', data.response.securityLevel);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('CUSTOM', data.response.inferredSecurityLevel);
                assert.equal(true, Array.isArray(data.response.cipherSuites));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('sslcipher', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'getSSLCipher', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpenSSLCipherInfoList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpenSSLCipherInfoList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'getOpenSSLCipherInfoList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let identityPolicyParentId = 'fakedata';
    const identityPolicyAddIdentityRuleBodyParam = {
      name: 'string',
      action: 'PASSIVE',
      type: 'identityrule'
    };
    describe('#addIdentityRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIdentityRule(null, identityPolicyParentId, identityPolicyAddIdentityRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.ruleId);
                assert.equal(true, Array.isArray(data.response.sourceZones));
                assert.equal(true, Array.isArray(data.response.destinationZones));
                assert.equal(true, Array.isArray(data.response.sourceNetworks));
                assert.equal(true, Array.isArray(data.response.destinationNetworks));
                assert.equal(true, Array.isArray(data.response.sourcePorts));
                assert.equal(true, Array.isArray(data.response.destinationPorts));
                assert.equal('object', typeof data.response.realm);
                assert.equal(true, data.response.guestAccessFallback);
                assert.equal('HTTP_BASIC', data.response.authType);
                assert.equal('ACTIVE', data.response.action);
                assert.equal(true, data.response.enabled);
                assert.equal(2, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('identityrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              identityPolicyParentId = data.response.id;
              saveMockData('IdentityPolicy', 'addIdentityRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPolicy', 'getIdentityPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityPolicyObjId = 'fakedata';
    const identityPolicyEditIdentityPolicyBodyParam = {
      name: 'string',
      activeAuthPort: 3,
      type: 'identitypolicy'
    };
    describe('#editIdentityPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIdentityPolicy(identityPolicyObjId, identityPolicyEditIdentityPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPolicy', 'editIdentityPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityPolicy(identityPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.activeAuthPort);
                assert.equal('object', typeof data.response.defaultIdentityRule);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.activeAuthCertificate);
                assert.equal('identitypolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPolicy', 'getIdentityPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityRuleList(null, null, null, null, identityPolicyParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPolicy', 'getIdentityRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityPolicyEditIdentityRuleBodyParam = {
      name: 'string',
      action: 'NO_AUTH',
      type: 'identityrule'
    };
    describe('#editIdentityRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIdentityRule(null, identityPolicyParentId, identityPolicyObjId, identityPolicyEditIdentityRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPolicy', 'editIdentityRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityRule(identityPolicyParentId, identityPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.ruleId);
                assert.equal(true, Array.isArray(data.response.sourceZones));
                assert.equal(true, Array.isArray(data.response.destinationZones));
                assert.equal(true, Array.isArray(data.response.sourceNetworks));
                assert.equal(true, Array.isArray(data.response.destinationNetworks));
                assert.equal(true, Array.isArray(data.response.sourcePorts));
                assert.equal(true, Array.isArray(data.response.destinationPorts));
                assert.equal('object', typeof data.response.realm);
                assert.equal(true, data.response.guestAccessFallback);
                assert.equal('HTTP_RESPONSE_PAGE', data.response.authType);
                assert.equal('ACTIVE', data.response.action);
                assert.equal(true, data.response.enabled);
                assert.equal(5, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('identityrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPolicy', 'getIdentityRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let certificateObjId = 'fakedata';
    const certificateAddExternalCACertificateBodyParam = {
      name: 'string',
      type: 'externalcacertificate'
    };
    describe('#addExternalCACertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addExternalCACertificate(certificateAddExternalCACertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('externalcacertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              certificateObjId = data.response.id;
              saveMockData('Certificate', 'addExternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateAddExternalCertificateBodyParam = {
      name: 'string',
      type: 'externalcertificate'
    };
    describe('#addExternalCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addExternalCertificate(certificateAddExternalCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('externalcertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'addExternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateAddInternalCACertificateBodyParam = {
      name: 'string',
      type: 'internalcacertificate'
    };
    describe('#addInternalCACertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addInternalCACertificate(certificateAddInternalCACertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('SELFSIGNED', data.response.certType);
                assert.equal('string', data.response.id);
                assert.equal('internalcacertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'addInternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateAddInternalCertificateBodyParam = {
      name: 'string',
      type: 'internalcertificate'
    };
    describe('#addInternalCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addInternalCertificate(certificateAddInternalCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('UPLOAD', data.response.certType);
                assert.equal('string', data.response.id);
                assert.equal('internalcertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'addInternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCACertificateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExternalCACertificateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getExternalCACertificateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEditExternalCACertificateBodyParam = {
      name: 'string',
      type: 'externalcacertificate'
    };
    describe('#editExternalCACertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editExternalCACertificate(certificateObjId, certificateEditExternalCACertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'editExternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCACertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExternalCACertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('externalcacertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getExternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCertificateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExternalCertificateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getExternalCertificateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEditExternalCertificateBodyParam = {
      name: 'string',
      type: 'externalcertificate'
    };
    describe('#editExternalCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editExternalCertificate(certificateObjId, certificateEditExternalCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'editExternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExternalCertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('externalcertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getExternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCACertificateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInternalCACertificateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getInternalCACertificateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEditInternalCACertificateBodyParam = {
      name: 'string',
      type: 'internalcacertificate'
    };
    describe('#editInternalCACertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editInternalCACertificate(certificateObjId, certificateEditInternalCACertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'editInternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCACertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInternalCACertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('UPLOAD', data.response.certType);
                assert.equal('string', data.response.id);
                assert.equal('internalcacertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getInternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCertificateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInternalCertificateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getInternalCertificateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificateEditInternalCertificateBodyParam = {
      name: 'string',
      type: 'internalcertificate'
    };
    describe('#editInternalCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editInternalCertificate(certificateObjId, certificateEditInternalCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'editInternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInternalCertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('*********', data.response.cert);
                assert.equal('*********', data.response.privateKey);
                assert.equal('string', data.response.passPhrase);
                assert.equal('string', data.response.issuerCommonName);
                assert.equal('string', data.response.issuerCountry);
                assert.equal('string', data.response.issuerLocality);
                assert.equal('string', data.response.issuerOrganization);
                assert.equal('string', data.response.issuerOrganizationUnit);
                assert.equal('string', data.response.issuerState);
                assert.equal('string', data.response.subjectCommonName);
                assert.equal('string', data.response.subjectCountry);
                assert.equal('string', data.response.subjectDistinguishedName);
                assert.equal('string', data.response.subjectLocality);
                assert.equal('string', data.response.subjectOrganization);
                assert.equal('string', data.response.subjectOrganizationUnit);
                assert.equal('string', data.response.subjectState);
                assert.equal('string', data.response.validityStartDate);
                assert.equal('string', data.response.validityEndDate);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('UPLOAD', data.response.certType);
                assert.equal('string', data.response.id);
                assert.equal('internalcertificate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'getInternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let activeDirectoryRealmObjId = 'fakedata';
    const activeDirectoryRealmAddActiveDirectoryRealmBodyParam = {
      name: 'string',
      dirUsername: 'string',
      dirPassword: '*********',
      baseDN: 'string',
      adPrimaryDomain: 'string',
      type: 'activedirectoryrealm'
    };
    describe('#addActiveDirectoryRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addActiveDirectoryRealm(activeDirectoryRealmAddActiveDirectoryRealmBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.directoryConfigurations));
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.systemDefined);
                assert.equal(2, data.response.realmId);
                assert.equal('string', data.response.dirUsername);
                assert.equal('*********', data.response.dirPassword);
                assert.equal('string', data.response.baseDN);
                assert.equal('object', typeof data.response.ldapAttributeMap);
                assert.equal('string', data.response.adPrimaryDomain);
                assert.equal('string', data.response.id);
                assert.equal('activedirectoryrealm', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              activeDirectoryRealmObjId = data.response.id;
              saveMockData('ActiveDirectoryRealm', 'addActiveDirectoryRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveDirectoryRealmList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveDirectoryRealmList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectoryRealm', 'getActiveDirectoryRealmList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const activeDirectoryRealmEditActiveDirectoryRealmBodyParam = {
      name: 'string',
      dirUsername: 'string',
      dirPassword: '*********',
      baseDN: 'string',
      adPrimaryDomain: 'string',
      type: 'activedirectoryrealm'
    };
    describe('#editActiveDirectoryRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editActiveDirectoryRealm(activeDirectoryRealmObjId, activeDirectoryRealmEditActiveDirectoryRealmBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectoryRealm', 'editActiveDirectoryRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveDirectoryRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveDirectoryRealm(activeDirectoryRealmObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.directoryConfigurations));
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.systemDefined);
                assert.equal(10, data.response.realmId);
                assert.equal('string', data.response.dirUsername);
                assert.equal('*********', data.response.dirPassword);
                assert.equal('string', data.response.baseDN);
                assert.equal('object', typeof data.response.ldapAttributeMap);
                assert.equal('string', data.response.adPrimaryDomain);
                assert.equal('string', data.response.id);
                assert.equal('activedirectoryrealm', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectoryRealm', 'getActiveDirectoryRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecialRealmList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpecialRealmList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpecialRealm', 'getSpecialRealmList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const specialRealmObjId = 'fakedata';
    describe('#getSpecialRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpecialRealm(specialRealmObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.directoryConfigurations));
                assert.equal(false, data.response.enabled);
                assert.equal(true, data.response.systemDefined);
                assert.equal(8, data.response.realmId);
                assert.equal('string', data.response.id);
                assert.equal('specialrealm', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpecialRealm', 'getSpecialRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trafficUserParentId = 'fakedata';
    describe('#getRealmTrafficUserList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRealmTrafficUserList(null, null, null, null, trafficUserParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrafficUser', 'getRealmTrafficUserList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficUserList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrafficUserList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrafficUser', 'getTrafficUserList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trafficUserGroupParentId = 'fakedata';
    describe('#getRealmTrafficUserGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRealmTrafficUserGroupList(null, null, null, null, trafficUserGroupParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrafficUserGroup', 'getRealmTrafficUserGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficUserGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrafficUserGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrafficUserGroup', 'getTrafficUserGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const connectTestObjId = 'fakedata';
    describe('#getConnectTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnectTest(connectTestObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.destination);
                assert.equal(true, data.response.connected);
                assert.equal('string', data.response.msg);
                assert.equal('string', data.response.id);
                assert.equal('ConnectTest', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectTest', 'getConnectTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let scheduleTroubleshootObjId = 'fakedata';
    const scheduleTroubleshootAddScheduleTroubleshootBodyParam = {
      type: 'scheduletroubleshoot'
    };
    describe('#addScheduleTroubleshoot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addScheduleTroubleshoot(scheduleTroubleshootAddScheduleTroubleshootBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('scheduletroubleshoot', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              scheduleTroubleshootObjId = data.response.id;
              saveMockData('ScheduleTroubleshoot', 'addScheduleTroubleshoot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleTroubleshootList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleTroubleshootList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScheduleTroubleshoot', 'getScheduleTroubleshootList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scheduleTroubleshootEditScheduleTroubleshootBodyParam = {
      type: 'scheduletroubleshoot'
    };
    describe('#editScheduleTroubleshoot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editScheduleTroubleshoot(scheduleTroubleshootObjId, scheduleTroubleshootEditScheduleTroubleshootBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScheduleTroubleshoot', 'editScheduleTroubleshoot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleTroubleshoot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleTroubleshoot(scheduleTroubleshootObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('scheduletroubleshoot', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScheduleTroubleshoot', 'getScheduleTroubleshoot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTroubleshootJobHistoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTroubleshootJobHistoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TroubleshootJobHistory', 'getTroubleshootJobHistoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const troubleshootJobHistoryObjId = 'fakedata';
    describe('#getTroubleshootJobHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTroubleshootJobHistory(troubleshootJobHistoryObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('FAILED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.id);
                assert.equal('troubleshootjobhistory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TroubleshootJobHistory', 'getTroubleshootJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullUpgradeImmediateAddPullUpgradeImmediateBodyParam = {
      url: 'string',
      type: 'pullupgradeimmediate'
    };
    describe('#addPullUpgradeImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPullUpgradeImmediate(pullUpgradeImmediateAddPullUpgradeImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('pullupgradeimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullUpgradeImmediate', 'addPullUpgradeImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullUpgradeImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPullUpgradeImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullUpgradeImmediate', 'getPullUpgradeImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullFileAddPullFileBodyParam = {
      downloadUrl: 'string',
      checksumAlgorithm: 'MD5',
      timeout: 7,
      fileType: 'ANYCONNECTPACKAGEFILE',
      type: 'pullfile'
    };
    describe('#addPullFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPullFile(pullFileAddPullFileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('IMMEDIATE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.downloadUrl);
                assert.equal('MD5', data.response.checksumAlgorithm);
                assert.equal('string', data.response.checksum);
                assert.equal(5, data.response.timeout);
                assert.equal('ANYCONNECTPACKAGEFILE', data.response.fileType);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('pullfile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullFile', 'addPullFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullFileList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPullFileList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullFile', 'getPullFileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeFileList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUpgradeFileList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UpgradeFile', 'getUpgradeFileList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const upgradeFileObjId = 'fakedata';
    describe('#getUpgradeFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUpgradeFile(upgradeFileObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.uploadDate);
                assert.equal(2, data.response.fileSize);
                assert.equal('string', data.response.upgradeFileName);
                assert.equal('string', data.response.updateVersion);
                assert.equal('string', data.response.upgradeFrom);
                assert.equal(true, data.response.rebootRequired);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.upgradeType);
                assert.equal('upgradefile', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UpgradeFile', 'getUpgradeFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startUpgrade - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.startUpgrade((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Upgrade', 'startUpgrade', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCancelUpgrade - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCancelUpgrade((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('CancelUpgrade', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CancelUpgrade', 'addCancelUpgrade', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDHCPServerContainerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDHCPServerContainerList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCPServerContainer', 'getDHCPServerContainerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPServerContainerObjId = 'fakedata';
    const dHCPServerContainerEditDHCPServerContainerBodyParam = {
      autoConfig: false,
      type: 'dhcpservercontainer'
    };
    describe('#editDHCPServerContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDHCPServerContainer(dHCPServerContainerObjId, dHCPServerContainerEditDHCPServerContainerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCPServerContainer', 'editDHCPServerContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDHCPServerContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDHCPServerContainer(dHCPServerContainerObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.autoConfig);
                assert.equal('string', data.response.primaryDNS);
                assert.equal('string', data.response.secondaryDNS);
                assert.equal('string', data.response.primaryWINS);
                assert.equal('string', data.response.secondaryWINS);
                assert.equal(true, Array.isArray(data.response.servers));
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.interface);
                assert.equal('dhcpservercontainer', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCPServerContainer', 'getDHCPServerContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudConfigList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudConfigList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudConfig', 'getCloudConfigList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudConfigObjId = 'fakedata';
    const cloudConfigEditCloudConfigBodyParam = {
      type: 'cloudconfig'
    };
    describe('#editCloudConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editCloudConfig(cloudConfigObjId, cloudConfigEditCloudConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudConfig', 'editCloudConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudConfig(cloudConfigObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.enableAutomaticUpdates);
                assert.equal(false, data.response.queryCloudUnknown);
                assert.equal('EIGHT_HRS', data.response.urlCacheReloadTTL);
                assert.equal('string', data.response.id);
                assert.equal('cloudconfig', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudConfig', 'getCloudConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadcert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postuploadcert(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.cert);
                assert.equal('string', data.response.privateKey);
                assert.equal('string', data.response.id);
                assert.equal('certfileuploadstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CertFileUploadStatus', 'postuploadcert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testDirectoryAddTestDirectoryBodyParam = {
      hostname: 'string',
      directoryUsername: 'string',
      directoryPassword: 'string',
      port: 8,
      baseDN: 'string',
      type: 'TestDirectory'
    };
    describe('#addTestDirectory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTestDirectory(testDirectoryAddTestDirectoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.realmUUID);
                assert.equal('string', data.response.hostname);
                assert.equal('string', data.response.directoryUsername);
                assert.equal('string', data.response.directoryPassword);
                assert.equal(5, data.response.port);
                assert.equal('STARTTLS', data.response.protocol);
                assert.equal('string', data.response.sslCertUUID);
                assert.equal('string', data.response.sslCertString);
                assert.equal('string', data.response.baseDN);
                assert.equal('string', data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.linaStatusMessage);
                assert.equal(8, data.response.linaStatusCode);
                assert.equal('string', data.response.id);
                assert.equal('TestDirectory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestDirectory', 'addTestDirectory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testIdentityServicesEngineConnectivityAddTestIdentityServicesEngineConnectivityBodyParam = {
      ftdCertificateUUID: 'string',
      pxGridCertificateUUID: 'string',
      mntCertificateUUID: 'string',
      subscribeToSessionDirectoryTopic: true,
      subscribeToSxpTopic: false,
      type: 'TestIdentityServicesEngineConnectivity'
    };
    describe('#addTestIdentityServicesEngineConnectivity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTestIdentityServicesEngineConnectivity(testIdentityServicesEngineConnectivityAddTestIdentityServicesEngineConnectivityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ftdCertificateUUID);
                assert.equal('string', data.response.pxGridCertificateUUID);
                assert.equal('string', data.response.mntCertificateUUID);
                assert.equal(false, data.response.subscribeToSessionDirectoryTopic);
                assert.equal(true, data.response.subscribeToSxpTopic);
                assert.equal('string', data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.iseLogMessage);
                assert.equal('string', data.response.primaryStatusCode);
                assert.equal('string', data.response.primaryStatusMessage);
                assert.equal('string', data.response.secondaryStatusCode);
                assert.equal('string', data.response.secondaryStatusMessage);
                assert.equal('string', data.response.primaryIseServer);
                assert.equal('string', data.response.secondaryIseServer);
                assert.equal('string', data.response.id);
                assert.equal('TestIdentityServicesEngineConnectivity', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestIdentityServicesEngineConnectivity', 'addTestIdentityServicesEngineConnectivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityGroupTagList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityGroupTagList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityGroupTag', 'getSecurityGroupTagList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityGroupTagObjId = 'fakedata';
    describe('#getSecurityGroupTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityGroupTag(securityGroupTagObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.tag);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.deleted);
                assert.equal('string', data.response.id);
                assert.equal('SecurityGroupTag', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityGroupTag', 'getSecurityGroupTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sGTDynamicObjectObjId = 'fakedata';
    const sGTDynamicObjectAddSGTDynamicObjectBodyParam = {
      tags: [
        {
          type: 'securitygrouptagentry'
        }
      ],
      type: 'sgtdynamicobject'
    };
    describe('#addSGTDynamicObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSGTDynamicObject(sGTDynamicObjectAddSGTDynamicObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.id);
                assert.equal('sgtdynamicobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              sGTDynamicObjectObjId = data.response.id;
              saveMockData('SGTDynamicObject', 'addSGTDynamicObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSGTDynamicObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSGTDynamicObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SGTDynamicObject', 'getSGTDynamicObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sGTDynamicObjectEditSGTDynamicObjectBodyParam = {
      tags: [
        {
          type: 'securitygrouptagentry'
        }
      ],
      type: 'sgtdynamicobject'
    };
    describe('#editSGTDynamicObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSGTDynamicObject(sGTDynamicObjectObjId, sGTDynamicObjectEditSGTDynamicObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SGTDynamicObject', 'editSGTDynamicObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSGTDynamicObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSGTDynamicObject(sGTDynamicObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.id);
                assert.equal('sgtdynamicobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SGTDynamicObject', 'getSGTDynamicObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nTPStatusObjId = 'fakedata';
    describe('#getNTPStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNTPStatus(nTPStatusObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.status);
                assert.equal('string', data.response.id);
                assert.equal('NTPStatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NTPStatus', 'getNTPStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceInfoObjId = 'fakedata';
    describe('#getInterfaceInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceInfo(interfaceInfoObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.interfaceInfoList));
                assert.equal('string', data.response.id);
                assert.equal('InterfaceInfo', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InterfaceInfo', 'getInterfaceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sSLPolicyParentId = 'fakedata';
    const sSLPolicyAddSSLRuleBodyParam = {
      name: 'string',
      certificateStatus: {
        type: 'certificatestatus'
      },
      sslv3: false,
      tls10: false,
      tls11: true,
      tls12: true,
      type: 'sslrule'
    };
    describe('#addSSLRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSSLRule(null, sSLPolicyParentId, sSLPolicyAddSSLRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.ruleId);
                assert.equal(true, Array.isArray(data.response.sourceZones));
                assert.equal(true, Array.isArray(data.response.destinationZones));
                assert.equal(true, Array.isArray(data.response.sourceNetworks));
                assert.equal(true, Array.isArray(data.response.destinationNetworks));
                assert.equal(true, Array.isArray(data.response.sourcePorts));
                assert.equal(true, Array.isArray(data.response.destinationPorts));
                assert.equal(2, data.response.rulePosition);
                assert.equal('DECRYPT_KNOWN_KEY', data.response.ruleAction);
                assert.equal('LOG_NONE', data.response.eventLogAction);
                assert.equal(true, Array.isArray(data.response.identitySources));
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('object', typeof data.response.embeddedAppFilter);
                assert.equal(true, Array.isArray(data.response.urlCategories));
                assert.equal(true, Array.isArray(data.response.subjectDNs));
                assert.equal(true, Array.isArray(data.response.issuerDNs));
                assert.equal('object', typeof data.response.certificateStatus);
                assert.equal('object', typeof data.response.syslogServer);
                assert.equal(false, data.response.sslv3);
                assert.equal(true, data.response.tls10);
                assert.equal(false, data.response.tls11);
                assert.equal(false, data.response.tls12);
                assert.equal('string', data.response.id);
                assert.equal('sslrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              sSLPolicyParentId = data.response.id;
              saveMockData('SSLPolicy', 'addSSLRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSLPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLPolicy', 'getSSLPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSLPolicyObjId = 'fakedata';
    const sSLPolicyEditSSLPolicyBodyParam = {
      name: 'string',
      undecryptableActions: {
        type: 'sslundecryptableactions'
      },
      defaultAction: {
        type: 'sslpolicydefaultaction'
      },
      type: 'sslpolicy'
    };
    describe('#editSSLPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSSLPolicy(sSLPolicyObjId, sSLPolicyEditSSLPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLPolicy', 'editSSLPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSLPolicy(sSLPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.undecryptableActions);
                assert.equal('object', typeof data.response.decryptionCACertificate);
                assert.equal(true, Array.isArray(data.response.internalCertificates));
                assert.equal('object', typeof data.response.defaultAction);
                assert.equal('string', data.response.id);
                assert.equal('sslpolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLPolicy', 'getSSLPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSLRuleList(null, null, null, null, sSLPolicyParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLPolicy', 'getSSLRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSLPolicyEditSSLRuleBodyParam = {
      name: 'string',
      certificateStatus: {
        type: 'certificatestatus'
      },
      sslv3: true,
      tls10: true,
      tls11: true,
      tls12: true,
      type: 'sslrule'
    };
    describe('#editSSLRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSSLRule(null, sSLPolicyParentId, sSLPolicyObjId, sSLPolicyEditSSLRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLPolicy', 'editSSLRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSLRule(sSLPolicyParentId, sSLPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.ruleId);
                assert.equal(true, Array.isArray(data.response.sourceZones));
                assert.equal(true, Array.isArray(data.response.destinationZones));
                assert.equal(true, Array.isArray(data.response.sourceNetworks));
                assert.equal(true, Array.isArray(data.response.destinationNetworks));
                assert.equal(true, Array.isArray(data.response.sourcePorts));
                assert.equal(true, Array.isArray(data.response.destinationPorts));
                assert.equal(10, data.response.rulePosition);
                assert.equal('DECRYPT_RE_SIGN', data.response.ruleAction);
                assert.equal('LOG_FLOW_END', data.response.eventLogAction);
                assert.equal(true, Array.isArray(data.response.identitySources));
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal('object', typeof data.response.embeddedAppFilter);
                assert.equal(true, Array.isArray(data.response.urlCategories));
                assert.equal(true, Array.isArray(data.response.subjectDNs));
                assert.equal(true, Array.isArray(data.response.issuerDNs));
                assert.equal('object', typeof data.response.certificateStatus);
                assert.equal('object', typeof data.response.syslogServer);
                assert.equal(false, data.response.sslv3);
                assert.equal(false, data.response.tls10);
                assert.equal(false, data.response.tls11);
                assert.equal(false, data.response.tls12);
                assert.equal('string', data.response.id);
                assert.equal('sslrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLPolicy', 'getSSLRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostUpgradeFlagsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPostUpgradeFlagsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PostUpgradeFlags', 'getPostUpgradeFlagsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const postUpgradeFlagsObjId = 'fakedata';
    const postUpgradeFlagsEditPostUpgradeFlagsBodyParam = {
      type: 'postupgradeflags'
    };
    describe('#editPostUpgradeFlags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editPostUpgradeFlags(postUpgradeFlagsObjId, postUpgradeFlagsEditPostUpgradeFlagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PostUpgradeFlags', 'editPostUpgradeFlags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostUpgradeFlags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPostUpgradeFlags(postUpgradeFlagsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.showPostUpgradeDialog);
                assert.equal(true, Array.isArray(data.response.featureList));
                assert.equal('string', data.response.id);
                assert.equal('postupgradeflags', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PostUpgradeFlags', 'getPostUpgradeFlags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let oSPFVrfId = 'fakedata';
    let oSPFObjId = 'fakedata';
    const oSPFAddOSPFBodyParam = {
      name: 'string',
      processId: 'string',
      processConfiguration: {
        type: 'processconfiguration'
      },
      areas: [
        {
          type: 'area'
        }
      ],
      neighbors: [
        {
          type: 'neighbor'
        }
      ],
      summaryAddresses: [
        {
          type: 'summaryaddress'
        }
      ],
      filterRules: [
        {}
      ],
      redistributeProtocols: [
        {}
      ],
      type: 'ospf'
    };
    describe('#addOSPF - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addOSPF(oSPFVrfId, oSPFAddOSPFBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.processId);
                assert.equal('object', typeof data.response.logAdjacencyChanges);
                assert.equal('object', typeof data.response.processConfiguration);
                assert.equal(true, Array.isArray(data.response.areas));
                assert.equal(true, Array.isArray(data.response.neighbors));
                assert.equal(true, Array.isArray(data.response.summaryAddresses));
                assert.equal(true, Array.isArray(data.response.filterRules));
                assert.equal(true, Array.isArray(data.response.redistributeProtocols));
                assert.equal('string', data.response.id);
                assert.equal('ospf', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              oSPFVrfId = data.response.id;
              oSPFObjId = data.response.id;
              saveMockData('OSPF', 'addOSPF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOSPFList(null, null, null, null, oSPFVrfId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPF', 'getOSPFList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oSPFEditOSPFBodyParam = {
      name: 'string',
      processId: 'string',
      processConfiguration: {
        type: 'processconfiguration'
      },
      areas: [
        {
          type: 'area'
        }
      ],
      neighbors: [
        {
          type: 'neighbor'
        }
      ],
      summaryAddresses: [
        {
          type: 'summaryaddress'
        }
      ],
      filterRules: [
        {}
      ],
      redistributeProtocols: [
        {}
      ],
      type: 'ospf'
    };
    describe('#editOSPF - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editOSPF(oSPFVrfId, oSPFObjId, oSPFEditOSPFBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPF', 'editOSPF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPF - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOSPF(oSPFVrfId, oSPFObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.processId);
                assert.equal('object', typeof data.response.logAdjacencyChanges);
                assert.equal('object', typeof data.response.processConfiguration);
                assert.equal(true, Array.isArray(data.response.areas));
                assert.equal(true, Array.isArray(data.response.neighbors));
                assert.equal(true, Array.isArray(data.response.summaryAddresses));
                assert.equal(true, Array.isArray(data.response.filterRules));
                assert.equal(true, Array.isArray(data.response.redistributeProtocols));
                assert.equal('string', data.response.id);
                assert.equal('ospf', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPF', 'getOSPF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let oSPFInterfaceSettingsVrfId = 'fakedata';
    let oSPFInterfaceSettingsObjId = 'fakedata';
    const oSPFInterfaceSettingsAddOSPFInterfaceSettingsBodyParam = {
      name: 'string',
      deviceInterface: {
        type: 'string',
        name: 'string'
      },
      ospfProtocolConfiguration: {
        type: 'ospfprotocolconfiguration'
      },
      type: 'ospfinterfacesettings'
    };
    describe('#addOSPFInterfaceSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addOSPFInterfaceSettings(oSPFInterfaceSettingsVrfId, oSPFInterfaceSettingsAddOSPFInterfaceSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.deviceInterface);
                assert.equal('object', typeof data.response.ospfProtocolConfiguration);
                assert.equal('string', data.response.id);
                assert.equal('ospfinterfacesettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              oSPFInterfaceSettingsVrfId = data.response.id;
              oSPFInterfaceSettingsObjId = data.response.id;
              saveMockData('OSPFInterfaceSettings', 'addOSPFInterfaceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFInterfaceSettingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOSPFInterfaceSettingsList(null, null, null, null, oSPFInterfaceSettingsVrfId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPFInterfaceSettings', 'getOSPFInterfaceSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oSPFInterfaceSettingsEditOSPFInterfaceSettingsBodyParam = {
      name: 'string',
      deviceInterface: {
        type: 'string',
        name: 'string'
      },
      ospfProtocolConfiguration: {
        type: 'ospfprotocolconfiguration'
      },
      type: 'ospfinterfacesettings'
    };
    describe('#editOSPFInterfaceSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editOSPFInterfaceSettings(oSPFInterfaceSettingsVrfId, oSPFInterfaceSettingsObjId, oSPFInterfaceSettingsEditOSPFInterfaceSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPFInterfaceSettings', 'editOSPFInterfaceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFInterfaceSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOSPFInterfaceSettings(oSPFInterfaceSettingsVrfId, oSPFInterfaceSettingsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.deviceInterface);
                assert.equal('object', typeof data.response.ospfProtocolConfiguration);
                assert.equal('string', data.response.id);
                assert.equal('ospfinterfacesettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPFInterfaceSettings', 'getOSPFInterfaceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let bGPVrfId = 'fakedata';
    let bGPObjId = 'fakedata';
    const bGPAddBGPBodyParam = {
      name: 'string',
      asNumber: 'string',
      type: 'bgp'
    };
    describe('#addBGP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBGP(bGPVrfId, bGPAddBGPBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.asNumber);
                assert.equal('string', data.response.routerId);
                assert.equal('object', typeof data.response.addressFamilyIPv4);
                assert.equal('object', typeof data.response.addressFamilyIPv6);
                assert.equal('string', data.response.id);
                assert.equal('bgp', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              bGPVrfId = data.response.id;
              bGPObjId = data.response.id;
              saveMockData('BGP', 'addBGP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBGPList(null, null, null, null, bGPVrfId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGP', 'getBGPList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bGPEditBGPBodyParam = {
      name: 'string',
      asNumber: 'string',
      type: 'bgp'
    };
    describe('#editBGP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editBGP(bGPVrfId, bGPObjId, bGPEditBGPBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGP', 'editBGP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBGP(bGPVrfId, bGPObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.asNumber);
                assert.equal('string', data.response.routerId);
                assert.equal('object', typeof data.response.addressFamilyIPv4);
                assert.equal('object', typeof data.response.addressFamilyIPv6);
                assert.equal('string', data.response.id);
                assert.equal('bgp', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGP', 'getBGP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let bGPGeneralSettingsObjId = 'fakedata';
    const bGPGeneralSettingsAddBGPGeneralSettingsBodyParam = {
      name: 'string',
      asNumber: 'string',
      logNeighborChanges: true,
      transportPathMtuDiscovery: true,
      fastExternalFallOver: false,
      enforceFirstAs: false,
      asnotationDot: false,
      bgpTimers: {
        type: 'bgptimers'
      },
      bgpBestPath: {
        type: 'bgpbestpath'
      },
      type: 'bgpgeneralsettings'
    };
    describe('#addBGPGeneralSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBGPGeneralSettings(bGPGeneralSettingsAddBGPGeneralSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.asNumber);
                assert.equal('string', data.response.routerId);
                assert.equal(4, data.response.scanTime);
                assert.equal(4, data.response.aggregateTimer);
                assert.equal(10, data.response.bgpNextHopTriggerDelay);
                assert.equal(true, data.response.bgpNextHopTriggerEnable);
                assert.equal(7, data.response.maxasLimit);
                assert.equal(false, data.response.logNeighborChanges);
                assert.equal(false, data.response.transportPathMtuDiscovery);
                assert.equal(true, data.response.fastExternalFallOver);
                assert.equal(false, data.response.enforceFirstAs);
                assert.equal(true, data.response.asnotationDot);
                assert.equal('object', typeof data.response.bgpTimers);
                assert.equal('object', typeof data.response.bgpGracefulRestart);
                assert.equal('object', typeof data.response.bgpBestPath);
                assert.equal('string', data.response.id);
                assert.equal('bgpgeneralsettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              bGPGeneralSettingsObjId = data.response.id;
              saveMockData('BGPGeneralSettings', 'addBGPGeneralSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPGeneralSettingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBGPGeneralSettingsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGPGeneralSettings', 'getBGPGeneralSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bGPGeneralSettingsEditBGPGeneralSettingsBodyParam = {
      name: 'string',
      asNumber: 'string',
      logNeighborChanges: true,
      transportPathMtuDiscovery: true,
      fastExternalFallOver: true,
      enforceFirstAs: true,
      asnotationDot: false,
      bgpTimers: {
        type: 'bgptimers'
      },
      bgpBestPath: {
        type: 'bgpbestpath'
      },
      type: 'bgpgeneralsettings'
    };
    describe('#editBGPGeneralSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editBGPGeneralSettings(bGPGeneralSettingsObjId, bGPGeneralSettingsEditBGPGeneralSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGPGeneralSettings', 'editBGPGeneralSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPGeneralSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBGPGeneralSettings(bGPGeneralSettingsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.asNumber);
                assert.equal('string', data.response.routerId);
                assert.equal(4, data.response.scanTime);
                assert.equal(10, data.response.aggregateTimer);
                assert.equal(7, data.response.bgpNextHopTriggerDelay);
                assert.equal(true, data.response.bgpNextHopTriggerEnable);
                assert.equal(4, data.response.maxasLimit);
                assert.equal(false, data.response.logNeighborChanges);
                assert.equal(true, data.response.transportPathMtuDiscovery);
                assert.equal(true, data.response.fastExternalFallOver);
                assert.equal(false, data.response.enforceFirstAs);
                assert.equal(true, data.response.asnotationDot);
                assert.equal('object', typeof data.response.bgpTimers);
                assert.equal('object', typeof data.response.bgpGracefulRestart);
                assert.equal('object', typeof data.response.bgpBestPath);
                assert.equal('string', data.response.id);
                assert.equal('bgpgeneralsettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGPGeneralSettings', 'getBGPGeneralSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let flexConfigObjectObjId = 'fakedata';
    const flexConfigObjectAddFlexConfigObjectBodyParam = {
      name: 'string',
      isBlacklisted: true,
      type: 'flexconfigobject'
    };
    describe('#addFlexConfigObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addFlexConfigObject(flexConfigObjectAddFlexConfigObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(true, Array.isArray(data.response.negateLines));
                assert.equal(false, data.response.isBlacklisted);
                assert.equal(true, Array.isArray(data.response.variables));
                assert.equal('string', data.response.id);
                assert.equal('flexconfigobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              flexConfigObjectObjId = data.response.id;
              saveMockData('FlexConfigObject', 'addFlexConfigObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlexConfigObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigObject', 'getFlexConfigObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flexConfigObjectEditFlexConfigObjectBodyParam = {
      name: 'string',
      isBlacklisted: true,
      type: 'flexconfigobject'
    };
    describe('#editFlexConfigObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editFlexConfigObject(flexConfigObjectObjId, flexConfigObjectEditFlexConfigObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigObject', 'editFlexConfigObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlexConfigObject(flexConfigObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.lines));
                assert.equal(true, Array.isArray(data.response.negateLines));
                assert.equal(true, data.response.isBlacklisted);
                assert.equal(true, Array.isArray(data.response.variables));
                assert.equal('string', data.response.id);
                assert.equal('flexconfigobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigObject', 'getFlexConfigObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let flexConfigPolicyObjId = 'fakedata';
    const flexConfigPolicyAddFlexConfigPolicyBodyParam = {
      name: 'string',
      type: 'flexconfigpolicy'
    };
    describe('#addFlexConfigPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addFlexConfigPolicy(flexConfigPolicyAddFlexConfigPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.flexConfigObjects));
                assert.equal('string', data.response.id);
                assert.equal('flexconfigpolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              flexConfigPolicyObjId = data.response.id;
              saveMockData('FlexConfigPolicy', 'addFlexConfigPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlexConfigPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigPolicy', 'getFlexConfigPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flexConfigPolicyEditFlexConfigPolicyBodyParam = {
      name: 'string',
      type: 'flexconfigpolicy'
    };
    describe('#editFlexConfigPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editFlexConfigPolicy(flexConfigPolicyObjId, flexConfigPolicyEditFlexConfigPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigPolicy', 'editFlexConfigPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlexConfigPolicy(flexConfigPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.flexConfigObjects));
                assert.equal('string', data.response.id);
                assert.equal('flexconfigpolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigPolicy', 'getFlexConfigPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAConfigurationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHAConfigurationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAConfiguration', 'getHAConfigurationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hAConfigurationObjId = 'fakedata';
    const hAConfigurationEditHAConfigurationBodyParam = {
      name: 'string',
      nodeRole: 'HA_PRIMARY',
      failoverInterface: {
        type: 'string',
        name: 'string'
      },
      failoverName: 'string',
      statefulFailoverInterface: {
        type: 'string',
        name: 'string'
      },
      statefulFailoverName: 'string',
      type: 'haconfiguration'
    };
    describe('#editHAConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editHAConfiguration(hAConfigurationObjId, hAConfigurationEditHAConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAConfiguration', 'editHAConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHAConfiguration(hAConfigurationObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('HA_SECONDARY', data.response.nodeRole);
                assert.equal('object', typeof data.response.failoverInterface);
                assert.equal('string', data.response.failoverName);
                assert.equal('object', typeof data.response.primaryFailoverIPv4);
                assert.equal('object', typeof data.response.secondaryFailoverIPv4);
                assert.equal('object', typeof data.response.primaryFailoverIPv6);
                assert.equal('object', typeof data.response.secondaryFailoverIPv6);
                assert.equal('object', typeof data.response.statefulFailoverInterface);
                assert.equal('string', data.response.statefulFailoverName);
                assert.equal('object', typeof data.response.primaryStatefulFailoverIPv4);
                assert.equal('object', typeof data.response.secondaryStatefulFailoverIPv4);
                assert.equal('object', typeof data.response.primaryStatefulFailoverIPv6);
                assert.equal('object', typeof data.response.secondaryStatefulFailoverIPv6);
                assert.equal('*********', data.response.sharedKey);
                assert.equal('string', data.response.id);
                assert.equal('haconfiguration', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAConfiguration', 'getHAConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAFailoverConfigurationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHAFailoverConfigurationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAFailoverConfiguration', 'getHAFailoverConfigurationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hAFailoverConfigurationObjId = 'fakedata';
    const hAFailoverConfigurationEditHAFailoverConfigurationBodyParam = {
      peerPollTime: {
        type: 'time'
      },
      peerHoldTime: {
        type: 'time'
      },
      interfacePollTime: {
        type: 'time'
      },
      interfaceHoldTime: {
        type: 'time'
      },
      interfaceFailureThreshold: 7,
      interfaceFailureUnit: 'PERCENTAGE',
      type: 'hafailoverconfiguration'
    };
    describe('#editHAFailoverConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editHAFailoverConfiguration(hAFailoverConfigurationObjId, hAFailoverConfigurationEditHAFailoverConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAFailoverConfiguration', 'editHAFailoverConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAFailoverConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHAFailoverConfiguration(hAFailoverConfigurationObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.peerPollTime);
                assert.equal('object', typeof data.response.peerHoldTime);
                assert.equal('object', typeof data.response.interfacePollTime);
                assert.equal('object', typeof data.response.interfaceHoldTime);
                assert.equal(3, data.response.interfaceFailureThreshold);
                assert.equal('NUMBER', data.response.interfaceFailureUnit);
                assert.equal('string', data.response.id);
                assert.equal('hafailoverconfiguration', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAFailoverConfiguration', 'getHAFailoverConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hAStatusObjId = 'fakedata';
    describe('#getHAStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHAStatus(hAStatusObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('HA_SECONDARY', data.response.nodeRole);
                assert.equal('HA_UNKNOWN_NODE', data.response.nodeState);
                assert.equal('HA_UNKNOWN_NODE', data.response.peerNodeState);
                assert.equal('UNKNOWN', data.response.configStatus);
                assert.equal('HEALTHY', data.response.haHealthStatus);
                assert.equal('string', data.response.disabledReason);
                assert.equal('string', data.response.disabledTimestamp);
                assert.equal('string', data.response.id);
                assert.equal('HAStatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HAStatus', 'getHAStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let radiusIdentitySourceObjId = 'fakedata';
    const radiusIdentitySourceAddRadiusIdentitySourceBodyParam = {
      name: 'string',
      host: 'string',
      type: 'radiusidentitysource'
    };
    describe('#addRadiusIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRadiusIdentitySource(radiusIdentitySourceAddRadiusIdentitySourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.host);
                assert.equal(3, data.response.timeout);
                assert.equal(3, data.response.serverAuthenticationPort);
                assert.equal('*********', data.response.serverSecretKey);
                assert.equal(true, Array.isArray(data.response.capabilities));
                assert.equal(true, data.response.useRoutingToSelectInterface);
                assert.equal('object', typeof data.response.redirectAcl);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.interface);
                assert.equal('radiusidentitysource', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              radiusIdentitySourceObjId = data.response.id;
              saveMockData('RadiusIdentitySource', 'addRadiusIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySourceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRadiusIdentitySourceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySource', 'getRadiusIdentitySourceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusIdentitySourceEditRadiusIdentitySourceBodyParam = {
      name: 'string',
      host: 'string',
      type: 'radiusidentitysource'
    };
    describe('#editRadiusIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editRadiusIdentitySource(radiusIdentitySourceObjId, radiusIdentitySourceEditRadiusIdentitySourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySource', 'editRadiusIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRadiusIdentitySource(radiusIdentitySourceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.host);
                assert.equal(1, data.response.timeout);
                assert.equal(1, data.response.serverAuthenticationPort);
                assert.equal('*********', data.response.serverSecretKey);
                assert.equal(true, Array.isArray(data.response.capabilities));
                assert.equal(false, data.response.useRoutingToSelectInterface);
                assert.equal('object', typeof data.response.redirectAcl);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.interface);
                assert.equal('radiusidentitysource', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySource', 'getRadiusIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocalIdentitySourceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLocalIdentitySourceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LocalIdentitySource', 'getLocalIdentitySourceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const localIdentitySourceObjId = 'fakedata';
    describe('#getLocalIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLocalIdentitySource(localIdentitySourceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('localidentitysource', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LocalIdentitySource', 'getLocalIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const testIdentitySourceAddTestIdentitySourceBodyParam = {
      identitySource: {
        type: 'identitysourcebase'
      },
      username: 'string',
      password: samProps.authentication.password,
      type: 'TestIdentitySource'
    };
    describe('#addTestIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTestIdentitySource(testIdentitySourceAddTestIdentitySourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.identitySource);
                assert.equal('string', data.response.username);
                assert.equal('*********', data.response.password);
                assert.equal(5, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.id);
                assert.equal('TestIdentitySource', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TestIdentitySource', 'addTestIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveUserSessionsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveUserSessionsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveUserSessions', 'getActiveUserSessionsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const activeUserSessionsObjId = 'fakedata';
    describe('#getActiveUserSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveUserSessions(activeUserSessionsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.userName);
                assert.equal('string', data.response.identitySourceId);
                assert.equal(1, data.response.issuedAt);
                assert.equal(8, data.response.expiresAt);
                assert.equal('string', data.response.sessionDuration);
                assert.equal('string', data.response.id);
                assert.equal('ActiveUserSessions', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveUserSessions', 'getActiveUserSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAASettingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAAASettingList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AAASetting', 'getAAASettingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aAASettingObjId = 'fakedata';
    const aAASettingEditAAASettingBodyParam = {
      identitySourceGroup: {
        type: 'string'
      },
      protocolType: 'SSH',
      useLocal: 'NOT_APPLICABLE',
      type: 'aaasetting'
    };
    describe('#editAAASetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAAASetting(aAASettingObjId, aAASettingEditAAASettingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AAASetting', 'editAAASetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAASetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAAASetting(aAASettingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.identitySourceGroup);
                assert.equal('string', data.response.description);
                assert.equal('SSH', data.response.protocolType);
                assert.equal('BEFORE', data.response.useLocal);
                assert.equal('string', data.response.id);
                assert.equal('aaasetting', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AAASetting', 'getAAASetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let radiusIdentitySourceGroupObjId = 'fakedata';
    const radiusIdentitySourceGroupAddRadiusIdentitySourceGroupBodyParam = {
      name: 'string',
      type: 'radiusidentitysourcegroup'
    };
    describe('#addRadiusIdentitySourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRadiusIdentitySourceGroup(radiusIdentitySourceGroupAddRadiusIdentitySourceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.maxFailedAttempts);
                assert.equal(1, data.response.deadTime);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.radiusIdentitySources));
                assert.equal('object', typeof data.response.activeDirectoryRealm);
                assert.equal(true, data.response.enableDynamicAuthorization);
                assert.equal(7, data.response.dynamicAuthorizationPort);
                assert.equal('string', data.response.id);
                assert.equal('radiusidentitysourcegroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              radiusIdentitySourceGroupObjId = data.response.id;
              saveMockData('RadiusIdentitySourceGroup', 'addRadiusIdentitySourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySourceGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRadiusIdentitySourceGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySourceGroup', 'getRadiusIdentitySourceGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const radiusIdentitySourceGroupEditRadiusIdentitySourceGroupBodyParam = {
      name: 'string',
      type: 'radiusidentitysourcegroup'
    };
    describe('#editRadiusIdentitySourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editRadiusIdentitySourceGroup(radiusIdentitySourceGroupObjId, radiusIdentitySourceGroupEditRadiusIdentitySourceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySourceGroup', 'editRadiusIdentitySourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySourceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRadiusIdentitySourceGroup(radiusIdentitySourceGroupObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.maxFailedAttempts);
                assert.equal(8, data.response.deadTime);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.radiusIdentitySources));
                assert.equal('object', typeof data.response.activeDirectoryRealm);
                assert.equal(false, data.response.enableDynamicAuthorization);
                assert.equal(5, data.response.dynamicAuthorizationPort);
                assert.equal('string', data.response.id);
                assert.equal('radiusidentitysourcegroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySourceGroup', 'getRadiusIdentitySourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRolePermissionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRolePermissionList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RolePermission', 'getRolePermissionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolePermissionObjId = 'fakedata';
    describe('#getRolePermission - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRolePermission(rolePermissionObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.role);
                assert.equal(true, Array.isArray(data.response.rolePermissionSet));
                assert.equal('string', data.response.id);
                assert.equal('RolePermission', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RolePermission', 'getRolePermission', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let customLoggingListObjId = 'fakedata';
    const customLoggingListAddCustomLoggingListBodyParam = {
      name: 'string',
      type: 'customlogginglist'
    };
    describe('#addCustomLoggingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCustomLoggingList(customLoggingListAddCustomLoggingListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.messagesCriteria));
                assert.equal(true, Array.isArray(data.response.logClassCriteria));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('customlogginglist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              customLoggingListObjId = data.response.id;
              saveMockData('CustomLoggingList', 'addCustomLoggingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomLoggingListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomLoggingListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomLoggingList', 'getCustomLoggingListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customLoggingListEditCustomLoggingListBodyParam = {
      name: 'string',
      type: 'customlogginglist'
    };
    describe('#editCustomLoggingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editCustomLoggingList(customLoggingListObjId, customLoggingListEditCustomLoggingListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomLoggingList', 'editCustomLoggingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomLoggingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomLoggingList(customLoggingListObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.messagesCriteria));
                assert.equal(true, Array.isArray(data.response.logClassCriteria));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('customlogginglist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomLoggingList', 'getCustomLoggingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebAnalyticsSettingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebAnalyticsSettingList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebAnalyticsSetting', 'getWebAnalyticsSettingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webAnalyticsSettingObjId = 'fakedata';
    const webAnalyticsSettingEditWebAnalyticsSettingBodyParam = {
      type: 'webanalyticssetting'
    };
    describe('#editWebAnalyticsSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editWebAnalyticsSetting(webAnalyticsSettingObjId, webAnalyticsSettingEditWebAnalyticsSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebAnalyticsSetting', 'editWebAnalyticsSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebAnalyticsSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebAnalyticsSetting(webAnalyticsSettingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.disabled);
                assert.equal('string', data.response.id);
                assert.equal('webanalyticssetting', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebAnalyticsSetting', 'getWebAnalyticsSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let initialProvisionObjId = 'fakedata';
    const initialProvisionAddInitialProvisionBodyParam = {
      type: 'initialprovision'
    };
    describe('#addInitialProvision - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addInitialProvision(initialProvisionAddInitialProvisionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.acceptEULA);
                assert.equal('string', data.response.eulaText);
                assert.equal('*********', data.response.currentPassword);
                assert.equal('*********', data.response.newPassword);
                assert.equal('string', data.response.id);
                assert.equal('initialprovision', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              initialProvisionObjId = data.response.id;
              saveMockData('InitialProvision', 'addInitialProvision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInitialProvisionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInitialProvisionList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InitialProvision', 'getInitialProvisionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInitialProvision - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInitialProvision(initialProvisionObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.acceptEULA);
                assert.equal('string', data.response.eulaText);
                assert.equal('*********', data.response.currentPassword);
                assert.equal('*********', data.response.newPassword);
                assert.equal('string', data.response.id);
                assert.equal('initialprovision', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InitialProvision', 'getInitialProvision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hitCountParentId = 'fakedata';
    describe('#editHitCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editHitCount(hitCountParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HitCount', 'editHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHitCountList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHitCountList(null, null, null, null, hitCountParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HitCount', 'getHitCountList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let duoLDAPIdentitySourceObjId = 'fakedata';
    const duoLDAPIdentitySourceAddDuoLDAPIdentitySourceBodyParam = {
      name: 'string',
      apiHostname: 'string',
      port: 4,
      integrationKey: 'string',
      secretKey: samProps.authentication.password,
      type: 'duoldapidentitysource'
    };
    describe('#addDuoLDAPIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDuoLDAPIdentitySource(duoLDAPIdentitySourceAddDuoLDAPIdentitySourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.apiHostname);
                assert.equal(5, data.response.port);
                assert.equal(6, data.response.timeout);
                assert.equal('string', data.response.integrationKey);
                assert.equal('*********', data.response.secretKey);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.interface);
                assert.equal('duoldapidentitysource', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              duoLDAPIdentitySourceObjId = data.response.id;
              saveMockData('DuoLDAPIdentitySource', 'addDuoLDAPIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDuoLDAPIdentitySourceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDuoLDAPIdentitySourceList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DuoLDAPIdentitySource', 'getDuoLDAPIdentitySourceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const duoLDAPIdentitySourceEditDuoLDAPIdentitySourceBodyParam = {
      name: 'string',
      apiHostname: 'string',
      port: 9,
      integrationKey: 'string',
      secretKey: samProps.authentication.password,
      type: 'duoldapidentitysource'
    };
    describe('#editDuoLDAPIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDuoLDAPIdentitySource(duoLDAPIdentitySourceObjId, duoLDAPIdentitySourceEditDuoLDAPIdentitySourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DuoLDAPIdentitySource', 'editDuoLDAPIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDuoLDAPIdentitySource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDuoLDAPIdentitySource(duoLDAPIdentitySourceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.apiHostname);
                assert.equal(3, data.response.port);
                assert.equal(7, data.response.timeout);
                assert.equal('string', data.response.integrationKey);
                assert.equal('*********', data.response.secretKey);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.interface);
                assert.equal('duoldapidentitysource', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DuoLDAPIdentitySource', 'getDuoLDAPIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyAddScheduleStoredFileSHAListBodyParam = {
      type: 'schedulestoredfileshalist'
    };
    describe('#addScheduleStoredFileSHAList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addScheduleStoredFileSHAList(fileAndMalwarePolicyAddScheduleStoredFileSHAListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('WEEKLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('schedulestoredfileshalist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'addScheduleStoredFileSHAList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadcleanhashlist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postuploadcleanhashlist(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.id);
                assert.equal('fileuploadstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'postuploadcleanhashlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadcustomdetectionhashlist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postuploadcustomdetectionhashlist(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.fileName);
                assert.equal('string', data.response.id);
                assert.equal('fileuploadstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'postuploadcustomdetectionhashlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let fileAndMalwarePolicyParentId = 'fakedata';
    const fileAndMalwarePolicyAddFilePolicyBodyParam = {
      name: 'string',
      type: 'filepolicy'
    };
    describe('#addFilePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addFilePolicy(fileAndMalwarePolicyAddFilePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.firstTimeAnalysis);
                assert.equal(false, data.response.enableCustomDetectionList);
                assert.equal(false, data.response.enableCleanList);
                assert.equal(true, data.response.inspectArchives);
                assert.equal(false, data.response.blockEncryptedArchives);
                assert.equal(true, data.response.blockUnInspectableArchives);
                assert.equal(2, data.response.maxArchiveDepth);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('filepolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              fileAndMalwarePolicyParentId = data.response.id;
              saveMockData('FileAndMalwarePolicy', 'addFilePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyAddFileRuleBodyParam = {
      name: 'string',
      applicationProtocols: 'IMAP',
      directionOfTransfer: 'UPLOAD',
      ruleAction: 'BLOCK_FILES',
      type: 'filerule'
    };
    describe('#addFileRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addFileRule(null, fileAndMalwarePolicyParentId, fileAndMalwarePolicyAddFileRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.rulePosition);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.fileTypeCategories));
                assert.equal(true, Array.isArray(data.response.fileTypes));
                assert.equal('POP3', data.response.applicationProtocols);
                assert.equal(true, Array.isArray(data.response.malwareAnalysisOptions));
                assert.equal(true, Array.isArray(data.response.storeFiles));
                assert.equal('ANY', data.response.directionOfTransfer);
                assert.equal('DETECT_FILES', data.response.ruleAction);
                assert.equal(true, data.response.reset);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('filerule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'addFileRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyObjId = 'fakedata';
    describe('#getdownloadcleanhashlist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadcleanhashlist(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getdownloadcleanhashlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadcustomdetectionhashlist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadcustomdetectionhashlist(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getdownloadcustomdetectionhashlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadstoredfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadstoredfiles(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getdownloadstoredfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadstoredfileshalist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getdownloadstoredfileshalist(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getdownloadstoredfileshalist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleStoredFileSHAListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleStoredFileSHAListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getScheduleStoredFileSHAListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmpCloudConfigList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAmpCloudConfigList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getAmpCloudConfigList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyEditAmpCloudConfigBodyParam = {
      name: 'string',
      type: 'ampcloudconfig'
    };
    describe('#editAmpCloudConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAmpCloudConfig(fileAndMalwarePolicyObjId, fileAndMalwarePolicyEditAmpCloudConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'editAmpCloudConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmpCloudConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAmpCloudConfig(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.automaticLocalMalwareUpdatesEnabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.id);
                assert.equal('ampcloudconfig', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getAmpCloudConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPCloudConnectionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAMPCloudConnectionList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getAMPCloudConnectionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyEditAMPCloudConnectionBodyParam = {
      name: 'string',
      ampNetworkServer: {
        type: 'string',
        name: 'string'
      },
      type: 'ampcloudconnection'
    };
    describe('#editAMPCloudConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAMPCloudConnection(fileAndMalwarePolicyObjId, fileAndMalwarePolicyEditAMPCloudConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'editAMPCloudConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPCloudConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAMPCloudConnection(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.enableAmpForNetwork);
                assert.equal('object', typeof data.response.ampNetworkServer);
                assert.equal('string', data.response.id);
                assert.equal('ampcloudconnection', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getAMPCloudConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPServerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAMPServerList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getAMPServerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAMPServer(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.hostName);
                assert.equal('PRIVATE_CLOUD', data.response.cloudType);
                assert.equal(false, data.response.systemDefined);
                assert.equal('string', data.response.id);
                assert.equal('ampserver', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getAMPServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicyConfigurationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilePolicyConfigurationList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFilePolicyConfigurationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyEditFilePolicyConfigurationBodyParam = {
      type: 'filepolicyconfiguration'
    };
    describe('#editFilePolicyConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editFilePolicyConfiguration(fileAndMalwarePolicyObjId, fileAndMalwarePolicyEditFilePolicyConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'editFilePolicyConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicyConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilePolicyConfiguration(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.bytesToBeInspected);
                assert.equal(1, data.response.allowFileTimeout);
                assert.equal(10, data.response.disableSha256ForLargerFiles);
                assert.equal(2, data.response.minFileSizeAdvancedFileInspection);
                assert.equal(8, data.response.maxFileSizeAdvancedFileInspection);
                assert.equal(true, data.response.systemDefined);
                assert.equal('string', data.response.id);
                assert.equal('filepolicyconfiguration', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFilePolicyConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleStoredFileSHAListJobHistoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleStoredFileSHAListJobHistoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getScheduleStoredFileSHAListJobHistoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileTypeCategoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFileTypeCategoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFileTypeCategoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileTypeCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFileTypeCategory(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.fileTypes));
                assert.equal('string', data.response.id);
                assert.equal('filetypecategory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFileTypeCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileTypeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFileTypeList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFileTypeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFileType(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.ruleAction));
                assert.equal('ANY', data.response.direction);
                assert.equal(true, Array.isArray(data.response.malwareCapabilities));
                assert.equal('string', data.response.id);
                assert.equal('filetype', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFileType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCleanListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCleanListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getCleanListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCleanList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCleanList(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.cleanListHashCount);
                assert.equal('string', data.response.id);
                assert.equal('cleanlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getCleanList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomDetectionListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomDetectionListList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getCustomDetectionListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomDetectionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomDetectionList(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.detectionListHashCount);
                assert.equal('string', data.response.id);
                assert.equal('customdetectionlist', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getCustomDetectionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPCloudConnectionStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAMPCloudConnectionStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getAMPCloudConnectionStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMalwareUpdateConnectionStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMalwareUpdateConnectionStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getMalwareUpdateConnectionStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilePolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFilePolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyEditFilePolicyBodyParam = {
      name: 'string',
      type: 'filepolicy'
    };
    describe('#editFilePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editFilePolicy(fileAndMalwarePolicyObjId, fileAndMalwarePolicyEditFilePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'editFilePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilePolicy(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.firstTimeAnalysis);
                assert.equal(false, data.response.enableCustomDetectionList);
                assert.equal(true, data.response.enableCleanList);
                assert.equal(true, data.response.inspectArchives);
                assert.equal(true, data.response.blockEncryptedArchives);
                assert.equal(false, data.response.blockUnInspectableArchives);
                assert.equal(5, data.response.maxArchiveDepth);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('filepolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFilePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFileRuleList(null, null, null, null, fileAndMalwarePolicyParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFileRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileAndMalwarePolicyEditFileRuleBodyParam = {
      name: 'string',
      applicationProtocols: 'IMAP',
      directionOfTransfer: 'DOWNLOAD',
      ruleAction: 'MALWARE_CLOUD_LOOKUP',
      type: 'filerule'
    };
    describe('#editFileRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editFileRule(null, fileAndMalwarePolicyParentId, fileAndMalwarePolicyObjId, fileAndMalwarePolicyEditFileRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'editFileRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFileRule(fileAndMalwarePolicyParentId, fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.rulePosition);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.fileTypeCategories));
                assert.equal(true, Array.isArray(data.response.fileTypes));
                assert.equal('IMAP', data.response.applicationProtocols);
                assert.equal(true, Array.isArray(data.response.malwareAnalysisOptions));
                assert.equal(true, Array.isArray(data.response.storeFiles));
                assert.equal('UPLOAD', data.response.directionOfTransfer);
                assert.equal('MALWARE_CLOUD_LOOKUP', data.response.ruleAction);
                assert.equal(false, data.response.reset);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal('filerule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'getFileRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let routingParentId = 'fakedata';
    let routingObjId = 'fakedata';
    const routingAddVirtualRouterBodyParam = {
      name: 'string',
      isSystemDefined: true,
      type: 'virtualrouter'
    };
    describe('#addVirtualRouter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addVirtualRouter(routingAddVirtualRouterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('string', data.response.id);
                assert.equal('virtualrouter', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              routingParentId = data.response.id;
              routingObjId = data.response.id;
              saveMockData('Routing', 'addVirtualRouter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingAddStaticRouteEntryBodyParam = {
      name: 'string',
      iface: {
        type: 'string',
        name: 'string'
      },
      networks: [
        {
          type: 'string',
          name: 'string'
        }
      ],
      ipType: 'IPv6',
      type: 'staticrouteentry'
    };
    describe('#addStaticRouteEntry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addStaticRouteEntry(null, routingParentId, routingAddStaticRouteEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.iface);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('object', typeof data.response.gateway);
                assert.equal(5, data.response.metricValue);
                assert.equal('IPv6', data.response.ipType);
                assert.equal('object', typeof data.response.slaMonitor);
                assert.equal(8, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('staticrouteentry', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'addStaticRouteEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualRouterList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualRouterList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'getVirtualRouterList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingEditVirtualRouterBodyParam = {
      name: 'string',
      isSystemDefined: false,
      type: 'virtualrouter'
    };
    describe('#editVirtualRouter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editVirtualRouter(routingObjId, routingEditVirtualRouterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'editVirtualRouter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualRouter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualRouter(routingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('string', data.response.id);
                assert.equal('virtualrouter', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'getVirtualRouter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStaticRouteEntryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStaticRouteEntryList(null, null, null, null, routingParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'getStaticRouteEntryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingEditStaticRouteEntryBodyParam = {
      name: 'string',
      iface: {
        type: 'string',
        name: 'string'
      },
      networks: [
        {
          type: 'string',
          name: 'string'
        }
      ],
      ipType: 'IPv6',
      type: 'staticrouteentry'
    };
    describe('#editStaticRouteEntry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editStaticRouteEntry(null, routingParentId, routingObjId, routingEditStaticRouteEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'editStaticRouteEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStaticRouteEntry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStaticRouteEntry(routingParentId, routingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.iface);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('object', typeof data.response.gateway);
                assert.equal(5, data.response.metricValue);
                assert.equal('IPv4', data.response.ipType);
                assert.equal('object', typeof data.response.slaMonitor);
                assert.equal(5, data.response.rulePosition);
                assert.equal('string', data.response.id);
                assert.equal('staticrouteentry', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'getStaticRouteEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationImportExportAddScheduleConfigExportBodyParam = {
      configExportType: 'PENDING_CHANGE_EXPORT',
      type: 'scheduleconfigexport'
    };
    describe('#addScheduleConfigExport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addScheduleConfigExport(configurationImportExportAddScheduleConfigExportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('HOURLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.diskFileName);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal(false, data.response.doNotEncrypt);
                assert.equal('PENDING_CHANGE_EXPORT', data.response.configExportType);
                assert.equal(true, data.response.deployedObjectsOnly);
                assert.equal(true, Array.isArray(data.response.entityIds));
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('scheduleconfigexport', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'addScheduleConfigExport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationImportExportAddScheduleConfigImportBodyParam = {
      type: 'scheduleconfigimport'
    };
    describe('#addScheduleConfigImport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addScheduleConfigImport(configurationImportExportAddScheduleConfigImportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('WEEKLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.diskFileName);
                assert.equal('*********', data.response.encryptionKey);
                assert.equal(false, data.response.preserveConfigFile);
                assert.equal(false, data.response.autoDeploy);
                assert.equal(false, data.response.allowPendingChange);
                assert.equal(true, Array.isArray(data.response.inputEntities));
                assert.equal(true, Array.isArray(data.response.excludeEntities));
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('scheduleconfigimport', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'addScheduleConfigImport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadconfigfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postuploadconfigfile(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.diskFileName);
                assert.equal('string', data.response.dateModified);
                assert.equal(5, data.response.sizeBytes);
                assert.equal('string', data.response.id);
                assert.equal('ConfigImportExportFileInfo', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'postuploadconfigfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleConfigExportList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleConfigExportList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getScheduleConfigExportList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigImportExportFileInfoList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigImportExportFileInfoList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getConfigImportExportFileInfoList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationImportExportObjId = 'fakedata';
    describe('#getConfigImportExportFileInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigImportExportFileInfo(configurationImportExportObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.diskFileName);
                assert.equal('string', data.response.dateModified);
                assert.equal(2, data.response.sizeBytes);
                assert.equal('string', data.response.id);
                assert.equal('ConfigImportExportFileInfo', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getConfigImportExportFileInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleConfigImportList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleConfigImportList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getScheduleConfigImportList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadConfigFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadConfigFile(configurationImportExportObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'downloadConfigFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigExportJobStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigExportJobStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getConfigExportJobStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigExportJobStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigExportJobStatus(configurationImportExportObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('IN_PROGRESS', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.diskFileName);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal('FULL_EXPORT', data.response.configExportType);
                assert.equal(true, data.response.deployedObjectsOnly);
                assert.equal(true, Array.isArray(data.response.entityIds));
                assert.equal('string', data.response.id);
                assert.equal('configexportjobstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getConfigExportJobStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigImportJobStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigImportJobStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getConfigImportJobStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigImportJobStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigImportJobStatus(configurationImportExportObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobDescription);
                assert.equal('string', data.response.user);
                assert.equal('string', data.response.startDateTime);
                assert.equal('string', data.response.endDateTime);
                assert.equal('FAILED', data.response.status);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.scheduleUuid);
                assert.equal('string', data.response.diskFileName);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal('DELTA_IMPORT', data.response.configImportType);
                assert.equal(false, data.response.preserveConfigFile);
                assert.equal(false, data.response.autoDeploy);
                assert.equal(true, data.response.allowPendingChange);
                assert.equal(true, Array.isArray(data.response.excludeEntities));
                assert.equal('string', data.response.id);
                assert.equal('configimportjobstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'getConfigImportJobStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionSettingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntrusionSettingsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'getIntrusionSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intrusionPolicyObjId = 'fakedata';
    const intrusionPolicyEditIntrusionSettingsBodyParam = {
      type: 'intrusionsettings'
    };
    describe('#editIntrusionSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIntrusionSettings(intrusionPolicyObjId, intrusionPolicyEditIntrusionSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'editIntrusionSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntrusionSettings(intrusionPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.syslogServer);
                assert.equal('string', data.response.id);
                assert.equal('intrusionsettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'getIntrusionSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntrusionPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'getIntrusionPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intrusionPolicyEditIntrusionPolicyBodyParam = {
      type: 'intrusionpolicy'
    };
    describe('#editIntrusionPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIntrusionPolicy(intrusionPolicyObjId, intrusionPolicyEditIntrusionPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'editIntrusionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntrusionPolicy(intrusionPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.rules);
                assert.equal('PREVENTION', data.response.inspectionMode);
                assert.equal('string', data.response.id);
                assert.equal('intrusionpolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'getIntrusionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intrusionPolicyEditIntrusionPolicyRuleUpdateBodyParam = {
      type: 'intrusionpolicyruleupdate'
    };
    describe('#editIntrusionPolicyRuleUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIntrusionPolicyRuleUpdate(intrusionPolicyObjId, intrusionPolicyEditIntrusionPolicyRuleUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'editIntrusionPolicyRuleUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intrusionPolicyParentId = 'fakedata';
    describe('#getIntrusionRuleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntrusionRuleList(null, null, null, null, intrusionPolicyParentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'getIntrusionRuleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntrusionRule(intrusionPolicyParentId, intrusionPolicyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.gid);
                assert.equal(1, data.response.sid);
                assert.equal(2, data.response.revision);
                assert.equal('string', data.response.msg);
                assert.equal('string', data.response.ruleData);
                assert.equal('DROP', data.response.defaultState);
                assert.equal('ALERT', data.response.overrideState);
                assert.equal('string', data.response.id);
                assert.equal('intrusionrule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IntrusionPolicy', 'getIntrusionRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceMonitoringObjId = 'fakedata';
    describe('#getTrendingReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrendingReport(null, deviceMonitoringObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.title);
                assert.equal('TrendingReport', data.response.type);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceMonitoring', 'getTrendingReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiskUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDiskUsage(deviceMonitoringObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.title);
                assert.equal('DiskUsage', data.response.type);
                assert.equal(8, data.response.used);
                assert.equal(5, data.response.free);
                assert.equal(6, data.response.total);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceMonitoring', 'getDiskUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sLAMonitorAddSLAMonitorBodyParam = {
      name: 'string',
      monitoredAddress: {
        type: 'string',
        name: 'string'
      },
      targetInterface: {
        type: 'string',
        name: 'string'
      },
      slaOperation: {
        type: 'string',
        name: 'string'
      },
      type: 'slamonitor'
    };
    describe('#addSLAMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSLAMonitor(sLAMonitorAddSLAMonitorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.monitoredAddress);
                assert.equal('object', typeof data.response.targetInterface);
                assert.equal('object', typeof data.response.slaOperation);
                assert.equal('string', data.response.id);
                assert.equal('slamonitor', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLAMonitor', 'addSLAMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitorStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSLAMonitorStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLAMonitor', 'getSLAMonitorStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sLAMonitorObjId = 'fakedata';
    describe('#getSLAMonitorStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSLAMonitorStatus(sLAMonitorObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UP', data.response.operationalCode);
                assert.equal('string', data.response.id);
                assert.equal('SLAMonitorStatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLAMonitor', 'getSLAMonitorStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitorList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSLAMonitorList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLAMonitor', 'getSLAMonitorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sLAMonitorEditSLAMonitorBodyParam = {
      name: 'string',
      monitoredAddress: {
        type: 'string',
        name: 'string'
      },
      targetInterface: {
        type: 'string',
        name: 'string'
      },
      slaOperation: {
        type: 'string',
        name: 'string'
      },
      type: 'slamonitor'
    };
    describe('#editSLAMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSLAMonitor(sLAMonitorObjId, sLAMonitorEditSLAMonitorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLAMonitor', 'editSLAMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSLAMonitor(sLAMonitorObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.monitoredAddress);
                assert.equal('object', typeof data.response.targetInterface);
                assert.equal('object', typeof data.response.slaOperation);
                assert.equal('string', data.response.id);
                assert.equal('slamonitor', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLAMonitor', 'getSLAMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let securityIntelligenceObjId = 'fakedata';
    const securityIntelligenceAddSecurityIntelligenceUpdateFeedsImmediateBodyParam = {
      type: 'securityintelligenceupdatefeedsimmediate'
    };
    describe('#addSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSecurityIntelligenceUpdateFeedsImmediate(securityIntelligenceAddSecurityIntelligenceUpdateFeedsImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('IMMEDIATE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('securityintelligenceupdatefeedsimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              securityIntelligenceObjId = data.response.id;
              saveMockData('SecurityIntelligence', 'addSecurityIntelligenceUpdateFeedsImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceAddSecurityIntelligenceUpdateFeedsScheduleBodyParam = {
      type: 'securityintelligenceupdatefeedsschedule'
    };
    describe('#addSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSecurityIntelligenceUpdateFeedsSchedule(securityIntelligenceAddSecurityIntelligenceUpdateFeedsScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('securityintelligenceupdatefeedsschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'addSecurityIntelligenceUpdateFeedsSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceAddDomainNameFeedBodyParam = {
      name: 'string',
      feedURL: 'string',
      type: 'domainnamefeed'
    };
    describe('#addDomainNameFeed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDomainNameFeed(securityIntelligenceAddDomainNameFeedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('ONE_HOUR', data.response.updateFrequency);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.feedURL);
                assert.equal('string', data.response.checksumURL);
                assert.equal('string', data.response.id);
                assert.equal('domainnamefeed', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'addDomainNameFeed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsImmediateList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceUpdateFeedsImmediateList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceUpdateFeedsImmediateList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditSecurityIntelligenceUpdateFeedsImmediateBodyParam = {
      type: 'securityintelligenceupdatefeedsimmediate'
    };
    describe('#editSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecurityIntelligenceUpdateFeedsImmediate(securityIntelligenceObjId, securityIntelligenceEditSecurityIntelligenceUpdateFeedsImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editSecurityIntelligenceUpdateFeedsImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceUpdateFeedsImmediate(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('SINGLE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('securityintelligenceupdatefeedsimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceUpdateFeedsImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsScheduleList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceUpdateFeedsScheduleList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceUpdateFeedsScheduleList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditSecurityIntelligenceUpdateFeedsScheduleBodyParam = {
      type: 'securityintelligenceupdatefeedsschedule'
    };
    describe('#editSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecurityIntelligenceUpdateFeedsSchedule(securityIntelligenceObjId, securityIntelligenceEditSecurityIntelligenceUpdateFeedsScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editSecurityIntelligenceUpdateFeedsSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceUpdateFeedsSchedule(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(false, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.runTimes);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('securityintelligenceupdatefeedsschedule', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceUpdateFeedsSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeedCategoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameFeedCategoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getDomainNameFeedCategoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeedCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameFeedCategory(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.longDescription);
                assert.equal('string', data.response.shortDescription);
                assert.equal('string', data.response.id);
                assert.equal('domainnamefeedcategory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getDomainNameFeedCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeedList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameFeedList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getDomainNameFeedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditDomainNameFeedBodyParam = {
      name: 'string',
      feedURL: 'string',
      type: 'domainnamefeed'
    };
    describe('#editDomainNameFeed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDomainNameFeed(securityIntelligenceObjId, securityIntelligenceEditDomainNameFeedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editDomainNameFeed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameFeed(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('ONE_DAY', data.response.updateFrequency);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.feedURL);
                assert.equal('string', data.response.checksumURL);
                assert.equal('string', data.response.id);
                assert.equal('domainnamefeed', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getDomainNameFeed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getDomainNameGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditDomainNameGroupBodyParam = {
      type: 'domainnamegroup'
    };
    describe('#editDomainNameGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDomainNameGroup(securityIntelligenceObjId, securityIntelligenceEditDomainNameGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editDomainNameGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameGroup(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.domainNames));
                assert.equal('string', data.response.id);
                assert.equal('domainnamegroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getDomainNameGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFeedCategoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkFeedCategoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getNetworkFeedCategoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFeedCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkFeedCategory(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.longDescription);
                assert.equal('string', data.response.shortDescription);
                assert.equal('string', data.response.id);
                assert.equal('networkfeedcategory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getNetworkFeedCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemFeedObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemFeedObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSystemFeedObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditSystemFeedObjectBodyParam = {
      name: 'string',
      type: 'systemfeedobject'
    };
    describe('#editSystemFeedObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSystemFeedObject(securityIntelligenceObjId, securityIntelligenceEditSystemFeedObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editSystemFeedObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemFeedObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemFeedObject(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('SIX_HOURS', data.response.updateFrequency);
                assert.equal('string', data.response.lastUpdate);
                assert.equal('string', data.response.id);
                assert.equal('systemfeedobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSystemFeedObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLFeedCategoryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLFeedCategoryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getURLFeedCategoryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLFeedCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLFeedCategory(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.longDescription);
                assert.equal('string', data.response.shortDescription);
                assert.equal('string', data.response.id);
                assert.equal('urlfeedcategory', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getURLFeedCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceDNSPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceDNSPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceDNSPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditSecurityIntelligenceDNSPolicyBodyParam = {
      type: 'securityintelligencednspolicy'
    };
    describe('#editSecurityIntelligenceDNSPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecurityIntelligenceDNSPolicy(securityIntelligenceObjId, securityIntelligenceEditSecurityIntelligenceDNSPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editSecurityIntelligenceDNSPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceDNSPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceDNSPolicy(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.whitelistRules));
                assert.equal(true, Array.isArray(data.response.blacklistRules));
                assert.equal('string', data.response.id);
                assert.equal('securityintelligencednspolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceDNSPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceNetworkPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceNetworkPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceNetworkPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditSecurityIntelligenceNetworkPolicyBodyParam = {
      type: 'securityintelligencenetworkpolicy'
    };
    describe('#editSecurityIntelligenceNetworkPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecurityIntelligenceNetworkPolicy(securityIntelligenceObjId, securityIntelligenceEditSecurityIntelligenceNetworkPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editSecurityIntelligenceNetworkPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceNetworkPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceNetworkPolicy(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.whitelist));
                assert.equal(true, Array.isArray(data.response.blacklistForBlock));
                assert.equal('string', data.response.id);
                assert.equal('securityintelligencenetworkpolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceNetworkPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligencePolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligencePolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligencePolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditSecurityIntelligencePolicyBodyParam = {
      name: 'string',
      securityIntelligenceNetworkPolicy: {
        type: 'string',
        name: 'string'
      },
      securityIntelligenceURLPolicy: {
        type: 'string',
        name: 'string'
      },
      securityIntelligenceDNSPolicy: {
        type: 'string',
        name: 'string'
      },
      type: 'securityintelligencepolicy'
    };
    describe('#editSecurityIntelligencePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecurityIntelligencePolicy(securityIntelligenceObjId, securityIntelligenceEditSecurityIntelligencePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editSecurityIntelligencePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligencePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligencePolicy(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.securityIntelligenceNetworkPolicy);
                assert.equal('object', typeof data.response.securityIntelligenceURLPolicy);
                assert.equal('object', typeof data.response.securityIntelligenceDNSPolicy);
                assert.equal(true, data.response.logEnabled);
                assert.equal('object', typeof data.response.syslogServer);
                assert.equal('string', data.response.id);
                assert.equal('securityintelligencepolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligencePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceURLPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceURLPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceURLPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityIntelligenceEditSecurityIntelligenceURLPolicyBodyParam = {
      type: 'securityintelligenceurlpolicy'
    };
    describe('#editSecurityIntelligenceURLPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecurityIntelligenceURLPolicy(securityIntelligenceObjId, securityIntelligenceEditSecurityIntelligenceURLPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'editSecurityIntelligenceURLPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceURLPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityIntelligenceURLPolicy(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.whitelist));
                assert.equal(true, Array.isArray(data.response.blacklistForBlock));
                assert.equal('string', data.response.id);
                assert.equal('securityintelligenceurlpolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'getSecurityIntelligenceURLPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMetricDataList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceMetricDataList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceMetrics', 'getDeviceMetricDataList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceMetricsObjId = 'fakedata';
    describe('#getDeviceMetricData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceMetricData(deviceMetricsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.metric);
                assert.equal(8, data.response.timestamp);
                assert.equal('string', data.response.dateTime);
                assert.equal('string', data.response.id);
                assert.equal('DeviceMetricData', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceMetrics', 'getDeviceMetricData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMetricNodeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceMetricNodeList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceMetrics', 'getDeviceMetricNodeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMetricNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceMetricNode(deviceMetricsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.parent);
                assert.equal('string', data.response.id);
                assert.equal('DeviceMetricNode', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceMetrics', 'getDeviceMetricNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPTPList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PTP', 'getPTPList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pTPObjId = 'fakedata';
    const pTPEditPTPBodyParam = {
      type: 'ptp'
    };
    describe('#editPTP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editPTP(pTPObjId, pTPEditPTPBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PTP', 'editPTP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPTP(pTPObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.domainNumber);
                assert.equal('ENDTOENDTRANSPARENT', data.response.clockMode);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('string', data.response.id);
                assert.equal('ptp', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PTP', 'getPTP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let cloudServicesObjId = 'fakedata';
    const cloudServicesAddCloudManagementBodyParam = {
      type: 'cloudmanagement'
    };
    describe('#addCloudManagement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCloudManagement(cloudServicesAddCloudManagementBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('SINGLE', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.authToken);
                assert.equal('AUTO_ENROLL_NETWORK_PARTICIPATION', data.response.action);
                assert.equal('string', data.response.cloudRegion);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('cloudmanagement', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              cloudServicesObjId = data.response.id;
              saveMockData('CloudServices', 'addCloudManagement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudServicesAddCloudEnrollmentImmediateBodyParam = {
      region: 'string',
      accountType: 'SMART_VIRTUAL_ACCOUNT',
      type: 'cloudenrollmentimmediate'
    };
    describe('#addCloudEnrollmentImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCloudEnrollmentImmediate(cloudServicesAddCloudEnrollmentImmediateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('DAILY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.region);
                assert.equal('string', data.response.token);
                assert.equal('SMART_VIRTUAL_ACCOUNT', data.response.accountType);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('cloudenrollmentimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'addCloudEnrollmentImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCloudUnenrollmentImmediate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCloudUnenrollmentImmediate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('CRON', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('cloudunenrollmentimmediate', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'addCloudUnenrollmentImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudManagementList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudManagementList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudManagementList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudServicesEditCloudManagementBodyParam = {
      type: 'cloudmanagement'
    };
    describe('#editCloudManagement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editCloudManagement(cloudServicesObjId, cloudServicesEditCloudManagementBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'editCloudManagement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudManagement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudManagement(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('WEEKLY', data.response.scheduleType);
                assert.equal('string', data.response.user);
                assert.equal(true, data.response.forceOperation);
                assert.equal('string', data.response.jobHistoryUuid);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.authToken);
                assert.equal('TOKEN_ENROLL_CDO', data.response.action);
                assert.equal('string', data.response.cloudRegion);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.id);
                assert.equal('cloudmanagement', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudManagement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudCommunicationSettingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudCommunicationSettingsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudCommunicationSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudServicesEditCloudCommunicationSettingsBodyParam = {
      type: 'cloudcommunicationsettings'
    };
    describe('#editCloudCommunicationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editCloudCommunicationSettings(cloudServicesObjId, cloudServicesEditCloudCommunicationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'editCloudCommunicationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudCommunicationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudCommunicationSettings(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.telemetryEnabled);
                assert.equal(true, data.response.defenseOrchestratorEnabled);
                assert.equal(false, data.response.eventToCloudEnabled);
                assert.equal('string', data.response.id);
                assert.equal('cloudcommunicationsettings', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudCommunicationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefenseOrchestratorList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefenseOrchestratorList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getDefenseOrchestratorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudServicesEditDefenseOrchestratorBodyParam = {
      type: 'defenseorchestrator'
    };
    describe('#editDefenseOrchestrator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editDefenseOrchestrator(cloudServicesObjId, cloudServicesEditDefenseOrchestratorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'editDefenseOrchestrator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefenseOrchestrator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefenseOrchestrator(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.enableService);
                assert.equal('string', data.response.id);
                assert.equal('defenseorchestrator', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getDefenseOrchestrator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSuccessNetworkList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSuccessNetworkList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getSuccessNetworkList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudServicesEditSuccessNetworkBodyParam = {
      type: 'successnetwork'
    };
    describe('#editSuccessNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSuccessNetwork(cloudServicesObjId, cloudServicesEditSuccessNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'editSuccessNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSuccessNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSuccessNetwork(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.enableService);
                assert.equal('string', data.response.id);
                assert.equal('successnetwork', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getSuccessNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudEventsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudEventsList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudServicesEditCloudEventsBodyParam = {
      type: 'cloudevents'
    };
    describe('#editCloudEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editCloudEvents(cloudServicesObjId, cloudServicesEditCloudEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'editCloudEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudEvents(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.enableService);
                assert.equal(true, Array.isArray(data.response.eventTypes));
                assert.equal('string', data.response.id);
                assert.equal('cloudevents', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudRegionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudRegionList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudRegionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudRegion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudRegion(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.region);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('CloudRegion', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudServicesInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudServicesInfo(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.servicesPortalURL);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusDescription);
                assert.equal('object', typeof data.response.cloudRegion);
                assert.equal('SECURITY_ACCOUNT', data.response.accountType);
                assert.equal('string', data.response.id);
                assert.equal('CloudServicesInfo', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'getCloudServicesInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smartLicensingAddPLRReleaseCodeBodyParam = {
      type: 'PLRReleaseCode'
    };
    describe('#addPLRReleaseCode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPLRReleaseCode(smartLicensingAddPLRReleaseCodeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.code);
                assert.equal('string', data.response.authorizationCode);
                assert.equal('string', data.response.id);
                assert.equal('PLRReleaseCode', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'addPLRReleaseCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smartLicensingAddPLRAuthorizationCodeBodyParam = {
      type: 'PLRAuthorizationCode'
    };
    describe('#addPLRAuthorizationCode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPLRAuthorizationCode(smartLicensingAddPLRAuthorizationCodeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.code);
                assert.equal('string', data.response.id);
                assert.equal('PLRAuthorizationCode', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'addPLRAuthorizationCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smartLicensingAddSmartAgentConnectionBodyParam = {
      connectionType: 'UNIVERSAL_PLR',
      type: 'smartagentconnection'
    };
    describe('#addSmartAgentConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSmartAgentConnection(smartLicensingAddSmartAgentConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('EVALUATION', data.response.connectionType);
                assert.equal('string', data.response.token);
                assert.equal('string', data.response.id);
                assert.equal('smartagentconnection', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'addSmartAgentConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smartLicensingAddSmartAgentSyncRequestBodyParam = {
      sync: false,
      type: 'SmartAgentSyncRequest'
    };
    describe('#addSmartAgentSyncRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSmartAgentSyncRequest(smartLicensingAddSmartAgentSyncRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.sync);
                assert.equal('string', data.response.id);
                assert.equal('SmartAgentSyncRequest', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'addSmartAgentSyncRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smartLicensingAddLicenseBodyParam = {
      count: 9,
      licenseType: 'MALWARE',
      type: 'license'
    };
    describe('#addLicense - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addLicense(smartLicensingAddLicenseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(5, data.response.count);
                assert.equal(false, data.response.compliant);
                assert.equal('string', data.response.id);
                assert.equal('MALWARE', data.response.licenseType);
                assert.equal('license', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'addLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPLRRequestCodeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPLRRequestCodeList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getPLRRequestCodeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smartLicensingObjId = 'fakedata';
    describe('#getPLRRequestCode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPLRRequestCode(smartLicensingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.code);
                assert.equal('string', data.response.id);
                assert.equal('PLRRequestCode', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getPLRRequestCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentConnectionList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSmartAgentConnectionList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getSmartAgentConnectionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smartLicensingEditSmartAgentConnectionBodyParam = {
      connectionType: 'EVALUATION',
      type: 'smartagentconnection'
    };
    describe('#editSmartAgentConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSmartAgentConnection(smartLicensingObjId, smartLicensingEditSmartAgentConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'editSmartAgentConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSmartAgentConnection(smartLicensingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('EVALUATION', data.response.connectionType);
                assert.equal('string', data.response.token);
                assert.equal('string', data.response.id);
                assert.equal('smartagentconnection', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getSmartAgentConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSmartAgentStatusList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getSmartAgentStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSmartAgentStatus(smartLicensingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('RENEW_CERT_FAILED', data.response.registrationStatus);
                assert.equal(5, data.response.registrationTimeStamp);
                assert.equal('AUTHORIZED', data.response.authorizationStatus);
                assert.equal(6, data.response.authorizationTimeStamp);
                assert.equal('string', data.response.virtualAccount);
                assert.equal(true, data.response.exportControl);
                assert.equal(7, data.response.evaluationStartTime);
                assert.equal(2, data.response.evaluationRemainingDays);
                assert.equal(7, data.response.syncTimeStamp);
                assert.equal('string', data.response.id);
                assert.equal('smartagentstatus', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getSmartAgentStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicenseList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getLicenseList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicense - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicense(smartLicensingObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(8, data.response.count);
                assert.equal(true, data.response.compliant);
                assert.equal('string', data.response.id);
                assert.equal('URLFILTERING', data.response.licenseType);
                assert.equal('license', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'getLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let networkObjectObjId = 'fakedata';
    const networkObjectAddNetworkObjectGroupBodyParam = {
      name: 'string',
      type: 'networkobjectgroup'
    };
    describe('#addNetworkObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNetworkObjectGroup(networkObjectAddNetworkObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal('networkobjectgroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              networkObjectObjId = data.response.id;
              saveMockData('NetworkObject', 'addNetworkObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkObjectAddNetworkObjectBodyParam = {
      name: 'string',
      subType: 'NETWORK',
      value: 'string',
      type: 'networkobject'
    };
    describe('#addNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNetworkObject(networkObjectAddNetworkObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('NETWORK', data.response.subType);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('IPV4_AND_IPV6', data.response.dnsResolution);
                assert.equal('string', data.response.id);
                assert.equal('networkobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'addNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObjectGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkObjectGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'getNetworkObjectGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkObjectEditNetworkObjectGroupBodyParam = {
      name: 'string',
      type: 'networkobjectgroup'
    };
    describe('#editNetworkObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editNetworkObjectGroup(networkObjectObjId, networkObjectEditNetworkObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'editNetworkObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkObjectGroup(networkObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal('networkobjectgroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'getNetworkObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'getNetworkObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkObjectEditNetworkObjectBodyParam = {
      name: 'string',
      subType: 'RANGE',
      value: 'string',
      type: 'networkobject'
    };
    describe('#editNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editNetworkObject(networkObjectObjId, networkObjectEditNetworkObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'editNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkObject(networkObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('RANGE', data.response.subType);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('IPV4_AND_IPV6', data.response.dnsResolution);
                assert.equal('string', data.response.id);
                assert.equal('networkobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'getNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let portObjectObjId = 'fakedata';
    const portObjectAddICMPv4PortObjectBodyParam = {
      name: 'string',
      icmpv4Type: 'REDIRECT_MESSAGE',
      type: 'icmpv4portobject'
    };
    describe('#addICMPv4PortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addICMPv4PortObject(portObjectAddICMPv4PortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('PARAMETER_PROBLEM', data.response.icmpv4Type);
                assert.equal('DO_NOT_ROUTE_COMMON_TRAFFIC', data.response.icmpv4Code);
                assert.equal('string', data.response.id);
                assert.equal('icmpv4portobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              portObjectObjId = data.response.id;
              saveMockData('PortObject', 'addICMPv4PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectAddICMPv6PortObjectBodyParam = {
      name: 'string',
      icmpv6Type: 'MULTICAST_LISTENER_QUERY',
      type: 'icmpv6portobject'
    };
    describe('#addICMPv6PortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addICMPv6PortObject(portObjectAddICMPv6PortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('RPL_CTRL_MSG', data.response.icmpv6Type);
                assert.equal('DATA_CONTAINS_IPv6', data.response.icmpv6Code);
                assert.equal('string', data.response.id);
                assert.equal('icmpv6portobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'addICMPv6PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectAddPortObjectGroupBodyParam = {
      name: 'string',
      type: 'portobjectgroup'
    };
    describe('#addPortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPortObjectGroup(portObjectAddPortObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal('portobjectgroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'addPortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectAddProtocolObjectBodyParam = {
      name: 'string',
      protocol: 'FC',
      type: 'protocolobject'
    };
    describe('#addProtocolObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addProtocolObject(portObjectAddProtocolObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('BRSATMON', data.response.protocol);
                assert.equal('string', data.response.id);
                assert.equal('protocolobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'addProtocolObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectAddTCPPortObjectBodyParam = {
      name: 'string',
      port: 'string',
      type: 'tcpportobject'
    };
    describe('#addTCPPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTCPPortObject(portObjectAddTCPPortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.port);
                assert.equal('string', data.response.id);
                assert.equal('tcpportobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'addTCPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectAddUDPPortObjectBodyParam = {
      name: 'string',
      port: 'string',
      type: 'udpportobject'
    };
    describe('#addUDPPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addUDPPortObject(portObjectAddUDPPortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.port);
                assert.equal('string', data.response.id);
                assert.equal('udpportobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'addUDPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv4PortObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPv4PortObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getICMPv4PortObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectEditICMPv4PortObjectBodyParam = {
      name: 'string',
      icmpv4Type: 'TRACEROUTE',
      type: 'icmpv4portobject'
    };
    describe('#editICMPv4PortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editICMPv4PortObject(portObjectObjId, portObjectEditICMPv4PortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'editICMPv4PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv4PortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPv4PortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('WHERE_ARE_YOU', data.response.icmpv4Type);
                assert.equal('REDIRECT_DATAGRAM_SERVICE_HOST', data.response.icmpv4Code);
                assert.equal('string', data.response.id);
                assert.equal('icmpv4portobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getICMPv4PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv6PortObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPv6PortObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getICMPv6PortObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectEditICMPv6PortObjectBodyParam = {
      name: 'string',
      icmpv6Type: 'HOME_AGENT_ADDR_DISCOVERY_REPLY',
      type: 'icmpv6portobject'
    };
    describe('#editICMPv6PortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editICMPv6PortObject(portObjectObjId, portObjectEditICMPv6PortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'editICMPv6PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv6PortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPv6PortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('TIME_EXCEEDED', data.response.icmpv6Type);
                assert.equal('ROUTER_RENUMBERING_RESULT', data.response.icmpv6Code);
                assert.equal('string', data.response.id);
                assert.equal('icmpv6portobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getICMPv6PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortObjectGroupList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getPortObjectGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectEditPortObjectGroupBodyParam = {
      name: 'string',
      type: 'portobjectgroup'
    };
    describe('#editPortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editPortObjectGroup(portObjectObjId, portObjectEditPortObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'editPortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortObjectGroup(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal('portobjectgroup', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getPortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProtocolObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getProtocolObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectEditProtocolObjectBodyParam = {
      name: 'string',
      protocol: 'DIVERT',
      type: 'protocolobject'
    };
    describe('#editProtocolObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editProtocolObject(portObjectObjId, portObjectEditProtocolObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'editProtocolObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProtocolObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('IPCV', data.response.protocol);
                assert.equal('string', data.response.id);
                assert.equal('protocolobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getProtocolObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTCPPortObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTCPPortObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getTCPPortObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectEditTCPPortObjectBodyParam = {
      name: 'string',
      port: 'string',
      type: 'tcpportobject'
    };
    describe('#editTCPPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editTCPPortObject(portObjectObjId, portObjectEditTCPPortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'editTCPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTCPPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTCPPortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.isSystemDefined);
                assert.equal('string', data.response.port);
                assert.equal('string', data.response.id);
                assert.equal('tcpportobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getTCPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUDPPortObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUDPPortObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getUDPPortObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portObjectEditUDPPortObjectBodyParam = {
      name: 'string',
      port: 'string',
      type: 'udpportobject'
    };
    describe('#editUDPPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editUDPPortObject(portObjectObjId, portObjectEditUDPPortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'editUDPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUDPPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUDPPortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isSystemDefined);
                assert.equal('string', data.response.port);
                assert.equal('string', data.response.id);
                assert.equal('udpportobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'getUDPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const featureCapabilitiesObjId = 'fakedata';
    describe('#getFeatureCapabilities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFeatureCapabilities(featureCapabilitiesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.featureMap);
                assert.equal('string', data.response.id);
                assert.equal('FeatureCapabilities', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FeatureCapabilities', 'getFeatureCapabilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPProxyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHTTPProxyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HTTPProxy', 'getHTTPProxyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hTTPProxyObjId = 'fakedata';
    const hTTPProxyEditHTTPProxyBodyParam = {
      type: 'httpproxy'
    };
    describe('#editHTTPProxy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editHTTPProxy(hTTPProxyObjId, hTTPProxyEditHTTPProxyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HTTPProxy', 'editHTTPProxy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPProxy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHTTPProxy(hTTPProxyObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.enabled);
                assert.equal(true, data.response.authenticate);
                assert.equal('string', data.response.proxyServer);
                assert.equal(6, data.response.port);
                assert.equal('string', data.response.username);
                assert.equal('*********', data.response.password);
                assert.equal('string', data.response.id);
                assert.equal('httpproxy', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HTTPProxy', 'getHTTPProxy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let secretObjId = 'fakedata';
    const secretAddSecretBodyParam = {
      name: 'string',
      password: samProps.authentication.password,
      type: 'secret'
    };
    describe('#addSecret - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSecret(secretAddSecretBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('*********', data.response.password);
                assert.equal('string', data.response.id);
                assert.equal('secret', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              secretObjId = data.response.id;
              saveMockData('Secret', 'addSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecretList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secret', 'getSecretList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretEditSecretBodyParam = {
      name: 'string',
      password: samProps.authentication.password,
      type: 'secret'
    };
    describe('#editSecret - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editSecret(secretObjId, secretEditSecretBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secret', 'editSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecret - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecret(secretObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('*********', data.response.password);
                assert.equal('string', data.response.id);
                assert.equal('secret', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secret', 'getSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let timeRangeObjId = 'fakedata';
    const timeRangeAddTimeRangeObjectBodyParam = {
      name: 'string',
      type: 'timerangeobject'
    };
    describe('#addTimeRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTimeRangeObject(timeRangeAddTimeRangeObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.effectiveStartDateTime);
                assert.equal('string', data.response.effectiveEndDateTime);
                assert.equal(true, Array.isArray(data.response.recurrenceList));
                assert.equal(1, data.response.timeRangeObjectId);
                assert.equal('string', data.response.id);
                assert.equal('timerangeobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              timeRangeObjId = data.response.id;
              saveMockData('TimeRange', 'addTimeRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRangeObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeRangeObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRange', 'getTimeRangeObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeRangeEditTimeRangeObjectBodyParam = {
      name: 'string',
      type: 'timerangeobject'
    };
    describe('#editTimeRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editTimeRangeObject(timeRangeObjId, timeRangeEditTimeRangeObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRange', 'editTimeRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeRangeObject(timeRangeObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.effectiveStartDateTime);
                assert.equal('string', data.response.effectiveEndDateTime);
                assert.equal(true, Array.isArray(data.response.recurrenceList));
                assert.equal(5, data.response.timeRangeObjectId);
                assert.equal('string', data.response.id);
                assert.equal('timerangeobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRange', 'getTimeRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneSettingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeZoneSettingList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZoneSettings', 'getTimeZoneSettingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeZoneSettingsObjId = 'fakedata';
    const timeZoneSettingsEditTimeZoneSettingBodyParam = {
      type: 'timezonesetting'
    };
    describe('#editTimeZoneSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editTimeZoneSetting(timeZoneSettingsObjId, timeZoneSettingsEditTimeZoneSettingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZoneSettings', 'editTimeZoneSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneSetting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeZoneSetting(timeZoneSettingsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.timeZoneObject);
                assert.equal('string', data.response.id);
                assert.equal('timezonesetting', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZoneSettings', 'getTimeZoneSetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let timeZoneObjectsObjId = 'fakedata';
    const timeZoneObjectsAddTimeZoneObjectBodyParam = {
      name: 'string',
      type: 'timezoneobject'
    };
    describe('#addTimeZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTimeZoneObject(timeZoneObjectsAddTimeZoneObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal('object', typeof data.response.dstDateRange);
                assert.equal('object', typeof data.response.dstDayRecurrence);
                assert.equal('string', data.response.id);
                assert.equal('timezoneobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              timeZoneObjectsObjId = data.response.id;
              saveMockData('TimeZoneObjects', 'addTimeZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneObjectList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeZoneObjectList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZoneObjects', 'getTimeZoneObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeZoneObjectsEditTimeZoneObjectBodyParam = {
      name: 'string',
      type: 'timezoneobject'
    };
    describe('#editTimeZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editTimeZoneObject(timeZoneObjectsObjId, timeZoneObjectsEditTimeZoneObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZoneObjects', 'editTimeZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeZoneObject(timeZoneObjectsObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.timeZoneId);
                assert.equal('string', data.response.timeZone);
                assert.equal('object', typeof data.response.dstDateRange);
                assert.equal('object', typeof data.response.dstDayRecurrence);
                assert.equal('string', data.response.id);
                assert.equal('timezoneobject', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZoneObjects', 'getTimeZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeZonesObjId = 'fakedata';
    describe('#getTimeZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeZones(timeZonesObjId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.timeZones));
                assert.equal(true, Array.isArray(data.response.globalTimeZones));
                assert.equal('string', data.response.id);
                assert.equal('TimeZones', data.response.type);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZones', 'getTimeZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tokenTokenBodyParam = {
      grant_type: 'refresh_token'
    };
    describe('#token - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.token(tokenTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.access_token);
                assert.equal(1800, data.response.expires_in);
                assert.equal('Bearer', data.response.token_type);
                assert.equal('string', data.response.refresh_token);
                assert.equal(2400, data.response.refresh_expires_in);
                assert.equal(8, data.response.status_code);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Token', 'token', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiVersions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.supportedVersions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiVersions', 'getApiVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBridgeGroupInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBridgeGroupInterface(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'deleteBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEtherChannelInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEtherChannelInterface(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'deleteEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEtherChannelSubInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEtherChannelSubInterface(interfaceParentId, interfaceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'deleteEtherChannelSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSubInterface(interfaceParentId, interfaceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'deleteSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVlanInterface(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'deleteVlanInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryInterfaceMigration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryInterfaceMigration(interfaceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'deleteJobHistoryInterfaceMigration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDataInterfaceManagementAccess - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDataInterfaceManagementAccess(dataInterfaceManagementAccessObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataInterfaceManagementAccess', 'deleteDataInterfaceManagementAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityZone(securityZoneObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityZone', 'deleteSecurityZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityServicesEngine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIdentityServicesEngine(identityServicesEngineObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityServicesEngine', 'deleteIdentityServicesEngine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStandardAccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteStandardAccessList(standardAccessListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardAccessList', 'deleteStandardAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtendedAccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtendedAccessList(extendedAccessListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedAccessList', 'deleteExtendedAccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteASPathList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteASPathList(aSPathListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathList', 'deleteASPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouteMap - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRouteMap(routeMapObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMap', 'deleteRouteMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStandardCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteStandardCommunityList(standardCommunityListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StandardCommunityList', 'deleteStandardCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExpandedCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExpandedCommunityList(expandedCommunityListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExpandedCommunityList', 'deleteExpandedCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPV4PrefixList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIPV4PrefixList(iPV4PrefixListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV4PrefixList', 'deleteIPV4PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPV6PrefixList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIPV6PrefixList(iPV6PrefixListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPV6PrefixList', 'deleteIPV6PrefixList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicyList(policyListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyList', 'deletePolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationFilter - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationFilter(applicationObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'deleteApplicationFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeoLocation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGeoLocation(geoLocationObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeoLocation', 'deleteGeoLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLObjectGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteURLObjectGroup(uRLObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'deleteURLObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteURLObject(uRLObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('URLObject', 'deleteURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeployment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeployment(deploymentObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'deleteDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBaseEntityDiff - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBaseEntityDiff((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PendingChanges', 'deleteBaseEntityDiff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExportConfigJobHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExportConfigJobHistory(exportConfigJobHistoryObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExportConfigJobHistory', 'deleteExportConfigJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBackupImmediate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBackupImmediate(backupImmediateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupImmediate', 'deleteBackupImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBackupScheduled - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBackupScheduled(backupScheduledObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackupScheduled', 'deleteBackupScheduled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestoreImmediate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestoreImmediate(restoreImmediateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestoreImmediate', 'deleteRestoreImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteArchivedBackup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteArchivedBackup(archivedBackupObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArchivedBackup', 'deleteArchivedBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeolocationUpdateImmediate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGeolocationUpdateImmediate(geolocationUpdateImmediateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateImmediate', 'deleteGeolocationUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeolocationUpdateSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGeolocationUpdateSchedule(geolocationUpdateScheduleObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeolocationUpdateSchedule', 'deleteGeolocationUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSRUUpdateImmediate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSRUUpdateImmediate(sRUUpdateImmediateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateImmediate', 'deleteSRUUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSRUUpdateSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSRUUpdateSchedule(sRUUpdateScheduleObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SRUUpdateSchedule', 'deleteSRUUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDBUpdateImmediate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVDBUpdateImmediate(vDBUpdateImmediateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateImmediate', 'deleteVDBUpdateImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDBUpdateSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVDBUpdateSchedule(vDBUpdateScheduleObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDBUpdateSchedule', 'deleteVDBUpdateSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePullUpgradeJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePullUpgradeJob(pullUpgradeJobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullUpgradeJob', 'deletePullUpgradeJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePullFileJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePullFileJob(pullFileJobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PullFileJob', 'deletePullFileJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryEntities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryEntities((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryEntities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryBackup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryBackup(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryCloudManagement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryCloudManagement(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryCloudManagement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryDeployment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryDeployment(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryGeolocation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryGeolocation(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryGeolocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryHaConfigSync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryHaConfigSync(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryHaConfigSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryEntity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryEntity(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryEntity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLicenseJobHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLicenseJobHistory(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteLicenseJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistorySruUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistorySruUpdate(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistorySruUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryVDBUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobHistoryVDBUpdate(jobObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Job', 'deleteJobHistoryVDBUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRaVpnGroupPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRaVpnGroupPolicy(raVpnGroupPolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnGroupPolicy', 'deleteRaVpnGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLdapAttributeMap - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLdapAttributeMap(ldapAttributeMapObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LdapAttributeMap', 'deleteLdapAttributeMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnyConnectPackageFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnyConnectPackageFile(anyConnectPackageFileObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectPackageFile', 'deleteAnyConnectPackageFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRaVpnConnectionProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRaVpnConnectionProfile(raVpnConnectionProfileParentId, raVpnConnectionProfileObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpnConnectionProfile', 'deleteRaVpnConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRaVpn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRaVpn(raVpnObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RaVpn', 'deleteRaVpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccessRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccessRule(accessPolicyParentId, accessPolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicy', 'deleteAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteManualNatRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteManualNatRule(nATParentId, nATObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'deleteManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectNatRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteObjectNatRule(nATParentId, nATObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NAT', 'deleteObjectNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnyConnectClientProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnyConnectClientProfile(anyConnectClientProfileObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnyConnectClientProfile', 'deleteAnyConnectClientProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevOnePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIkevOnePolicy(ikevOnePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOnePolicy', 'deleteIkevOnePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevOneProposal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIkevOneProposal(ikevOneProposalObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevOneProposal', 'deleteIkevOneProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevTwoPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIkevTwoPolicy(ikevTwoPolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoPolicy', 'deleteIkevTwoPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevTwoProposal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIkevTwoProposal(ikevTwoProposalObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkevTwoProposal', 'deleteIkevTwoProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSToSConnectionProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSToSConnectionProfile(sToSConnectionProfileObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SToSConnectionProfile', 'deleteSToSConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser(userObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'deleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSyslogServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSyslogServer(syslogServerObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyslogServer', 'deleteSyslogServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDNSServerGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDNSServerGroup(dNSObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteDNSServerGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSSLCipher - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSSLCipher(sSLCipherObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLCipher', 'deleteSSLCipher', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIdentityRule(identityPolicyParentId, identityPolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPolicy', 'deleteIdentityRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExternalCACertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExternalCACertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'deleteExternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExternalCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExternalCertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'deleteExternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternalCACertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternalCACertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'deleteInternalCACertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternalCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternalCertificate(certificateObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificate', 'deleteInternalCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActiveDirectoryRealm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteActiveDirectoryRealm(activeDirectoryRealmObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveDirectoryRealm', 'deleteActiveDirectoryRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScheduleTroubleshoot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteScheduleTroubleshoot(scheduleTroubleshootObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScheduleTroubleshoot', 'deleteScheduleTroubleshoot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTroubleshootJobHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTroubleshootJobHistory(troubleshootJobHistoryObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TroubleshootJobHistory', 'deleteTroubleshootJobHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUpgradeFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUpgradeFile(upgradeFileObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UpgradeFile', 'deleteUpgradeFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSGTDynamicObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSGTDynamicObject(sGTDynamicObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SGTDynamicObject', 'deleteSGTDynamicObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSSLRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSSLRule(sSLPolicyParentId, sSLPolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSLPolicy', 'deleteSSLRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOSPF - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOSPF(oSPFVrfId, oSPFObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPF', 'deleteOSPF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOSPFInterfaceSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOSPFInterfaceSettings(oSPFInterfaceSettingsVrfId, oSPFInterfaceSettingsObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSPFInterfaceSettings', 'deleteOSPFInterfaceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBGP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBGP(bGPVrfId, bGPObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGP', 'deleteBGP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBGPGeneralSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBGPGeneralSettings(bGPGeneralSettingsObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGPGeneralSettings', 'deleteBGPGeneralSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFlexConfigObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFlexConfigObject(flexConfigObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigObject', 'deleteFlexConfigObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFlexConfigPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFlexConfigPolicy(flexConfigPolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlexConfigPolicy', 'deleteFlexConfigPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRadiusIdentitySource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRadiusIdentitySource(radiusIdentitySourceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySource', 'deleteRadiusIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActiveUserSessions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteActiveUserSessions(activeUserSessionsObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveUserSessions', 'deleteActiveUserSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRadiusIdentitySourceGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRadiusIdentitySourceGroup(radiusIdentitySourceGroupObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RadiusIdentitySourceGroup', 'deleteRadiusIdentitySourceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomLoggingList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomLoggingList(customLoggingListObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomLoggingList', 'deleteCustomLoggingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHitCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteHitCount(null, hitCountParentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HitCount', 'deleteHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDuoLDAPIdentitySource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDuoLDAPIdentitySource(duoLDAPIdentitySourceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DuoLDAPIdentitySource', 'deleteDuoLDAPIdentitySource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCleanList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCleanList(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'deleteCleanList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomDetectionList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomDetectionList(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'deleteCustomDetectionList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFilePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFilePolicy(fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'deleteFilePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFileRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFileRule(fileAndMalwarePolicyParentId, fileAndMalwarePolicyObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileAndMalwarePolicy', 'deleteFileRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualRouter - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVirtualRouter(routingObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'deleteVirtualRouter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStaticRouteEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteStaticRouteEntry(routingParentId, routingObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'deleteStaticRouteEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigImportExportFileInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteConfigImportExportFileInfo(configurationImportExportObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'deleteConfigImportExportFileInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigExportJobStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteConfigExportJobStatus(configurationImportExportObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'deleteConfigExportJobStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigImportJobStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteConfigImportJobStatus(configurationImportExportObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationImportExport', 'deleteConfigImportJobStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSLAMonitor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSLAMonitor(sLAMonitorObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLAMonitor', 'deleteSLAMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityIntelligenceUpdateFeedsImmediate(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'deleteSecurityIntelligenceUpdateFeedsImmediate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityIntelligenceUpdateFeedsSchedule(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'deleteSecurityIntelligenceUpdateFeedsSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNameFeed - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDomainNameFeed(securityIntelligenceObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityIntelligence', 'deleteDomainNameFeed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudManagement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCloudManagement(cloudServicesObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudServices', 'deleteCloudManagement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSmartAgentConnection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSmartAgentConnection(smartLicensingObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'deleteSmartAgentConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLicense(smartLicensingObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SmartLicensing', 'deleteLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkObjectGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkObjectGroup(networkObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'deleteNetworkObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkObject(networkObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkObject', 'deleteNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPv4PortObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteICMPv4PortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'deleteICMPv4PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPv6PortObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteICMPv6PortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'deleteICMPv6PortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortObjectGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePortObjectGroup(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'deletePortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProtocolObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteProtocolObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'deleteProtocolObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTCPPortObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTCPPortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'deleteTCPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUDPPortObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUDPPortObject(portObjectObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortObject', 'deleteUDPPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecret - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecret(secretObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secret', 'deleteSecret', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeRangeObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTimeRangeObject(timeRangeObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRange', 'deleteTimeRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeZoneObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTimeZoneObject(timeZoneObjectsObjId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowerthreatdefense-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeZoneObjects', 'deleteTimeZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
