## 0.3.0 [01-04-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-cisco_firepowerthreatdefense!3

---