## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco Firepowerthreatdefense. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco Firepowerthreatdefense.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Firepower_threat_defense . The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceMigrationImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getInterfaceMigrationImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/interfacemigration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInterfaceMigrationImmediate(body, callback)</td>
    <td style="padding:15px">addInterfaceMigrationImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/interfacemigration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryInterfaceMigrationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryInterfaceMigrationList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/interfacemigrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryInterfaceMigration(objId, callback)</td>
    <td style="padding:15px">getJobHistoryInterfaceMigration</td>
    <td style="padding:15px">{base_path}/{version}/jobs/interfacemigrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryInterfaceMigration(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryInterfaceMigration</td>
    <td style="padding:15px">{base_path}/{version}/jobs/interfacemigrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhysicalInterfaceList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPhysicalInterfaceList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhysicalInterface(objId, callback)</td>
    <td style="padding:15px">getPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editPhysicalInterface(objId, body, callback)</td>
    <td style="padding:15px">editPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeGroupInterfaceList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getBridgeGroupInterfaceList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/bridgegroupinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBridgeGroupInterface(body, callback)</td>
    <td style="padding:15px">addBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/bridgegroupinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeGroupInterface(objId, callback)</td>
    <td style="padding:15px">getBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/bridgegroupinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editBridgeGroupInterface(objId, body, callback)</td>
    <td style="padding:15px">editBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/bridgegroupinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBridgeGroupInterface(objId, callback)</td>
    <td style="padding:15px">deleteBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/bridgegroupinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubInterfaceList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getSubInterfaceList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces/{pathv1}/subinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubInterface(at, parentId, body, callback)</td>
    <td style="padding:15px">addSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces/{pathv1}/subinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubInterface(parentId, objId, callback)</td>
    <td style="padding:15px">getSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces/{pathv1}/subinterfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSubInterface(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces/{pathv1}/subinterfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubInterface(parentId, objId, callback)</td>
    <td style="padding:15px">deleteSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/interfaces/{pathv1}/subinterfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanInterfaceList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getVlanInterfaceList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/vlaninterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVlanInterface(body, callback)</td>
    <td style="padding:15px">addVlanInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/vlaninterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanInterface(objId, callback)</td>
    <td style="padding:15px">getVlanInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/vlaninterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVlanInterface(objId, body, callback)</td>
    <td style="padding:15px">editVlanInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/vlaninterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVlanInterface(objId, callback)</td>
    <td style="padding:15px">deleteVlanInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/vlaninterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEtherChannelInterfaceList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getEtherChannelInterfaceList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEtherChannelInterface(body, callback)</td>
    <td style="padding:15px">addEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEtherChannelInterface(objId, callback)</td>
    <td style="padding:15px">getEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editEtherChannelInterface(objId, body, callback)</td>
    <td style="padding:15px">editEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEtherChannelInterface(objId, callback)</td>
    <td style="padding:15px">deleteEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEtherChannelSubInterfaceList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getEtherChannelSubInterfaceList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}/subinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEtherChannelSubInterface(at, parentId, body, callback)</td>
    <td style="padding:15px">addEtherChannelSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}/subinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEtherChannelSubInterface(parentId, objId, callback)</td>
    <td style="padding:15px">getEtherChannelSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}/subinterfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editEtherChannelSubInterface(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editEtherChannelSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}/subinterfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEtherChannelSubInterface(parentId, objId, callback)</td>
    <td style="padding:15px">deleteEtherChannelSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/etherchannelinterfaces/{pathv1}/subinterfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceDataList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getInterfaceDataList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceData(objId, callback)</td>
    <td style="padding:15px">getInterfaceData</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemInformation(objId, callback)</td>
    <td style="padding:15px">getSystemInformation</td>
    <td style="padding:15px">{base_path}/{version}/operational/systeminfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlCategoryInfo(objId, callback)</td>
    <td style="padding:15px">getUrlCategoryInfo</td>
    <td style="padding:15px">{base_path}/{version}/operational/urlcategoryinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCommand(body, callback)</td>
    <td style="padding:15px">addCommand</td>
    <td style="padding:15px">{base_path}/{version}/action/command?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommandAutoComplete(objId, callback)</td>
    <td style="padding:15px">getCommandAutoComplete</td>
    <td style="padding:15px">{base_path}/{version}/operational/commandautocomplete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInterfacePresenceChange(body, callback)</td>
    <td style="padding:15px">addInterfacePresenceChange</td>
    <td style="padding:15px">{base_path}/{version}/action/interfacescan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeatureInformation(objId, callback)</td>
    <td style="padding:15px">getFeatureInformation</td>
    <td style="padding:15px">{base_path}/{version}/operational/featureinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelemetry(objId, callback)</td>
    <td style="padding:15px">getTelemetry</td>
    <td style="padding:15px">{base_path}/{version}/operational/telemetry/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHTTPAccessListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getHTTPAccessListList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/httpaccesslists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHTTPAccessList(objId, callback)</td>
    <td style="padding:15px">getHTTPAccessList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/httpaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editHTTPAccessList(objId, body, callback)</td>
    <td style="padding:15px">editHTTPAccessList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/httpaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSHAccessListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSSHAccessListList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/sshaccesslists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSHAccessList(objId, callback)</td>
    <td style="padding:15px">getSSHAccessList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/sshaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSSHAccessList(objId, body, callback)</td>
    <td style="padding:15px">editSSHAccessList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/sshaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataInterfaceManagementAccessList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDataInterfaceManagementAccessList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementaccess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDataInterfaceManagementAccess(body, callback)</td>
    <td style="padding:15px">addDataInterfaceManagementAccess</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementaccess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataInterfaceManagementAccess(objId, callback)</td>
    <td style="padding:15px">getDataInterfaceManagementAccess</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementaccess/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDataInterfaceManagementAccess(objId, body, callback)</td>
    <td style="padding:15px">editDataInterfaceManagementAccess</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementaccess/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDataInterfaceManagementAccess(objId, callback)</td>
    <td style="padding:15px">deleteDataInterfaceManagementAccess</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementaccess/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceHostnameList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDeviceHostnameList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/devicehostnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceHostname(objId, callback)</td>
    <td style="padding:15px">getDeviceHostname</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/devicehostnames/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDeviceHostname(objId, body, callback)</td>
    <td style="padding:15px">editDeviceHostname</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/devicehostnames/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebUICertificateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getWebUICertificateList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/webuicertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebUICertificate(objId, callback)</td>
    <td style="padding:15px">getWebUICertificate</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/webuicertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editWebUICertificate(objId, body, callback)</td>
    <td style="padding:15px">editWebUICertificate</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/webuicertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementIPList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getManagementIPList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementIP(objId, callback)</td>
    <td style="padding:15px">getManagementIP</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editManagementIP(objId, body, callback)</td>
    <td style="padding:15px">editManagementIP</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/managementips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityZoneList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityZoneList</td>
    <td style="padding:15px">{base_path}/{version}/object/securityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSecurityZone(body, callback)</td>
    <td style="padding:15px">addSecurityZone</td>
    <td style="padding:15px">{base_path}/{version}/object/securityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityZone(objId, callback)</td>
    <td style="padding:15px">getSecurityZone</td>
    <td style="padding:15px">{base_path}/{version}/object/securityzones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecurityZone(objId, body, callback)</td>
    <td style="padding:15px">editSecurityZone</td>
    <td style="padding:15px">{base_path}/{version}/object/securityzones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityZone(objId, callback)</td>
    <td style="padding:15px">deleteSecurityZone</td>
    <td style="padding:15px">{base_path}/{version}/object/securityzones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityServicesEngineList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIdentityServicesEngineList</td>
    <td style="padding:15px">{base_path}/{version}/integration/identityservicesengine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIdentityServicesEngine(body, callback)</td>
    <td style="padding:15px">addIdentityServicesEngine</td>
    <td style="padding:15px">{base_path}/{version}/integration/identityservicesengine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityServicesEngine(objId, callback)</td>
    <td style="padding:15px">getIdentityServicesEngine</td>
    <td style="padding:15px">{base_path}/{version}/integration/identityservicesengine/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIdentityServicesEngine(objId, body, callback)</td>
    <td style="padding:15px">editIdentityServicesEngine</td>
    <td style="padding:15px">{base_path}/{version}/integration/identityservicesengine/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityServicesEngine(objId, callback)</td>
    <td style="padding:15px">deleteIdentityServicesEngine</td>
    <td style="padding:15px">{base_path}/{version}/integration/identityservicesengine/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStandardAccessListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getStandardAccessListList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardaccesslists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addStandardAccessList(body, callback)</td>
    <td style="padding:15px">addStandardAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardaccesslists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStandardAccessList(objId, callback)</td>
    <td style="padding:15px">getStandardAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editStandardAccessList(objId, body, callback)</td>
    <td style="padding:15px">editStandardAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStandardAccessList(objId, callback)</td>
    <td style="padding:15px">deleteStandardAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtendedAccessListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getExtendedAccessListList</td>
    <td style="padding:15px">{base_path}/{version}/object/extendedaccesslists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExtendedAccessList(body, callback)</td>
    <td style="padding:15px">addExtendedAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/extendedaccesslists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtendedAccessList(objId, callback)</td>
    <td style="padding:15px">getExtendedAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/extendedaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editExtendedAccessList(objId, body, callback)</td>
    <td style="padding:15px">editExtendedAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/extendedaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtendedAccessList(objId, callback)</td>
    <td style="padding:15px">deleteExtendedAccessList</td>
    <td style="padding:15px">{base_path}/{version}/object/extendedaccesslists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getASPathListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getASPathListList</td>
    <td style="padding:15px">{base_path}/{version}/object/aspathlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addASPathList(body, callback)</td>
    <td style="padding:15px">addASPathList</td>
    <td style="padding:15px">{base_path}/{version}/object/aspathlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getASPathList(objId, callback)</td>
    <td style="padding:15px">getASPathList</td>
    <td style="padding:15px">{base_path}/{version}/object/aspathlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editASPathList(objId, body, callback)</td>
    <td style="padding:15px">editASPathList</td>
    <td style="padding:15px">{base_path}/{version}/object/aspathlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteASPathList(objId, callback)</td>
    <td style="padding:15px">deleteASPathList</td>
    <td style="padding:15px">{base_path}/{version}/object/aspathlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouteMapList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getRouteMapList</td>
    <td style="padding:15px">{base_path}/{version}/object/routemaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRouteMap(body, callback)</td>
    <td style="padding:15px">addRouteMap</td>
    <td style="padding:15px">{base_path}/{version}/object/routemaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouteMap(objId, callback)</td>
    <td style="padding:15px">getRouteMap</td>
    <td style="padding:15px">{base_path}/{version}/object/routemaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRouteMap(objId, body, callback)</td>
    <td style="padding:15px">editRouteMap</td>
    <td style="padding:15px">{base_path}/{version}/object/routemaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouteMap(objId, callback)</td>
    <td style="padding:15px">deleteRouteMap</td>
    <td style="padding:15px">{base_path}/{version}/object/routemaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStandardCommunityListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getStandardCommunityListList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardcommunitylists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addStandardCommunityList(body, callback)</td>
    <td style="padding:15px">addStandardCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardcommunitylists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStandardCommunityList(objId, callback)</td>
    <td style="padding:15px">getStandardCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardcommunitylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editStandardCommunityList(objId, body, callback)</td>
    <td style="padding:15px">editStandardCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardcommunitylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStandardCommunityList(objId, callback)</td>
    <td style="padding:15px">deleteStandardCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/standardcommunitylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExpandedCommunityListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getExpandedCommunityListList</td>
    <td style="padding:15px">{base_path}/{version}/object/expandedcommunitylists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExpandedCommunityList(body, callback)</td>
    <td style="padding:15px">addExpandedCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/expandedcommunitylists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExpandedCommunityList(objId, callback)</td>
    <td style="padding:15px">getExpandedCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/expandedcommunitylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editExpandedCommunityList(objId, body, callback)</td>
    <td style="padding:15px">editExpandedCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/expandedcommunitylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExpandedCommunityList(objId, callback)</td>
    <td style="padding:15px">deleteExpandedCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/object/expandedcommunitylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPV4PrefixListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIPV4PrefixListList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv4prefixlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIPV4PrefixList(body, callback)</td>
    <td style="padding:15px">addIPV4PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv4prefixlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPV4PrefixList(objId, callback)</td>
    <td style="padding:15px">getIPV4PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv4prefixlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIPV4PrefixList(objId, body, callback)</td>
    <td style="padding:15px">editIPV4PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv4prefixlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPV4PrefixList(objId, callback)</td>
    <td style="padding:15px">deleteIPV4PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv4prefixlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPV6PrefixListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIPV6PrefixListList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv6prefixlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIPV6PrefixList(body, callback)</td>
    <td style="padding:15px">addIPV6PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv6prefixlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPV6PrefixList(objId, callback)</td>
    <td style="padding:15px">getIPV6PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv6prefixlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIPV6PrefixList(objId, body, callback)</td>
    <td style="padding:15px">editIPV6PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv6prefixlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPV6PrefixList(objId, callback)</td>
    <td style="padding:15px">deleteIPV6PrefixList</td>
    <td style="padding:15px">{base_path}/{version}/object/ipv6prefixlists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPolicyListList</td>
    <td style="padding:15px">{base_path}/{version}/object/policylists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPolicyList(body, callback)</td>
    <td style="padding:15px">addPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/policylists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyList(objId, callback)</td>
    <td style="padding:15px">getPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/policylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editPolicyList(objId, body, callback)</td>
    <td style="padding:15px">editPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/policylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyList(objId, callback)</td>
    <td style="padding:15px">deletePolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/policylists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHardwareBypassList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getHardwareBypassList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/hardwarebypassinterfacepairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHardwareBypass(objId, callback)</td>
    <td style="padding:15px">getHardwareBypass</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/hardwarebypassinterfacepairs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editHardwareBypass(objId, body, callback)</td>
    <td style="padding:15px">editHardwareBypass</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/hardwarebypassinterfacepairs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getApplicationList</td>
    <td style="padding:15px">{base_path}/{version}/object/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationTagList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getApplicationTagList</td>
    <td style="padding:15px">{base_path}/{version}/object/applicationtags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationCategoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getApplicationCategoryList</td>
    <td style="padding:15px">{base_path}/{version}/object/applicationcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationFilterList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getApplicationFilterList</td>
    <td style="padding:15px">{base_path}/{version}/object/applicationfilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApplicationFilter(body, callback)</td>
    <td style="padding:15px">addApplicationFilter</td>
    <td style="padding:15px">{base_path}/{version}/object/applicationfilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationFilter(objId, callback)</td>
    <td style="padding:15px">getApplicationFilter</td>
    <td style="padding:15px">{base_path}/{version}/object/applicationfilters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editApplicationFilter(objId, body, callback)</td>
    <td style="padding:15px">editApplicationFilter</td>
    <td style="padding:15px">{base_path}/{version}/object/applicationfilters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationFilter(objId, callback)</td>
    <td style="padding:15px">deleteApplicationFilter</td>
    <td style="padding:15px">{base_path}/{version}/object/applicationfilters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCountryList</td>
    <td style="padding:15px">{base_path}/{version}/object/countries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountry(objId, callback)</td>
    <td style="padding:15px">getCountry</td>
    <td style="padding:15px">{base_path}/{version}/object/countries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContinentList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getContinentList</td>
    <td style="padding:15px">{base_path}/{version}/object/continents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContinent(objId, callback)</td>
    <td style="padding:15px">getContinent</td>
    <td style="padding:15px">{base_path}/{version}/object/continents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLCategoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getURLCategoryList</td>
    <td style="padding:15px">{base_path}/{version}/object/urlcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLReputationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getURLReputationList</td>
    <td style="padding:15px">{base_path}/{version}/object/urlreputation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoLocationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getGeoLocationList</td>
    <td style="padding:15px">{base_path}/{version}/object/geolocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGeoLocation(body, callback)</td>
    <td style="padding:15px">addGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/object/geolocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoLocation(objId, callback)</td>
    <td style="padding:15px">getGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/object/geolocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editGeoLocation(objId, body, callback)</td>
    <td style="padding:15px">editGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/object/geolocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeoLocation(objId, callback)</td>
    <td style="padding:15px">deleteGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/object/geolocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getURLObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addURLObject(body, callback)</td>
    <td style="padding:15px">addURLObject</td>
    <td style="padding:15px">{base_path}/{version}/object/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLObject(objId, callback)</td>
    <td style="padding:15px">getURLObject</td>
    <td style="padding:15px">{base_path}/{version}/object/urls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editURLObject(objId, body, callback)</td>
    <td style="padding:15px">editURLObject</td>
    <td style="padding:15px">{base_path}/{version}/object/urls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteURLObject(objId, callback)</td>
    <td style="padding:15px">deleteURLObject</td>
    <td style="padding:15px">{base_path}/{version}/object/urls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLObjectGroupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getURLObjectGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/urlgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addURLObjectGroup(body, callback)</td>
    <td style="padding:15px">addURLObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/urlgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLObjectGroup(objId, callback)</td>
    <td style="padding:15px">getURLObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/urlgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editURLObjectGroup(objId, body, callback)</td>
    <td style="padding:15px">editURLObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/urlgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteURLObjectGroup(objId, callback)</td>
    <td style="padding:15px">deleteURLObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/urlgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDeploymentList</td>
    <td style="padding:15px">{base_path}/{version}/operational/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeployment(callback)</td>
    <td style="padding:15px">addDeployment</td>
    <td style="padding:15px">{base_path}/{version}/operational/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeployment(objId, callback)</td>
    <td style="padding:15px">getDeployment</td>
    <td style="padding:15px">{base_path}/{version}/operational/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeployment(objId, callback)</td>
    <td style="padding:15px">deleteDeployment</td>
    <td style="padding:15px">{base_path}/{version}/operational/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigIssue(objId, callback)</td>
    <td style="padding:15px">getConfigIssue</td>
    <td style="padding:15px">{base_path}/{version}/operational/configissues/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJoinHAStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJoinHAStatusList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/join?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addJoinHAStatus(callback)</td>
    <td style="padding:15px">addJoinHAStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/join?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJoinHAStatus(objId, callback)</td>
    <td style="padding:15px">getJoinHAStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/join/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBreakHAStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getBreakHAStatusList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/break?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBreakHAStatus(clearIntfs, body, callback)</td>
    <td style="padding:15px">addBreakHAStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/break?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBreakHAStatus(objId, callback)</td>
    <td style="padding:15px">getBreakHAStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/break/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startHAResume(callback)</td>
    <td style="padding:15px">startHAResume</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startHASuspend(callback)</td>
    <td style="padding:15px">startHASuspend</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startHAFailover(callback)</td>
    <td style="padding:15px">startHAFailover</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/failover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addHAStatus(callback)</td>
    <td style="padding:15px">addHAStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/ha/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentData(objId, callback)</td>
    <td style="padding:15px">getDeploymentData</td>
    <td style="padding:15px">{base_path}/{version}/operational/deploymentdata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficInterruptionReasons(objId, callback)</td>
    <td style="padding:15px">getTrafficInterruptionReasons</td>
    <td style="padding:15px">{base_path}/{version}/operational/trafficinterruptionreasons/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClipboard(objId, callback)</td>
    <td style="padding:15px">getClipboard</td>
    <td style="padding:15px">{base_path}/{version}/operational/pendingchanges/clipboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownload(objId, callback)</td>
    <td style="padding:15px">getdownload</td>
    <td style="padding:15px">{base_path}/{version}/action/pendingchanges/download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBaseEntityDiffList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getBaseEntityDiffList</td>
    <td style="padding:15px">{base_path}/{version}/operational/pendingchanges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBaseEntityDiff(callback)</td>
    <td style="padding:15px">deleteBaseEntityDiff</td>
    <td style="padding:15px">{base_path}/{version}/operational/pendingchanges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadconfig(objId, callback)</td>
    <td style="padding:15px">getdownloadconfig</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadconfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloaddiskfile(objId, callback)</td>
    <td style="padding:15px">getdownloaddiskfile</td>
    <td style="padding:15px">{base_path}/{version}/action/downloaddiskfile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadinternalcacertificate(objId, callback)</td>
    <td style="padding:15px">getdownloadinternalcacertificate</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadinternalcacertificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadtroubleshoot(objId, callback)</td>
    <td style="padding:15px">getdownloadtroubleshoot</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadtroubleshoot/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadbackup(objId, callback)</td>
    <td style="padding:15px">getdownloadbackup</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadbackup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleExportConfigList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getScheduleExportConfigList</td>
    <td style="padding:15px">{base_path}/{version}/action/exportconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addScheduleExportConfig(body, callback)</td>
    <td style="padding:15px">addScheduleExportConfig</td>
    <td style="padding:15px">{base_path}/{version}/action/exportconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExportConfigJobHistoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getExportConfigJobHistoryList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/exportconfigjob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExportConfigJobHistory(objId, callback)</td>
    <td style="padding:15px">getExportConfigJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/jobs/exportconfigjob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExportConfigJobHistory(objId, callback)</td>
    <td style="padding:15px">deleteExportConfigJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/jobs/exportconfigjob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditEventList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAuditEventList</td>
    <td style="padding:15px">{base_path}/{version}/operational/auditevents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditEvent(objId, callback)</td>
    <td style="padding:15px">getAuditEvent</td>
    <td style="padding:15px">{base_path}/{version}/operational/auditevents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditEntityChangeList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getAuditEntityChangeList</td>
    <td style="padding:15px">{base_path}/{version}/operational/auditevents/{pathv1}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCliDeploymentError(objId, callback)</td>
    <td style="padding:15px">getCliDeploymentError</td>
    <td style="padding:15px">{base_path}/{version}/operational/deploymenterrors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getBackupImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBackupImmediate(body, callback)</td>
    <td style="padding:15px">addBackupImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupImmediate(objId, callback)</td>
    <td style="padding:15px">getBackupImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editBackupImmediate(objId, body, callback)</td>
    <td style="padding:15px">editBackupImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBackupImmediate(objId, callback)</td>
    <td style="padding:15px">deleteBackupImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupScheduledList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getBackupScheduledList</td>
    <td style="padding:15px">{base_path}/{version}/action/scheduledbackup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBackupScheduled(body, callback)</td>
    <td style="padding:15px">addBackupScheduled</td>
    <td style="padding:15px">{base_path}/{version}/action/scheduledbackup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupScheduled(objId, callback)</td>
    <td style="padding:15px">getBackupScheduled</td>
    <td style="padding:15px">{base_path}/{version}/action/scheduledbackup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editBackupScheduled(objId, body, callback)</td>
    <td style="padding:15px">editBackupScheduled</td>
    <td style="padding:15px">{base_path}/{version}/action/scheduledbackup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBackupScheduled(objId, callback)</td>
    <td style="padding:15px">deleteBackupScheduled</td>
    <td style="padding:15px">{base_path}/{version}/action/scheduledbackup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestoreImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getRestoreImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRestoreImmediate(body, callback)</td>
    <td style="padding:15px">addRestoreImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestoreImmediate(objId, callback)</td>
    <td style="padding:15px">getRestoreImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRestoreImmediate(objId, body, callback)</td>
    <td style="padding:15px">editRestoreImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestoreImmediate(objId, callback)</td>
    <td style="padding:15px">deleteRestoreImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArchivedBackupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getArchivedBackupList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/archivedbackups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArchivedBackup(objId, callback)</td>
    <td style="padding:15px">getArchivedBackup</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/archivedbackups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArchivedBackup(objId, callback)</td>
    <td style="padding:15px">deleteArchivedBackup</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/archivedbackups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeolocationUpdateImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getGeolocationUpdateImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/updategeolocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGeolocationUpdateImmediate(body, callback)</td>
    <td style="padding:15px">addGeolocationUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updategeolocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeolocationUpdateImmediate(objId, callback)</td>
    <td style="padding:15px">getGeolocationUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updategeolocation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editGeolocationUpdateImmediate(objId, body, callback)</td>
    <td style="padding:15px">editGeolocationUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updategeolocation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeolocationUpdateImmediate(objId, callback)</td>
    <td style="padding:15px">deleteGeolocationUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updategeolocation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeolocationUpdateScheduleList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getGeolocationUpdateScheduleList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/geolocationupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGeolocationUpdateSchedule(body, callback)</td>
    <td style="padding:15px">addGeolocationUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/geolocationupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeolocationUpdateSchedule(objId, callback)</td>
    <td style="padding:15px">getGeolocationUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/geolocationupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editGeolocationUpdateSchedule(objId, body, callback)</td>
    <td style="padding:15px">editGeolocationUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/geolocationupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeolocationUpdateSchedule(objId, callback)</td>
    <td style="padding:15px">deleteGeolocationUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/geolocationupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postupdategeolocationfromfile(fileToUpload, callback)</td>
    <td style="padding:15px">postupdategeolocationfromfile</td>
    <td style="padding:15px">{base_path}/{version}/action/updategeolocationfromfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRUUpdateImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSRUUpdateImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/updatesru?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSRUUpdateImmediate(body, callback)</td>
    <td style="padding:15px">addSRUUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatesru?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRUUpdateImmediate(objId, callback)</td>
    <td style="padding:15px">getSRUUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatesru/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSRUUpdateImmediate(objId, body, callback)</td>
    <td style="padding:15px">editSRUUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatesru/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSRUUpdateImmediate(objId, callback)</td>
    <td style="padding:15px">deleteSRUUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatesru/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRUUpdateScheduleList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSRUUpdateScheduleList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/sruupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSRUUpdateSchedule(body, callback)</td>
    <td style="padding:15px">addSRUUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/sruupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRUUpdateSchedule(objId, callback)</td>
    <td style="padding:15px">getSRUUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/sruupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSRUUpdateSchedule(objId, body, callback)</td>
    <td style="padding:15px">editSRUUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/sruupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSRUUpdateSchedule(objId, callback)</td>
    <td style="padding:15px">deleteSRUUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/sruupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSRUFileUpload(fileToUpload, uploadOnly, callback)</td>
    <td style="padding:15px">addSRUFileUpload</td>
    <td style="padding:15px">{base_path}/{version}/action/updatesrufromfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVDBUpdateImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getVDBUpdateImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/updatevdb?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVDBUpdateImmediate(body, callback)</td>
    <td style="padding:15px">addVDBUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatevdb?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVDBUpdateImmediate(objId, callback)</td>
    <td style="padding:15px">getVDBUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatevdb/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVDBUpdateImmediate(objId, body, callback)</td>
    <td style="padding:15px">editVDBUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatevdb/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVDBUpdateImmediate(objId, callback)</td>
    <td style="padding:15px">deleteVDBUpdateImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/updatevdb/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVDBUpdateScheduleList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getVDBUpdateScheduleList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/vdbupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVDBUpdateSchedule(body, callback)</td>
    <td style="padding:15px">addVDBUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/vdbupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVDBUpdateSchedule(objId, callback)</td>
    <td style="padding:15px">getVDBUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/vdbupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVDBUpdateSchedule(objId, body, callback)</td>
    <td style="padding:15px">editVDBUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/vdbupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVDBUpdateSchedule(objId, callback)</td>
    <td style="padding:15px">deleteVDBUpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/vdbupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postupdatevdbfromfile(fileToUpload, callback)</td>
    <td style="padding:15px">postupdatevdbfromfile</td>
    <td style="padding:15px">{base_path}/{version}/action/updatevdbfromfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullUpgradeJobList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPullUpgradeJobList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/pullupgradejob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullUpgradeJob(objId, callback)</td>
    <td style="padding:15px">getPullUpgradeJob</td>
    <td style="padding:15px">{base_path}/{version}/jobs/pullupgradejob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePullUpgradeJob(objId, callback)</td>
    <td style="padding:15px">deletePullUpgradeJob</td>
    <td style="padding:15px">{base_path}/{version}/jobs/pullupgradejob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullFileJobList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPullFileJobList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/pullfilejob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullFileJob(objId, callback)</td>
    <td style="padding:15px">getPullFileJob</td>
    <td style="padding:15px">{base_path}/{version}/jobs/pullfilejob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePullFileJob(objId, callback)</td>
    <td style="padding:15px">deletePullFileJob</td>
    <td style="padding:15px">{base_path}/{version}/jobs/pullfilejob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryBackupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryBackupList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryBackup(objId, callback)</td>
    <td style="padding:15px">getJobHistoryBackup</td>
    <td style="padding:15px">{base_path}/{version}/jobs/backups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryBackup(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryBackup</td>
    <td style="padding:15px">{base_path}/{version}/jobs/backups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryGeolocationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryGeolocationList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/geolocationupdates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryGeolocation(objId, callback)</td>
    <td style="padding:15px">getJobHistoryGeolocation</td>
    <td style="padding:15px">{base_path}/{version}/jobs/geolocationupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryGeolocation(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryGeolocation</td>
    <td style="padding:15px">{base_path}/{version}/jobs/geolocationupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistorySruUpdateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistorySruUpdateList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/sruupdates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistorySruUpdate(objId, callback)</td>
    <td style="padding:15px">getJobHistorySruUpdate</td>
    <td style="padding:15px">{base_path}/{version}/jobs/sruupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistorySruUpdate(objId, callback)</td>
    <td style="padding:15px">deleteJobHistorySruUpdate</td>
    <td style="padding:15px">{base_path}/{version}/jobs/sruupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryVDBUpdateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryVDBUpdateList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/vdbupdates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryVDBUpdate(objId, callback)</td>
    <td style="padding:15px">getJobHistoryVDBUpdate</td>
    <td style="padding:15px">{base_path}/{version}/jobs/vdbupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryVDBUpdate(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryVDBUpdate</td>
    <td style="padding:15px">{base_path}/{version}/jobs/vdbupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryDeploymentList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryDeploymentList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryDeployment(objId, callback)</td>
    <td style="padding:15px">getJobHistoryDeployment</td>
    <td style="padding:15px">{base_path}/{version}/jobs/deployments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryDeployment(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryDeployment</td>
    <td style="padding:15px">{base_path}/{version}/jobs/deployments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryHaConfigSyncList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryHaConfigSyncList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/haconfigsync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryHaConfigSync(objId, callback)</td>
    <td style="padding:15px">getJobHistoryHaConfigSync</td>
    <td style="padding:15px">{base_path}/{version}/jobs/haconfigsync/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryHaConfigSync(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryHaConfigSync</td>
    <td style="padding:15px">{base_path}/{version}/jobs/haconfigsync/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryCloudManagementList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryCloudManagementList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/cloudmanagementupdates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryCloudManagement(objId, callback)</td>
    <td style="padding:15px">getJobHistoryCloudManagement</td>
    <td style="padding:15px">{base_path}/{version}/jobs/cloudmanagementupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryCloudManagement(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryCloudManagement</td>
    <td style="padding:15px">{base_path}/{version}/jobs/cloudmanagementupdates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseJobHistoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getLicenseJobHistoryList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/licenseregistrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseJobHistory(objId, callback)</td>
    <td style="padding:15px">getLicenseJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/jobs/licenseregistrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLicenseJobHistory(objId, callback)</td>
    <td style="padding:15px">deleteLicenseJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/jobs/licenseregistrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryEntityList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getJobHistoryEntityList</td>
    <td style="padding:15px">{base_path}/{version}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryEntities(callback)</td>
    <td style="padding:15px">deleteJobHistoryEntities</td>
    <td style="padding:15px">{base_path}/{version}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobHistoryEntity(objId, callback)</td>
    <td style="padding:15px">getJobHistoryEntity</td>
    <td style="padding:15px">{base_path}/{version}/jobs/jobhistory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobHistoryEntity(objId, callback)</td>
    <td style="padding:15px">deleteJobHistoryEntity</td>
    <td style="padding:15px">{base_path}/{version}/jobs/jobhistory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaVpnGroupPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getRaVpnGroupPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/ravpngrouppolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRaVpnGroupPolicy(body, callback)</td>
    <td style="padding:15px">addRaVpnGroupPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ravpngrouppolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaVpnGroupPolicy(objId, callback)</td>
    <td style="padding:15px">getRaVpnGroupPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ravpngrouppolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRaVpnGroupPolicy(objId, body, callback)</td>
    <td style="padding:15px">editRaVpnGroupPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ravpngrouppolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRaVpnGroupPolicy(objId, callback)</td>
    <td style="padding:15px">deleteRaVpnGroupPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ravpngrouppolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapAttributeMapList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getLdapAttributeMapList</td>
    <td style="padding:15px">{base_path}/{version}/object/ldapattributemaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLdapAttributeMap(body, callback)</td>
    <td style="padding:15px">addLdapAttributeMap</td>
    <td style="padding:15px">{base_path}/{version}/object/ldapattributemaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapAttributeMap(objId, callback)</td>
    <td style="padding:15px">getLdapAttributeMap</td>
    <td style="padding:15px">{base_path}/{version}/object/ldapattributemaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editLdapAttributeMap(objId, body, callback)</td>
    <td style="padding:15px">editLdapAttributeMap</td>
    <td style="padding:15px">{base_path}/{version}/object/ldapattributemaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLdapAttributeMap(objId, callback)</td>
    <td style="padding:15px">deleteLdapAttributeMap</td>
    <td style="padding:15px">{base_path}/{version}/object/ldapattributemaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnyConnectPackageFileList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAnyConnectPackageFileList</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectpackagefiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAnyConnectPackageFile(body, callback)</td>
    <td style="padding:15px">addAnyConnectPackageFile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectpackagefiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnyConnectPackageFile(objId, callback)</td>
    <td style="padding:15px">getAnyConnectPackageFile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectpackagefiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAnyConnectPackageFile(objId, body, callback)</td>
    <td style="padding:15px">editAnyConnectPackageFile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectpackagefiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnyConnectPackageFile(objId, callback)</td>
    <td style="padding:15px">deleteAnyConnectPackageFile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectpackagefiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaVpnConnectionProfileList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getRaVpnConnectionProfileList</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}/ravpnconnectionprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRaVpnConnectionProfile(parentId, body, callback)</td>
    <td style="padding:15px">addRaVpnConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}/ravpnconnectionprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaVpnConnectionProfile(parentId, objId, callback)</td>
    <td style="padding:15px">getRaVpnConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}/ravpnconnectionprofiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRaVpnConnectionProfile(parentId, objId, body, callback)</td>
    <td style="padding:15px">editRaVpnConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}/ravpnconnectionprofiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRaVpnConnectionProfile(parentId, objId, callback)</td>
    <td style="padding:15px">deleteRaVpnConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}/ravpnconnectionprofiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaVpnList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getRaVpnList</td>
    <td style="padding:15px">{base_path}/{version}/ravpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRaVpn(body, callback)</td>
    <td style="padding:15px">addRaVpn</td>
    <td style="padding:15px">{base_path}/{version}/ravpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaVpn(objId, callback)</td>
    <td style="padding:15px">getRaVpn</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRaVpn(objId, body, callback)</td>
    <td style="padding:15px">editRaVpn</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRaVpn(objId, callback)</td>
    <td style="padding:15px">deleteRaVpn</td>
    <td style="padding:15px">{base_path}/{version}/ravpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessRuleList(offset, limit, sort, filter, includeHitCounts, parentId, callback)</td>
    <td style="padding:15px">getAccessRuleList</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/accessrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccessRule(at, parentId, body, callback)</td>
    <td style="padding:15px">addAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/accessrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessRule(includeHitCounts, parentId, objId, callback)</td>
    <td style="padding:15px">getAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/accessrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAccessRule(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/accessrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessRule(parentId, objId, callback)</td>
    <td style="padding:15px">deleteAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/accessrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAccessPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPolicy(objId, callback)</td>
    <td style="padding:15px">getAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAccessPolicy(objId, body, callback)</td>
    <td style="padding:15px">editAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManualNatRuleList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getManualNatRuleList</td>
    <td style="padding:15px">{base_path}/{version}/policy/manualnatpolicies/{pathv1}/manualnatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addManualNatRule(at, parentId, body, callback)</td>
    <td style="padding:15px">addManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/manualnatpolicies/{pathv1}/manualnatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManualNatRule(parentId, objId, callback)</td>
    <td style="padding:15px">getManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/manualnatpolicies/{pathv1}/manualnatrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editManualNatRule(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/manualnatpolicies/{pathv1}/manualnatrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteManualNatRule(parentId, objId, callback)</td>
    <td style="padding:15px">deleteManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/manualnatpolicies/{pathv1}/manualnatrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManualNatRuleContainerList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getManualNatRuleContainerList</td>
    <td style="padding:15px">{base_path}/{version}/policy/manualnatpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManualNatRuleContainer(objId, callback)</td>
    <td style="padding:15px">getManualNatRuleContainer</td>
    <td style="padding:15px">{base_path}/{version}/policy/manualnatpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectNatRuleList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getObjectNatRuleList</td>
    <td style="padding:15px">{base_path}/{version}/policy/objectnatpolicies/{pathv1}/objectnatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addObjectNatRule(at, parentId, body, callback)</td>
    <td style="padding:15px">addObjectNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/objectnatpolicies/{pathv1}/objectnatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectNatRule(parentId, objId, callback)</td>
    <td style="padding:15px">getObjectNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/objectnatpolicies/{pathv1}/objectnatrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editObjectNatRule(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editObjectNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/objectnatpolicies/{pathv1}/objectnatrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectNatRule(parentId, objId, callback)</td>
    <td style="padding:15px">deleteObjectNatRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/objectnatpolicies/{pathv1}/objectnatrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectNatRuleContainerList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getObjectNatRuleContainerList</td>
    <td style="padding:15px">{base_path}/{version}/policy/objectnatpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectNatRuleContainer(objId, callback)</td>
    <td style="padding:15px">getObjectNatRuleContainer</td>
    <td style="padding:15px">{base_path}/{version}/policy/objectnatpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploaddiskfile(fileToUpload, callback)</td>
    <td style="padding:15px">postuploaddiskfile</td>
    <td style="padding:15px">{base_path}/{version}/action/uploaddiskfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploadbackup(fileToUpload, callback)</td>
    <td style="padding:15px">postuploadbackup</td>
    <td style="padding:15px">{base_path}/{version}/action/uploadbackup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploadupgrade(fileToUpload, callback)</td>
    <td style="padding:15px">postuploadupgrade</td>
    <td style="padding:15px">{base_path}/{version}/action/uploadupgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnyConnectClientProfileList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAnyConnectClientProfileList</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectclientprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAnyConnectClientProfile(body, callback)</td>
    <td style="padding:15px">addAnyConnectClientProfile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectclientprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnyConnectClientProfile(objId, callback)</td>
    <td style="padding:15px">getAnyConnectClientProfile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectclientprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAnyConnectClientProfile(objId, body, callback)</td>
    <td style="padding:15px">editAnyConnectClientProfile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectclientprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnyConnectClientProfile(objId, callback)</td>
    <td style="padding:15px">deleteAnyConnectClientProfile</td>
    <td style="padding:15px">{base_path}/{version}/object/anyconnectclientprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevOnePolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIkevOnePolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIkevOnePolicy(body, callback)</td>
    <td style="padding:15px">addIkevOnePolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevOnePolicy(objId, callback)</td>
    <td style="padding:15px">getIkevOnePolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIkevOnePolicy(objId, body, callback)</td>
    <td style="padding:15px">editIkevOnePolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIkevOnePolicy(objId, callback)</td>
    <td style="padding:15px">deleteIkevOnePolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevOneProposalList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIkevOneProposalList</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1proposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIkevOneProposal(body, callback)</td>
    <td style="padding:15px">addIkevOneProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1proposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevOneProposal(objId, callback)</td>
    <td style="padding:15px">getIkevOneProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1proposals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIkevOneProposal(objId, body, callback)</td>
    <td style="padding:15px">editIkevOneProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1proposals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIkevOneProposal(objId, callback)</td>
    <td style="padding:15px">deleteIkevOneProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev1proposals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevTwoPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIkevTwoPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIkevTwoPolicy(body, callback)</td>
    <td style="padding:15px">addIkevTwoPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevTwoPolicy(objId, callback)</td>
    <td style="padding:15px">getIkevTwoPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIkevTwoPolicy(objId, body, callback)</td>
    <td style="padding:15px">editIkevTwoPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIkevTwoPolicy(objId, callback)</td>
    <td style="padding:15px">deleteIkevTwoPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevTwoProposalList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIkevTwoProposalList</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2proposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIkevTwoProposal(body, callback)</td>
    <td style="padding:15px">addIkevTwoProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2proposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkevTwoProposal(objId, callback)</td>
    <td style="padding:15px">getIkevTwoProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2proposals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIkevTwoProposal(objId, body, callback)</td>
    <td style="padding:15px">editIkevTwoProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2proposals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIkevTwoProposal(objId, callback)</td>
    <td style="padding:15px">deleteIkevTwoProposal</td>
    <td style="padding:15px">{base_path}/{version}/object/ikev2proposals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSToSConnectionProfileList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSToSConnectionProfileList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/s2sconnectionprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSToSConnectionProfile(body, callback)</td>
    <td style="padding:15px">addSToSConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/s2sconnectionprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSToSConnectionProfile(objId, callback)</td>
    <td style="padding:15px">getSToSConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/s2sconnectionprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSToSConnectionProfile(objId, body, callback)</td>
    <td style="padding:15px">editSToSConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/s2sconnectionprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSToSConnectionProfile(objId, callback)</td>
    <td style="padding:15px">deleteSToSConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/s2sconnectionprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getUserList</td>
    <td style="padding:15px">{base_path}/{version}/object/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUser(body, callback)</td>
    <td style="padding:15px">addUser</td>
    <td style="padding:15px">{base_path}/{version}/object/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(objId, callback)</td>
    <td style="padding:15px">getUser</td>
    <td style="padding:15px">{base_path}/{version}/object/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editUser(objId, body, callback)</td>
    <td style="padding:15px">editUser</td>
    <td style="padding:15px">{base_path}/{version}/object/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(objId, callback)</td>
    <td style="padding:15px">deleteUser</td>
    <td style="padding:15px">{base_path}/{version}/object/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNTPList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getNTPList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNTP(objId, callback)</td>
    <td style="padding:15px">getNTP</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ntp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editNTP(objId, body, callback)</td>
    <td style="padding:15px">editNTP</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ntp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLogSettingsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDeviceLogSettingsList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/logsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLogSettings(objId, callback)</td>
    <td style="padding:15px">getDeviceLogSettings</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/logsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDeviceLogSettings(objId, body, callback)</td>
    <td style="padding:15px">editDeviceLogSettings</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/logsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyslogServerList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSyslogServerList</td>
    <td style="padding:15px">{base_path}/{version}/object/syslogalerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSyslogServer(body, callback)</td>
    <td style="padding:15px">addSyslogServer</td>
    <td style="padding:15px">{base_path}/{version}/object/syslogalerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyslogServer(objId, callback)</td>
    <td style="padding:15px">getSyslogServer</td>
    <td style="padding:15px">{base_path}/{version}/object/syslogalerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSyslogServer(objId, body, callback)</td>
    <td style="padding:15px">editSyslogServer</td>
    <td style="padding:15px">{base_path}/{version}/object/syslogalerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSyslogServer(objId, callback)</td>
    <td style="padding:15px">deleteSyslogServer</td>
    <td style="padding:15px">{base_path}/{version}/object/syslogalerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDNSSettingsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDeviceDNSSettingsList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/mgmtdnssettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDNSSettings(objId, callback)</td>
    <td style="padding:15px">getDeviceDNSSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/mgmtdnssettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDeviceDNSSettings(objId, body, callback)</td>
    <td style="padding:15px">editDeviceDNSSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/mgmtdnssettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataDNSSettingsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDataDNSSettingsList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/datadnssettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataDNSSettings(objId, callback)</td>
    <td style="padding:15px">getDataDNSSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/datadnssettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDataDNSSettings(objId, body, callback)</td>
    <td style="padding:15px">editDataDNSSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/datadnssettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerGroupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDNSServerGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/dnsservergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDNSServerGroup(body, callback)</td>
    <td style="padding:15px">addDNSServerGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/dnsservergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerGroup(objId, callback)</td>
    <td style="padding:15px">getDNSServerGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/dnsservergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDNSServerGroup(objId, body, callback)</td>
    <td style="padding:15px">editDNSServerGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/dnsservergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSServerGroup(objId, callback)</td>
    <td style="padding:15px">deleteDNSServerGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/dnsservergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataSSLCipherSettingList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDataSSLCipherSettingList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/datasslciphersettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataSSLCipherSetting(objId, callback)</td>
    <td style="padding:15px">getDataSSLCipherSetting</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/datasslciphersettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDataSSLCipherSetting(objId, body, callback)</td>
    <td style="padding:15px">editDataSSLCipherSetting</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/datasslciphersettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLCipherList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSSLCipherList</td>
    <td style="padding:15px">{base_path}/{version}/object/sslciphers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSSLCipher(body, callback)</td>
    <td style="padding:15px">addSSLCipher</td>
    <td style="padding:15px">{base_path}/{version}/object/sslciphers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLCipher(objId, callback)</td>
    <td style="padding:15px">getSSLCipher</td>
    <td style="padding:15px">{base_path}/{version}/object/sslciphers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSSLCipher(objId, body, callback)</td>
    <td style="padding:15px">editSSLCipher</td>
    <td style="padding:15px">{base_path}/{version}/object/sslciphers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSSLCipher(objId, callback)</td>
    <td style="padding:15px">deleteSSLCipher</td>
    <td style="padding:15px">{base_path}/{version}/object/sslciphers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpenSSLCipherInfoList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getOpenSSLCipherInfoList</td>
    <td style="padding:15px">{base_path}/{version}/operational/opensslcipherinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityRuleList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getIdentityRuleList</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies/{pathv1}/identityrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIdentityRule(at, parentId, body, callback)</td>
    <td style="padding:15px">addIdentityRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies/{pathv1}/identityrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityRule(parentId, objId, callback)</td>
    <td style="padding:15px">getIdentityRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies/{pathv1}/identityrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIdentityRule(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editIdentityRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies/{pathv1}/identityrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityRule(parentId, objId, callback)</td>
    <td style="padding:15px">deleteIdentityRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies/{pathv1}/identityrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIdentityPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityPolicy(objId, callback)</td>
    <td style="padding:15px">getIdentityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIdentityPolicy(objId, body, callback)</td>
    <td style="padding:15px">editIdentityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/identitypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternalCertificateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getInternalCertificateList</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInternalCertificate(body, callback)</td>
    <td style="padding:15px">addInternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternalCertificate(objId, callback)</td>
    <td style="padding:15px">getInternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editInternalCertificate(objId, body, callback)</td>
    <td style="padding:15px">editInternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternalCertificate(objId, callback)</td>
    <td style="padding:15px">deleteInternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternalCACertificateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getInternalCACertificateList</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcacertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInternalCACertificate(body, callback)</td>
    <td style="padding:15px">addInternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcacertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternalCACertificate(objId, callback)</td>
    <td style="padding:15px">getInternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcacertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editInternalCACertificate(objId, body, callback)</td>
    <td style="padding:15px">editInternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcacertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternalCACertificate(objId, callback)</td>
    <td style="padding:15px">deleteInternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/internalcacertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalCertificateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getExternalCertificateList</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExternalCertificate(body, callback)</td>
    <td style="padding:15px">addExternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalCertificate(objId, callback)</td>
    <td style="padding:15px">getExternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editExternalCertificate(objId, body, callback)</td>
    <td style="padding:15px">editExternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalCertificate(objId, callback)</td>
    <td style="padding:15px">deleteExternalCertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalCACertificateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getExternalCACertificateList</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcacertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExternalCACertificate(body, callback)</td>
    <td style="padding:15px">addExternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcacertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalCACertificate(objId, callback)</td>
    <td style="padding:15px">getExternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcacertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editExternalCACertificate(objId, body, callback)</td>
    <td style="padding:15px">editExternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcacertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalCACertificate(objId, callback)</td>
    <td style="padding:15px">deleteExternalCACertificate</td>
    <td style="padding:15px">{base_path}/{version}/object/externalcacertificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveDirectoryRealmList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getActiveDirectoryRealmList</td>
    <td style="padding:15px">{base_path}/{version}/object/realms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addActiveDirectoryRealm(body, callback)</td>
    <td style="padding:15px">addActiveDirectoryRealm</td>
    <td style="padding:15px">{base_path}/{version}/object/realms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveDirectoryRealm(objId, callback)</td>
    <td style="padding:15px">getActiveDirectoryRealm</td>
    <td style="padding:15px">{base_path}/{version}/object/realms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editActiveDirectoryRealm(objId, body, callback)</td>
    <td style="padding:15px">editActiveDirectoryRealm</td>
    <td style="padding:15px">{base_path}/{version}/object/realms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActiveDirectoryRealm(objId, callback)</td>
    <td style="padding:15px">deleteActiveDirectoryRealm</td>
    <td style="padding:15px">{base_path}/{version}/object/realms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecialRealmList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSpecialRealmList</td>
    <td style="padding:15px">{base_path}/{version}/object/specialrealms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecialRealm(objId, callback)</td>
    <td style="padding:15px">getSpecialRealm</td>
    <td style="padding:15px">{base_path}/{version}/object/specialrealms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealmTrafficUserList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getRealmTrafficUserList</td>
    <td style="padding:15px">{base_path}/{version}/object/realms/{pathv1}/trafficusers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficUserList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getTrafficUserList</td>
    <td style="padding:15px">{base_path}/{version}/object/trafficusers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealmTrafficUserGroupList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getRealmTrafficUserGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/realms/{pathv1}/trafficusergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficUserGroupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getTrafficUserGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/trafficusergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectTest(objId, callback)</td>
    <td style="padding:15px">getConnectTest</td>
    <td style="padding:15px">{base_path}/{version}/action/connecttest/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleTroubleshootList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getScheduleTroubleshootList</td>
    <td style="padding:15px">{base_path}/{version}/action/troubleshoot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addScheduleTroubleshoot(body, callback)</td>
    <td style="padding:15px">addScheduleTroubleshoot</td>
    <td style="padding:15px">{base_path}/{version}/action/troubleshoot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleTroubleshoot(objId, callback)</td>
    <td style="padding:15px">getScheduleTroubleshoot</td>
    <td style="padding:15px">{base_path}/{version}/action/troubleshoot/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editScheduleTroubleshoot(objId, body, callback)</td>
    <td style="padding:15px">editScheduleTroubleshoot</td>
    <td style="padding:15px">{base_path}/{version}/action/troubleshoot/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScheduleTroubleshoot(objId, callback)</td>
    <td style="padding:15px">deleteScheduleTroubleshoot</td>
    <td style="padding:15px">{base_path}/{version}/action/troubleshoot/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTroubleshootJobHistoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getTroubleshootJobHistoryList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/jobs/troubleshootjob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTroubleshootJobHistory(objId, callback)</td>
    <td style="padding:15px">getTroubleshootJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/jobs/troubleshootjob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTroubleshootJobHistory(objId, callback)</td>
    <td style="padding:15px">deleteTroubleshootJobHistory</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/jobs/troubleshootjob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullUpgradeImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPullUpgradeImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/pullupgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPullUpgradeImmediate(body, callback)</td>
    <td style="padding:15px">addPullUpgradeImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/pullupgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullFileList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPullFileList</td>
    <td style="padding:15px">{base_path}/{version}/action/pullfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPullFile(body, callback)</td>
    <td style="padding:15px">addPullFile</td>
    <td style="padding:15px">{base_path}/{version}/action/pullfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeFileList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getUpgradeFileList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/upgradefiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeFile(objId, callback)</td>
    <td style="padding:15px">getUpgradeFile</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/upgradefiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUpgradeFile(objId, callback)</td>
    <td style="padding:15px">deleteUpgradeFile</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/upgradefiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startUpgrade(callback)</td>
    <td style="padding:15px">startUpgrade</td>
    <td style="padding:15px">{base_path}/{version}/action/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCancelUpgrade(callback)</td>
    <td style="padding:15px">addCancelUpgrade</td>
    <td style="padding:15px">{base_path}/{version}/action/cancelupgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServerContainerList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDHCPServerContainerList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/dhcpservercontainers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServerContainer(objId, callback)</td>
    <td style="padding:15px">getDHCPServerContainer</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/dhcpservercontainers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDHCPServerContainer(objId, body, callback)</td>
    <td style="padding:15px">editDHCPServerContainer</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/dhcpservercontainers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudConfigList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCloudConfigList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudConfig(objId, callback)</td>
    <td style="padding:15px">getCloudConfig</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudconfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCloudConfig(objId, body, callback)</td>
    <td style="padding:15px">editCloudConfig</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudconfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploadcert(fileToUpload, callback)</td>
    <td style="padding:15px">postuploadcert</td>
    <td style="padding:15px">{base_path}/{version}/action/uploadcert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTestDirectory(body, callback)</td>
    <td style="padding:15px">addTestDirectory</td>
    <td style="padding:15px">{base_path}/{version}/action/testrealm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTestIdentityServicesEngineConnectivity(body, callback)</td>
    <td style="padding:15px">addTestIdentityServicesEngineConnectivity</td>
    <td style="padding:15px">{base_path}/{version}/action/testidentityservicesengineconnectivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityGroupTagList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityGroupTagList</td>
    <td style="padding:15px">{base_path}/{version}/object/securitygrouptags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityGroupTag(objId, callback)</td>
    <td style="padding:15px">getSecurityGroupTag</td>
    <td style="padding:15px">{base_path}/{version}/object/securitygrouptags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSGTDynamicObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSGTDynamicObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/sgtdynamicobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSGTDynamicObject(body, callback)</td>
    <td style="padding:15px">addSGTDynamicObject</td>
    <td style="padding:15px">{base_path}/{version}/object/sgtdynamicobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSGTDynamicObject(objId, callback)</td>
    <td style="padding:15px">getSGTDynamicObject</td>
    <td style="padding:15px">{base_path}/{version}/object/sgtdynamicobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSGTDynamicObject(objId, body, callback)</td>
    <td style="padding:15px">editSGTDynamicObject</td>
    <td style="padding:15px">{base_path}/{version}/object/sgtdynamicobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSGTDynamicObject(objId, callback)</td>
    <td style="padding:15px">deleteSGTDynamicObject</td>
    <td style="padding:15px">{base_path}/{version}/object/sgtdynamicobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNTPStatus(objId, callback)</td>
    <td style="padding:15px">getNTPStatus</td>
    <td style="padding:15px">{base_path}/{version}/operational/ntpstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceInfo(objId, callback)</td>
    <td style="padding:15px">getInterfaceInfo</td>
    <td style="padding:15px">{base_path}/{version}/operational/interfaceinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSSLPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLPolicy(objId, callback)</td>
    <td style="padding:15px">getSSLPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSSLPolicy(objId, body, callback)</td>
    <td style="padding:15px">editSSLPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLRuleList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getSSLRuleList</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies/{pathv1}/sslrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSSLRule(at, parentId, body, callback)</td>
    <td style="padding:15px">addSSLRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies/{pathv1}/sslrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLRule(parentId, objId, callback)</td>
    <td style="padding:15px">getSSLRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies/{pathv1}/sslrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSSLRule(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editSSLRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies/{pathv1}/sslrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSSLRule(parentId, objId, callback)</td>
    <td style="padding:15px">deleteSSLRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/sslpolicies/{pathv1}/sslrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostUpgradeFlagsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPostUpgradeFlagsList</td>
    <td style="padding:15px">{base_path}/{version}/managemententity/postupgradeflags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostUpgradeFlags(objId, callback)</td>
    <td style="padding:15px">getPostUpgradeFlags</td>
    <td style="padding:15px">{base_path}/{version}/managemententity/postupgradeflags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editPostUpgradeFlags(objId, body, callback)</td>
    <td style="padding:15px">editPostUpgradeFlags</td>
    <td style="padding:15px">{base_path}/{version}/managemententity/postupgradeflags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOSPFList(offset, limit, sort, filter, vrfId, callback)</td>
    <td style="padding:15px">getOSPFList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOSPF(vrfId, body, callback)</td>
    <td style="padding:15px">addOSPF</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOSPF(vrfId, objId, callback)</td>
    <td style="padding:15px">getOSPF</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editOSPF(vrfId, objId, body, callback)</td>
    <td style="padding:15px">editOSPF</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOSPF(vrfId, objId, callback)</td>
    <td style="padding:15px">deleteOSPF</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospf/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOSPFInterfaceSettingsList(offset, limit, sort, filter, vrfId, callback)</td>
    <td style="padding:15px">getOSPFInterfaceSettingsList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospfinterfacesettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOSPFInterfaceSettings(vrfId, body, callback)</td>
    <td style="padding:15px">addOSPFInterfaceSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospfinterfacesettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOSPFInterfaceSettings(vrfId, objId, callback)</td>
    <td style="padding:15px">getOSPFInterfaceSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospfinterfacesettings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editOSPFInterfaceSettings(vrfId, objId, body, callback)</td>
    <td style="padding:15px">editOSPFInterfaceSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospfinterfacesettings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOSPFInterfaceSettings(vrfId, objId, callback)</td>
    <td style="padding:15px">deleteOSPFInterfaceSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/ospfinterfacesettings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGPList(offset, limit, sort, filter, vrfId, callback)</td>
    <td style="padding:15px">getBGPList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBGP(vrfId, body, callback)</td>
    <td style="padding:15px">addBGP</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGP(vrfId, objId, callback)</td>
    <td style="padding:15px">getBGP</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/bgp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editBGP(vrfId, objId, body, callback)</td>
    <td style="padding:15px">editBGP</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/bgp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBGP(vrfId, objId, callback)</td>
    <td style="padding:15px">deleteBGP</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/bgp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGPGeneralSettingsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getBGPGeneralSettingsList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/bgpgeneralsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBGPGeneralSettings(body, callback)</td>
    <td style="padding:15px">addBGPGeneralSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/bgpgeneralsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGPGeneralSettings(objId, callback)</td>
    <td style="padding:15px">getBGPGeneralSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/bgpgeneralsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editBGPGeneralSettings(objId, body, callback)</td>
    <td style="padding:15px">editBGPGeneralSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/bgpgeneralsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBGPGeneralSettings(objId, callback)</td>
    <td style="padding:15px">deleteBGPGeneralSettings</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/bgpgeneralsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlexConfigObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getFlexConfigObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFlexConfigObject(body, callback)</td>
    <td style="padding:15px">addFlexConfigObject</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlexConfigObject(objId, callback)</td>
    <td style="padding:15px">getFlexConfigObject</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editFlexConfigObject(objId, body, callback)</td>
    <td style="padding:15px">editFlexConfigObject</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFlexConfigObject(objId, callback)</td>
    <td style="padding:15px">deleteFlexConfigObject</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlexConfigPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getFlexConfigPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFlexConfigPolicy(body, callback)</td>
    <td style="padding:15px">addFlexConfigPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlexConfigPolicy(objId, callback)</td>
    <td style="padding:15px">getFlexConfigPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editFlexConfigPolicy(objId, body, callback)</td>
    <td style="padding:15px">editFlexConfigPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFlexConfigPolicy(objId, callback)</td>
    <td style="padding:15px">deleteFlexConfigPolicy</td>
    <td style="padding:15px">{base_path}/{version}/object/flexconfigpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHAConfigurationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getHAConfigurationList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/ha/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHAConfiguration(objId, callback)</td>
    <td style="padding:15px">getHAConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/ha/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editHAConfiguration(objId, body, callback)</td>
    <td style="padding:15px">editHAConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/ha/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHAFailoverConfigurationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getHAFailoverConfigurationList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/ha/failoverconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHAFailoverConfiguration(objId, callback)</td>
    <td style="padding:15px">getHAFailoverConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/ha/failoverconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editHAFailoverConfiguration(objId, body, callback)</td>
    <td style="padding:15px">editHAFailoverConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/ha/failoverconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHAStatus(objId, callback)</td>
    <td style="padding:15px">getHAStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/ha/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRadiusIdentitySourceList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getRadiusIdentitySourceList</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRadiusIdentitySource(body, callback)</td>
    <td style="padding:15px">addRadiusIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRadiusIdentitySource(objId, callback)</td>
    <td style="padding:15px">getRadiusIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRadiusIdentitySource(objId, body, callback)</td>
    <td style="padding:15px">editRadiusIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRadiusIdentitySource(objId, callback)</td>
    <td style="padding:15px">deleteRadiusIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalIdentitySourceList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getLocalIdentitySourceList</td>
    <td style="padding:15px">{base_path}/{version}/object/localidentitysources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalIdentitySource(objId, callback)</td>
    <td style="padding:15px">getLocalIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/localidentitysources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTestIdentitySource(body, callback)</td>
    <td style="padding:15px">addTestIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/action/testidentitysource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveUserSessionsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getActiveUserSessionsList</td>
    <td style="padding:15px">{base_path}/{version}/action/activeusersessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveUserSessions(objId, callback)</td>
    <td style="padding:15px">getActiveUserSessions</td>
    <td style="padding:15px">{base_path}/{version}/action/activeusersessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActiveUserSessions(objId, callback)</td>
    <td style="padding:15px">deleteActiveUserSessions</td>
    <td style="padding:15px">{base_path}/{version}/action/activeusersessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAAASettingList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAAASettingList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/aaasettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAAASetting(objId, callback)</td>
    <td style="padding:15px">getAAASetting</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/aaasettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAAASetting(objId, body, callback)</td>
    <td style="padding:15px">editAAASetting</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/aaasettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRadiusIdentitySourceGroupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getRadiusIdentitySourceGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysourcegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRadiusIdentitySourceGroup(body, callback)</td>
    <td style="padding:15px">addRadiusIdentitySourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysourcegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRadiusIdentitySourceGroup(objId, callback)</td>
    <td style="padding:15px">getRadiusIdentitySourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysourcegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRadiusIdentitySourceGroup(objId, body, callback)</td>
    <td style="padding:15px">editRadiusIdentitySourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysourcegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRadiusIdentitySourceGroup(objId, callback)</td>
    <td style="padding:15px">deleteRadiusIdentitySourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/radiusidentitysourcegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRolePermissionList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getRolePermissionList</td>
    <td style="padding:15px">{base_path}/{version}/operational/rolepermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRolePermission(objId, callback)</td>
    <td style="padding:15px">getRolePermission</td>
    <td style="padding:15px">{base_path}/{version}/operational/rolepermissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomLoggingListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCustomLoggingListList</td>
    <td style="padding:15px">{base_path}/{version}/object/customlogginglists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCustomLoggingList(body, callback)</td>
    <td style="padding:15px">addCustomLoggingList</td>
    <td style="padding:15px">{base_path}/{version}/object/customlogginglists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomLoggingList(objId, callback)</td>
    <td style="padding:15px">getCustomLoggingList</td>
    <td style="padding:15px">{base_path}/{version}/object/customlogginglists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCustomLoggingList(objId, body, callback)</td>
    <td style="padding:15px">editCustomLoggingList</td>
    <td style="padding:15px">{base_path}/{version}/object/customlogginglists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomLoggingList(objId, callback)</td>
    <td style="padding:15px">deleteCustomLoggingList</td>
    <td style="padding:15px">{base_path}/{version}/object/customlogginglists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebAnalyticsSettingList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getWebAnalyticsSettingList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/webanalyticssettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebAnalyticsSetting(objId, callback)</td>
    <td style="padding:15px">getWebAnalyticsSetting</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/webanalyticssettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editWebAnalyticsSetting(objId, body, callback)</td>
    <td style="padding:15px">editWebAnalyticsSetting</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/webanalyticssettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInitialProvisionList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getInitialProvisionList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInitialProvision(body, callback)</td>
    <td style="padding:15px">addInitialProvision</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInitialProvision(objId, callback)</td>
    <td style="padding:15px">getInitialProvision</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/action/provision/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHitCountList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getHitCountList</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editHitCount(parentId, callback)</td>
    <td style="padding:15px">editHitCount</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHitCount(filter, parentId, callback)</td>
    <td style="padding:15px">deleteHitCount</td>
    <td style="padding:15px">{base_path}/{version}/policy/accesspolicies/{pathv1}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDuoLDAPIdentitySourceList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDuoLDAPIdentitySourceList</td>
    <td style="padding:15px">{base_path}/{version}/object/duoldapidentitysources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDuoLDAPIdentitySource(body, callback)</td>
    <td style="padding:15px">addDuoLDAPIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/duoldapidentitysources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDuoLDAPIdentitySource(objId, callback)</td>
    <td style="padding:15px">getDuoLDAPIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/duoldapidentitysources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDuoLDAPIdentitySource(objId, body, callback)</td>
    <td style="padding:15px">editDuoLDAPIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/duoldapidentitysources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDuoLDAPIdentitySource(objId, callback)</td>
    <td style="padding:15px">deleteDuoLDAPIdentitySource</td>
    <td style="padding:15px">{base_path}/{version}/object/duoldapidentitysources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilePolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getFilePolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFilePolicy(body, callback)</td>
    <td style="padding:15px">addFilePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilePolicy(objId, callback)</td>
    <td style="padding:15px">getFilePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editFilePolicy(objId, body, callback)</td>
    <td style="padding:15px">editFilePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFilePolicy(objId, callback)</td>
    <td style="padding:15px">deleteFilePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilePolicyConfigurationList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getFilePolicyConfigurationList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/filepolicyconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilePolicyConfiguration(objId, callback)</td>
    <td style="padding:15px">getFilePolicyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/filepolicyconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editFilePolicyConfiguration(objId, body, callback)</td>
    <td style="padding:15px">editFilePolicyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/filepolicyconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileRuleList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getFileRuleList</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}/filerules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFileRule(at, parentId, body, callback)</td>
    <td style="padding:15px">addFileRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}/filerules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileRule(parentId, objId, callback)</td>
    <td style="padding:15px">getFileRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}/filerules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editFileRule(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editFileRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}/filerules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFileRule(parentId, objId, callback)</td>
    <td style="padding:15px">deleteFileRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/filepolicies/{pathv1}/filerules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileTypeList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getFileTypeList</td>
    <td style="padding:15px">{base_path}/{version}/object/filetypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileType(objId, callback)</td>
    <td style="padding:15px">getFileType</td>
    <td style="padding:15px">{base_path}/{version}/object/filetypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileTypeCategoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getFileTypeCategoryList</td>
    <td style="padding:15px">{base_path}/{version}/object/filetypecategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileTypeCategory(objId, callback)</td>
    <td style="padding:15px">getFileTypeCategory</td>
    <td style="padding:15px">{base_path}/{version}/object/filetypecategories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAmpCloudConfigList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAmpCloudConfigList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampcloudconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAmpCloudConfig(objId, callback)</td>
    <td style="padding:15px">getAmpCloudConfig</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampcloudconfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAmpCloudConfig(objId, body, callback)</td>
    <td style="padding:15px">editAmpCloudConfig</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampcloudconfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMPServerList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAMPServerList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampservers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMPServer(objId, callback)</td>
    <td style="padding:15px">getAMPServer</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampservers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMPCloudConnectionList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAMPCloudConnectionList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampcloudconnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMPCloudConnection(objId, callback)</td>
    <td style="padding:15px">getAMPCloudConnection</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampcloudconnections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAMPCloudConnection(objId, body, callback)</td>
    <td style="padding:15px">editAMPCloudConnection</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ampcloudconnections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadstoredfileshalist(objId, callback)</td>
    <td style="padding:15px">getdownloadstoredfileshalist</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadstoredfileshalist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleStoredFileSHAListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getScheduleStoredFileSHAListList</td>
    <td style="padding:15px">{base_path}/{version}/action/storedfileshalist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addScheduleStoredFileSHAList(body, callback)</td>
    <td style="padding:15px">addScheduleStoredFileSHAList</td>
    <td style="padding:15px">{base_path}/{version}/action/storedfileshalist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleStoredFileSHAListJobHistoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getScheduleStoredFileSHAListJobHistoryList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/jobs/storedfileshalistjob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadstoredfiles(objId, callback)</td>
    <td style="padding:15px">getdownloadstoredfiles</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadstoredfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploadcleanhashlist(fileToUpload, callback)</td>
    <td style="padding:15px">postuploadcleanhashlist</td>
    <td style="padding:15px">{base_path}/{version}/action/uploadcleanhashlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadcleanhashlist(objId, callback)</td>
    <td style="padding:15px">getdownloadcleanhashlist</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadcleanhashlist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCleanListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCleanListList</td>
    <td style="padding:15px">{base_path}/{version}/objects/cleanhashlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCleanList(objId, callback)</td>
    <td style="padding:15px">getCleanList</td>
    <td style="padding:15px">{base_path}/{version}/objects/cleanhashlist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCleanList(objId, callback)</td>
    <td style="padding:15px">deleteCleanList</td>
    <td style="padding:15px">{base_path}/{version}/objects/cleanhashlist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploadcustomdetectionhashlist(fileToUpload, callback)</td>
    <td style="padding:15px">postuploadcustomdetectionhashlist</td>
    <td style="padding:15px">{base_path}/{version}/action/uploadcustomdetectionhashlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadcustomdetectionhashlist(objId, callback)</td>
    <td style="padding:15px">getdownloadcustomdetectionhashlist</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadcustomdetectionhashlist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomDetectionListList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCustomDetectionListList</td>
    <td style="padding:15px">{base_path}/{version}/objects/customdetectionhashlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomDetectionList(objId, callback)</td>
    <td style="padding:15px">getCustomDetectionList</td>
    <td style="padding:15px">{base_path}/{version}/objects/customdetectionhashlist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomDetectionList(objId, callback)</td>
    <td style="padding:15px">deleteCustomDetectionList</td>
    <td style="padding:15px">{base_path}/{version}/objects/customdetectionhashlist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMalwareUpdateConnectionStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getMalwareUpdateConnectionStatusList</td>
    <td style="padding:15px">{base_path}/{version}/operation/malwareupdateconnectionstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMPCloudConnectionStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getAMPCloudConnectionStatusList</td>
    <td style="padding:15px">{base_path}/{version}/operation/ampcloudconnectionstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualRouterList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getVirtualRouterList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVirtualRouter(body, callback)</td>
    <td style="padding:15px">addVirtualRouter</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualRouter(objId, callback)</td>
    <td style="padding:15px">getVirtualRouter</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVirtualRouter(objId, body, callback)</td>
    <td style="padding:15px">editVirtualRouter</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualRouter(objId, callback)</td>
    <td style="padding:15px">deleteVirtualRouter</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStaticRouteEntryList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getStaticRouteEntryList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/staticrouteentries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addStaticRouteEntry(at, parentId, body, callback)</td>
    <td style="padding:15px">addStaticRouteEntry</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/staticrouteentries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStaticRouteEntry(parentId, objId, callback)</td>
    <td style="padding:15px">getStaticRouteEntry</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/staticrouteentries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editStaticRouteEntry(at, parentId, objId, body, callback)</td>
    <td style="padding:15px">editStaticRouteEntry</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/staticrouteentries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStaticRouteEntry(parentId, objId, callback)</td>
    <td style="padding:15px">deleteStaticRouteEntry</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/routing/virtualrouters/{pathv1}/staticrouteentries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleConfigExportList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getScheduleConfigExportList</td>
    <td style="padding:15px">{base_path}/{version}/action/configexport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addScheduleConfigExport(body, callback)</td>
    <td style="padding:15px">addScheduleConfigExport</td>
    <td style="padding:15px">{base_path}/{version}/action/configexport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigExportJobStatus(objId, callback)</td>
    <td style="padding:15px">getConfigExportJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/jobs/configexportstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigExportJobStatus(objId, callback)</td>
    <td style="padding:15px">deleteConfigExportJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/jobs/configexportstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigExportJobStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getConfigExportJobStatusList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/configexportstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadConfigFile(objId, callback)</td>
    <td style="padding:15px">downloadConfigFile</td>
    <td style="padding:15px">{base_path}/{version}/action/downloadconfigfile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploadconfigfile(fileToUpload, callback)</td>
    <td style="padding:15px">postuploadconfigfile</td>
    <td style="padding:15px">{base_path}/{version}/action/uploadconfigfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigImportExportFileInfo(objId, callback)</td>
    <td style="padding:15px">getConfigImportExportFileInfo</td>
    <td style="padding:15px">{base_path}/{version}/action/configfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigImportExportFileInfo(objId, callback)</td>
    <td style="padding:15px">deleteConfigImportExportFileInfo</td>
    <td style="padding:15px">{base_path}/{version}/action/configfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigImportExportFileInfoList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getConfigImportExportFileInfoList</td>
    <td style="padding:15px">{base_path}/{version}/action/configfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleConfigImportList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getScheduleConfigImportList</td>
    <td style="padding:15px">{base_path}/{version}/action/configimport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addScheduleConfigImport(body, callback)</td>
    <td style="padding:15px">addScheduleConfigImport</td>
    <td style="padding:15px">{base_path}/{version}/action/configimport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigImportJobStatus(objId, callback)</td>
    <td style="padding:15px">getConfigImportJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/jobs/configimportstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigImportJobStatus(objId, callback)</td>
    <td style="padding:15px">deleteConfigImportJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/jobs/configimportstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigImportJobStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getConfigImportJobStatusList</td>
    <td style="padding:15px">{base_path}/{version}/jobs/configimportstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntrusionPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIntrusionPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/intrusionpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntrusionPolicy(objId, callback)</td>
    <td style="padding:15px">getIntrusionPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/intrusionpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIntrusionPolicy(objId, body, callback)</td>
    <td style="padding:15px">editIntrusionPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/intrusionpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntrusionRuleList(offset, limit, sort, filter, parentId, callback)</td>
    <td style="padding:15px">getIntrusionRuleList</td>
    <td style="padding:15px">{base_path}/{version}/policy/intrusionpolicies/{pathv1}/intrusionrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntrusionRule(parentId, objId, callback)</td>
    <td style="padding:15px">getIntrusionRule</td>
    <td style="padding:15px">{base_path}/{version}/policy/intrusionpolicies/{pathv1}/intrusionrules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIntrusionPolicyRuleUpdate(objId, body, callback)</td>
    <td style="padding:15px">editIntrusionPolicyRuleUpdate</td>
    <td style="padding:15px">{base_path}/{version}/policy/intrusionpolicies/{pathv1}/ruleupdates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntrusionSettingsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getIntrusionSettingsList</td>
    <td style="padding:15px">{base_path}/{version}/object/intrusionsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntrusionSettings(objId, callback)</td>
    <td style="padding:15px">getIntrusionSettings</td>
    <td style="padding:15px">{base_path}/{version}/object/intrusionsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIntrusionSettings(objId, body, callback)</td>
    <td style="padding:15px">editIntrusionSettings</td>
    <td style="padding:15px">{base_path}/{version}/object/intrusionsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiskUsage(objId, callback)</td>
    <td style="padding:15px">getDiskUsage</td>
    <td style="padding:15px">{base_path}/{version}/operational/diskusage/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrendingReport(timeDuration, objId, callback)</td>
    <td style="padding:15px">getTrendingReport</td>
    <td style="padding:15px">{base_path}/{version}/monitor/trendingreports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAMonitorList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSLAMonitorList</td>
    <td style="padding:15px">{base_path}/{version}/object/slamonitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSLAMonitor(body, callback)</td>
    <td style="padding:15px">addSLAMonitor</td>
    <td style="padding:15px">{base_path}/{version}/object/slamonitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAMonitor(objId, callback)</td>
    <td style="padding:15px">getSLAMonitor</td>
    <td style="padding:15px">{base_path}/{version}/object/slamonitors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSLAMonitor(objId, body, callback)</td>
    <td style="padding:15px">editSLAMonitor</td>
    <td style="padding:15px">{base_path}/{version}/object/slamonitors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSLAMonitor(objId, callback)</td>
    <td style="padding:15px">deleteSLAMonitor</td>
    <td style="padding:15px">{base_path}/{version}/object/slamonitors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAMonitorStatus(objId, callback)</td>
    <td style="padding:15px">getSLAMonitorStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/slamonitorstatuses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAMonitorStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSLAMonitorStatusList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/slamonitorstatuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsImmediateList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsImmediateList</td>
    <td style="padding:15px">{base_path}/{version}/action/securityintelligenceupdatefeeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSecurityIntelligenceUpdateFeedsImmediate(body, callback)</td>
    <td style="padding:15px">addSecurityIntelligenceUpdateFeedsImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/securityintelligenceupdatefeeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsImmediate(objId, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/securityintelligenceupdatefeeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecurityIntelligenceUpdateFeedsImmediate(objId, body, callback)</td>
    <td style="padding:15px">editSecurityIntelligenceUpdateFeedsImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/securityintelligenceupdatefeeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityIntelligenceUpdateFeedsImmediate(objId, callback)</td>
    <td style="padding:15px">deleteSecurityIntelligenceUpdateFeedsImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/securityintelligenceupdatefeeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsScheduleList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsScheduleList</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/securityintelligencefeedsupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSecurityIntelligenceUpdateFeedsSchedule(body, callback)</td>
    <td style="padding:15px">addSecurityIntelligenceUpdateFeedsSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/securityintelligencefeedsupdateschedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsSchedule(objId, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceUpdateFeedsSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/securityintelligencefeedsupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecurityIntelligenceUpdateFeedsSchedule(objId, body, callback)</td>
    <td style="padding:15px">editSecurityIntelligenceUpdateFeedsSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/securityintelligencefeedsupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityIntelligenceUpdateFeedsSchedule(objId, callback)</td>
    <td style="padding:15px">deleteSecurityIntelligenceUpdateFeedsSchedule</td>
    <td style="padding:15px">{base_path}/{version}/managedentity/securityintelligencefeedsupdateschedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameFeedList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDomainNameFeedList</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamefeeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDomainNameFeed(body, callback)</td>
    <td style="padding:15px">addDomainNameFeed</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamefeeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameFeed(objId, callback)</td>
    <td style="padding:15px">getDomainNameFeed</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamefeeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDomainNameFeed(objId, body, callback)</td>
    <td style="padding:15px">editDomainNameFeed</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamefeeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNameFeed(objId, callback)</td>
    <td style="padding:15px">deleteDomainNameFeed</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamefeeds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameGroupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDomainNameGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameGroup(objId, callback)</td>
    <td style="padding:15px">getDomainNameGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDomainNameGroup(objId, body, callback)</td>
    <td style="padding:15px">editDomainNameGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameFeedCategoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDomainNameFeedCategoryList</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamefeedcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameFeedCategory(objId, callback)</td>
    <td style="padding:15px">getDomainNameFeedCategory</td>
    <td style="padding:15px">{base_path}/{version}/object/domainnamefeedcategories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemFeedObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSystemFeedObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/systemfeedobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemFeedObject(objId, callback)</td>
    <td style="padding:15px">getSystemFeedObject</td>
    <td style="padding:15px">{base_path}/{version}/object/systemfeedobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSystemFeedObject(objId, body, callback)</td>
    <td style="padding:15px">editSystemFeedObject</td>
    <td style="padding:15px">{base_path}/{version}/object/systemfeedobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLFeedCategoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getURLFeedCategoryList</td>
    <td style="padding:15px">{base_path}/{version}/object/urlfeedcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLFeedCategory(objId, callback)</td>
    <td style="padding:15px">getURLFeedCategory</td>
    <td style="padding:15px">{base_path}/{version}/object/urlfeedcategories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkFeedCategoryList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getNetworkFeedCategoryList</td>
    <td style="padding:15px">{base_path}/{version}/object/networkfeedcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkFeedCategory(objId, callback)</td>
    <td style="padding:15px">getNetworkFeedCategory</td>
    <td style="padding:15px">{base_path}/{version}/object/networkfeedcategories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligencePolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityIntelligencePolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligencePolicy(objId, callback)</td>
    <td style="padding:15px">getSecurityIntelligencePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecurityIntelligencePolicy(objId, body, callback)</td>
    <td style="padding:15px">editSecurityIntelligencePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceNetworkPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceNetworkPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencenetworkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceNetworkPolicy(objId, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceNetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencenetworkpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecurityIntelligenceNetworkPolicy(objId, body, callback)</td>
    <td style="padding:15px">editSecurityIntelligenceNetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencenetworkpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceURLPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceURLPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligenceurlpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceURLPolicy(objId, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceURLPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligenceurlpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecurityIntelligenceURLPolicy(objId, body, callback)</td>
    <td style="padding:15px">editSecurityIntelligenceURLPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligenceurlpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceDNSPolicyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceDNSPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencednspolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityIntelligenceDNSPolicy(objId, callback)</td>
    <td style="padding:15px">getSecurityIntelligenceDNSPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencednspolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecurityIntelligenceDNSPolicy(objId, body, callback)</td>
    <td style="padding:15px">editSecurityIntelligenceDNSPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policy/securityintelligencednspolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceMetricDataList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDeviceMetricDataList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceMetricData(objId, callback)</td>
    <td style="padding:15px">getDeviceMetricData</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceMetricNodeList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDeviceMetricNodeList</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/metricsschema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceMetricNode(objId, callback)</td>
    <td style="padding:15px">getDeviceMetricNode</td>
    <td style="padding:15px">{base_path}/{version}/devices/default/operational/metricsschema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTPList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPTPList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ptp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPTP(objId, callback)</td>
    <td style="padding:15px">getPTP</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ptp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editPTP(objId, body, callback)</td>
    <td style="padding:15px">editPTP</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/ptp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudCommunicationSettingsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCloudCommunicationSettingsList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudcommunicationsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudCommunicationSettings(objId, callback)</td>
    <td style="padding:15px">getCloudCommunicationSettings</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudcommunicationsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCloudCommunicationSettings(objId, body, callback)</td>
    <td style="padding:15px">editCloudCommunicationSettings</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudcommunicationsettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudManagementList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCloudManagementList</td>
    <td style="padding:15px">{base_path}/{version}/action/cloudmanagement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudManagement(body, callback)</td>
    <td style="padding:15px">addCloudManagement</td>
    <td style="padding:15px">{base_path}/{version}/action/cloudmanagement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudManagement(objId, callback)</td>
    <td style="padding:15px">getCloudManagement</td>
    <td style="padding:15px">{base_path}/{version}/action/cloudmanagement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCloudManagement(objId, body, callback)</td>
    <td style="padding:15px">editCloudManagement</td>
    <td style="padding:15px">{base_path}/{version}/action/cloudmanagement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudManagement(objId, callback)</td>
    <td style="padding:15px">deleteCloudManagement</td>
    <td style="padding:15px">{base_path}/{version}/action/cloudmanagement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudServicesInfo(objId, callback)</td>
    <td style="padding:15px">getCloudServicesInfo</td>
    <td style="padding:15px">{base_path}/{version}/operational/cloudservicesinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudRegionList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCloudRegionList</td>
    <td style="padding:15px">{base_path}/{version}/operational/cloudregions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudRegion(objId, callback)</td>
    <td style="padding:15px">getCloudRegion</td>
    <td style="padding:15px">{base_path}/{version}/operational/cloudregions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudEnrollmentImmediate(body, callback)</td>
    <td style="padding:15px">addCloudEnrollmentImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/cloudservices/enroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudUnenrollmentImmediate(callback)</td>
    <td style="padding:15px">addCloudUnenrollmentImmediate</td>
    <td style="padding:15px">{base_path}/{version}/action/cloudservices/unenroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudEventsList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getCloudEventsList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudEvents(objId, callback)</td>
    <td style="padding:15px">getCloudEvents</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCloudEvents(objId, body, callback)</td>
    <td style="padding:15px">editCloudEvents</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefenseOrchestratorList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getDefenseOrchestratorList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/cdo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefenseOrchestrator(objId, callback)</td>
    <td style="padding:15px">getDefenseOrchestrator</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/cdo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDefenseOrchestrator(objId, body, callback)</td>
    <td style="padding:15px">editDefenseOrchestrator</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/cdo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSuccessNetworkList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSuccessNetworkList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/csn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSuccessNetwork(objId, callback)</td>
    <td style="padding:15px">getSuccessNetwork</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/csn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSuccessNetwork(objId, body, callback)</td>
    <td style="padding:15px">editSuccessNetwork</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/cloudservices/csn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmartAgentConnectionList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSmartAgentConnectionList</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentconnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSmartAgentConnection(body, callback)</td>
    <td style="padding:15px">addSmartAgentConnection</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentconnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmartAgentConnection(objId, callback)</td>
    <td style="padding:15px">getSmartAgentConnection</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentconnections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSmartAgentConnection(objId, body, callback)</td>
    <td style="padding:15px">editSmartAgentConnection</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentconnections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSmartAgentConnection(objId, callback)</td>
    <td style="padding:15px">deleteSmartAgentConnection</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentconnections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmartAgentStatusList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSmartAgentStatusList</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentstatuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmartAgentStatus(objId, callback)</td>
    <td style="padding:15px">getSmartAgentStatus</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentstatuses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getLicenseList</td>
    <td style="padding:15px">{base_path}/{version}/license/smartlicenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLicense(body, callback)</td>
    <td style="padding:15px">addLicense</td>
    <td style="padding:15px">{base_path}/{version}/license/smartlicenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicense(objId, callback)</td>
    <td style="padding:15px">getLicense</td>
    <td style="padding:15px">{base_path}/{version}/license/smartlicenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLicense(objId, callback)</td>
    <td style="padding:15px">deleteLicense</td>
    <td style="padding:15px">{base_path}/{version}/license/smartlicenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSmartAgentSyncRequest(body, callback)</td>
    <td style="padding:15px">addSmartAgentSyncRequest</td>
    <td style="padding:15px">{base_path}/{version}/license/smartagentsyncrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPLRRequestCode(objId, callback)</td>
    <td style="padding:15px">getPLRRequestCode</td>
    <td style="padding:15px">{base_path}/{version}/license/operational/plrrequestcode/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPLRRequestCodeList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPLRRequestCodeList</td>
    <td style="padding:15px">{base_path}/{version}/license/operational/plrrequestcode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPLRAuthorizationCode(body, callback)</td>
    <td style="padding:15px">addPLRAuthorizationCode</td>
    <td style="padding:15px">{base_path}/{version}/license/action/installplrcode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPLRReleaseCode(body, callback)</td>
    <td style="padding:15px">addPLRReleaseCode</td>
    <td style="padding:15px">{base_path}/{version}/license/action/cancelreservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getNetworkObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkObject(body, callback)</td>
    <td style="padding:15px">addNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/object/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkObject(objId, callback)</td>
    <td style="padding:15px">getNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/object/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editNetworkObject(objId, body, callback)</td>
    <td style="padding:15px">editNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/object/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkObject(objId, callback)</td>
    <td style="padding:15px">deleteNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/object/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkObjectGroupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getNetworkObjectGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/networkgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkObjectGroup(body, callback)</td>
    <td style="padding:15px">addNetworkObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/networkgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkObjectGroup(objId, callback)</td>
    <td style="padding:15px">getNetworkObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/networkgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editNetworkObjectGroup(objId, body, callback)</td>
    <td style="padding:15px">editNetworkObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/networkgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkObjectGroup(objId, callback)</td>
    <td style="padding:15px">deleteNetworkObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/networkgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTCPPortObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getTCPPortObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/tcpports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTCPPortObject(body, callback)</td>
    <td style="padding:15px">addTCPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/tcpports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTCPPortObject(objId, callback)</td>
    <td style="padding:15px">getTCPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/tcpports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTCPPortObject(objId, body, callback)</td>
    <td style="padding:15px">editTCPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/tcpports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTCPPortObject(objId, callback)</td>
    <td style="padding:15px">deleteTCPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/tcpports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUDPPortObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getUDPPortObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/udpports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUDPPortObject(body, callback)</td>
    <td style="padding:15px">addUDPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/udpports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUDPPortObject(objId, callback)</td>
    <td style="padding:15px">getUDPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/udpports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editUDPPortObject(objId, body, callback)</td>
    <td style="padding:15px">editUDPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/udpports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUDPPortObject(objId, callback)</td>
    <td style="padding:15px">deleteUDPPortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/udpports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProtocolObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getProtocolObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addProtocolObject(body, callback)</td>
    <td style="padding:15px">addProtocolObject</td>
    <td style="padding:15px">{base_path}/{version}/object/protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProtocolObject(objId, callback)</td>
    <td style="padding:15px">getProtocolObject</td>
    <td style="padding:15px">{base_path}/{version}/object/protocols/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editProtocolObject(objId, body, callback)</td>
    <td style="padding:15px">editProtocolObject</td>
    <td style="padding:15px">{base_path}/{version}/object/protocols/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProtocolObject(objId, callback)</td>
    <td style="padding:15px">deleteProtocolObject</td>
    <td style="padding:15px">{base_path}/{version}/object/protocols/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPv4PortObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getICMPv4PortObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv4ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addICMPv4PortObject(body, callback)</td>
    <td style="padding:15px">addICMPv4PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv4ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPv4PortObject(objId, callback)</td>
    <td style="padding:15px">getICMPv4PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv4ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editICMPv4PortObject(objId, body, callback)</td>
    <td style="padding:15px">editICMPv4PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv4ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteICMPv4PortObject(objId, callback)</td>
    <td style="padding:15px">deleteICMPv4PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv4ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPv6PortObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getICMPv6PortObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv6ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addICMPv6PortObject(body, callback)</td>
    <td style="padding:15px">addICMPv6PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv6ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPv6PortObject(objId, callback)</td>
    <td style="padding:15px">getICMPv6PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv6ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editICMPv6PortObject(objId, body, callback)</td>
    <td style="padding:15px">editICMPv6PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv6ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteICMPv6PortObject(objId, callback)</td>
    <td style="padding:15px">deleteICMPv6PortObject</td>
    <td style="padding:15px">{base_path}/{version}/object/icmpv6ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortObjectGroupList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getPortObjectGroupList</td>
    <td style="padding:15px">{base_path}/{version}/object/portgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPortObjectGroup(body, callback)</td>
    <td style="padding:15px">addPortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/portgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortObjectGroup(objId, callback)</td>
    <td style="padding:15px">getPortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/portgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editPortObjectGroup(objId, body, callback)</td>
    <td style="padding:15px">editPortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/portgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortObjectGroup(objId, callback)</td>
    <td style="padding:15px">deletePortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/object/portgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeatureCapabilities(objId, callback)</td>
    <td style="padding:15px">getFeatureCapabilities</td>
    <td style="padding:15px">{base_path}/{version}/operational/featurecapabilities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHTTPProxyList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getHTTPProxyList</td>
    <td style="padding:15px">{base_path}/{version}/object/httpproxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHTTPProxy(objId, callback)</td>
    <td style="padding:15px">getHTTPProxy</td>
    <td style="padding:15px">{base_path}/{version}/object/httpproxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editHTTPProxy(objId, body, callback)</td>
    <td style="padding:15px">editHTTPProxy</td>
    <td style="padding:15px">{base_path}/{version}/object/httpproxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getSecretList</td>
    <td style="padding:15px">{base_path}/{version}/object/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSecret(body, callback)</td>
    <td style="padding:15px">addSecret</td>
    <td style="padding:15px">{base_path}/{version}/object/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecret(objId, callback)</td>
    <td style="padding:15px">getSecret</td>
    <td style="padding:15px">{base_path}/{version}/object/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSecret(objId, body, callback)</td>
    <td style="padding:15px">editSecret</td>
    <td style="padding:15px">{base_path}/{version}/object/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecret(objId, callback)</td>
    <td style="padding:15px">deleteSecret</td>
    <td style="padding:15px">{base_path}/{version}/object/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeRangeObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getTimeRangeObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/timeranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTimeRangeObject(body, callback)</td>
    <td style="padding:15px">addTimeRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timeranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeRangeObject(objId, callback)</td>
    <td style="padding:15px">getTimeRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timeranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTimeRangeObject(objId, body, callback)</td>
    <td style="padding:15px">editTimeRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timeranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTimeRangeObject(objId, callback)</td>
    <td style="padding:15px">deleteTimeRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timeranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeZoneSettingList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getTimeZoneSettingList</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/timezonesettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeZoneSetting(objId, callback)</td>
    <td style="padding:15px">getTimeZoneSetting</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/timezonesettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTimeZoneSetting(objId, body, callback)</td>
    <td style="padding:15px">editTimeZoneSetting</td>
    <td style="padding:15px">{base_path}/{version}/devicesettings/default/timezonesettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeZoneObjectList(offset, limit, sort, filter, callback)</td>
    <td style="padding:15px">getTimeZoneObjectList</td>
    <td style="padding:15px">{base_path}/{version}/object/timezoneobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTimeZoneObject(body, callback)</td>
    <td style="padding:15px">addTimeZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timezoneobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeZoneObject(objId, callback)</td>
    <td style="padding:15px">getTimeZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timezoneobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTimeZoneObject(objId, body, callback)</td>
    <td style="padding:15px">editTimeZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timezoneobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTimeZoneObject(objId, callback)</td>
    <td style="padding:15px">deleteTimeZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/object/timezoneobjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeZones(objId, callback)</td>
    <td style="padding:15px">getTimeZones</td>
    <td style="padding:15px">{base_path}/{version}/operational/timezones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">token(body, callback)</td>
    <td style="padding:15px">token</td>
    <td style="padding:15px">{base_path}/{version}/fdm/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiVersions(callback)</td>
    <td style="padding:15px">getApiVersions</td>
    <td style="padding:15px">{base_path}/{version}/api/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
